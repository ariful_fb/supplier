<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends CI_Controller
{
    public $loggedUserId;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('FrontEndModel');
        $this->loggedUserId=$this->session->user_id;
        if (!$this->loggedUserId) {
            redirect('home');
        }
        $this->load->library('pagination');
    }
    public function index()
    {

        $evaluation=$this->FrontEndModel->getUserEvaluation($this->loggedUserId);
        $data['nick_name']=$this->FrontEndModel->getColumValue('users', 'user_id', $this->loggedUserId, 'nick_name');
        $data['user_id']=$this->loggedUserId;

        //pagination

        $config = array();
        $config["base_url"] = base_url() . "user/evaluation";
        $total_row = count($evaluation);
        $config["total_rows"] = $total_row;
        $config["per_page"] = 12;
        $config['use_page_numbers'] = true;
        $config['num_links'] = $total_row;

        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_link'] = 'Previous';
        $config['next_link'] = 'Next';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->pagination->initialize($config);

        if ($this->uri->segment(3)) {
            $page = ($this->uri->segment(3))-1 ;
        } else {
            $page = 0;
        }
        $data["links"] = $this->pagination->create_links();
        $data['evaluation'] = $this->FrontEndModel->getPaginationEvaluation($this->loggedUserId, $config["per_page"], $page*$config["per_page"]);

        $main['content'] = $this->load->view('frontend/user/frontpage', $data, true);
        $this->load->view('frontend/templates/master', $main);
    }

    public function viewEvaluation($id)
    {
        $this->session->user_id;
        $data['evaluation']=$this->FrontEndModel->getEvaluation($id);
        if ($data['evaluation']) {
            $main['content'] = $this->load->view('frontend/user/evaluation_details', $data, true);
            $this->load->view('frontend/templates/master', $main);
        } else {
            redirect('user/home');
        }
    }

    public function addNickname()
    {
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        $this->FrontEndModel->updateData('users', 'user_id', $clean['user_id'], $clean);
        redirect('user/home');
    }

    public function requestDeleteGDPR()
    {
        $uId = $this->loggedUserId;
        $this->FrontEndModel->updateData('users', 'user_id', $uId, ['delete_gdpr'=>1]);
        $userInfo = $this->FrontEndModel->getData('users', 'user_id', $uId);
        $emailTempData = [
            'uEmail'    => $userInfo['user_email'],
            'uNickName' => $userInfo['nick_name'],
        ];
        $body = $this->load->view('email_templates/gdpr_admin', $emailTempData, true);
        $subject = 'GDPR Request';
        sendEmail($subject, $body, $userInfo['user_email'], ADMIN_EMAIL);
        sendEmail($subject, $body, ADMIN_EMAIL, $userInfo['user_email']);
    }

    public function updateProfile($userId)
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);

        if (!isset($_SESSION['role']) || $_SESSION['role']!=3) {
            $this->session->set_flashdata('error_msg', 'Please login with supplier account');
            redirect('/');
        }

        $data['userInfo'] = $this->FrontEndModel->searchSupplier(['user_id'=>$this->loggedUserId]);

        $data['countries']=$this->FrontEndModel->get('countries');
        $data['industry']=$this->FrontEndModel->get('industry');
        //if user fonund or not

        if (!$data['userInfo'] || $this->loggedUserId != $userId) {
            show_404();
        } else {
            $data['s_skill']=$this->FrontEndModel->getSupplierSkills($userId, 'rt_array');
            $data['skills'] = $this->FrontEndModel->get('skills');
            $data['userInfo']=$data['userInfo'][0];
            $data['userInfo']['other_countries'] = json_decode($data['userInfo']['other_countries']);
        }
        //prdie( $data['s_skill']);
        if (!$clean) {
            $main['content'] = $this->load->view('frontend/supplier/profile_update', $data, true);
            $this->load->view('frontend/templates/master', $main);
        } else {
            $supplierId = $this->loggedUserId;
            $supplier_info['supplier_id']= $this->loggedUserId;
            $supplier_info['email']=$clean['email'];
            $supplier_info['main_country']=$clean['main_country'];
            $supplier_info['main_city']=$clean['main_city'];
            $supplier_info['company_name']=$clean['company_name'];
            $supplier_info['industry']=$clean['industry'];
            $supplier_info['other_countries']=!empty($clean['other_countries'])?json_encode($clean['other_countries']):'';
            $supplier_info['size']=$clean['size'];
            $supplier_info['company_core_business']=$clean['company_core_business'];
            $supplier_info['company_description']=$clean['company_description'];
            $supplier_info['main_activities']=$clean['main_activities'];
            $supplier_info['reference']=$clean['reference'];
            $supplier_info['contact_phone']=$clean['contact_phone'];
            $supplier_info['contact_name']=$clean['contact_name'];

            //upload Logo
            if (isset($_FILES["userfile"]) && $_FILES["userfile"]["size"]>0) {
                $config['upload_path']          = './assets/images/users/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100000;
                $this->load->library('upload', $config);

                if (! $this->upload->do_upload('userfile')) {
                    $error = array('error' => $this->upload->display_errors());
                    pr($error);
                } else {
                    $data =$this->upload->data();
                    $supplier_info['logo']="assets/images/users/".$data['file_name'];
                }
            }
            $this->FrontEndModel->updateData('supplier_info', 'supplier_id', $userId, $supplier_info);
            //$this->FrontEndModel->deleteData('supplier_skills', 'supplier_id', $userId);
            if (!isset($clean['add_skill'])) {
                $clean['add_skill']=array();
            }
            $s_skill=$this->FrontEndModel->getSupplierSkills($userId, 'rt_array');
        //Insert new skill

            $supplierSkill=array_column($s_skill, 'id');
            $insertId = array_diff($clean['add_skill'], $supplierSkill);

            if (!empty($insertId)) {
                $i=0;
                foreach ($insertId as $key => $value) {
                    $insert_batch[$i]['skill_id'] = $value;
                    $insert_batch[$i]['supplier_id'] = $userId;
                    $i++;
                }

                $this->FrontEndModel->insert_batch('supplier_skills', $insert_batch);
               // prdie($insert_batch);
            }
           // prdie($clean);
            $deleteId = array();
            $deleteId = array_diff($supplierSkill, $clean['add_skill']);

            if (!empty($deleteId)) {
                foreach ($deleteId as $key => $value) {
                    $this->FrontEndModel->deleteData('supplier_skills', 'id', $value);
                }
            }
                /*if (isset($clean['add_skill'])) {
                    for ($i=0; $i <count($clean['add_skill']); $i++) {
                        $add_skill=array();
                        $add_skill['skill_id']=$clean['add_skill'][$i];
                        $add_skill['supplier_id']=$supplierId;
                        $this->FrontEndModel->insert('supplier_skills', $add_skill);
                    }
                }*/
                $supplier_skill=$clean['skill'];
                $supplier_skills['supplier_id']=$supplierId;
                if (isset($supplier_skill) && !empty($supplier_skill[0])) {
                    for ($i=0; $i <count($supplier_skill); $i++) {
                        if (is_numeric($supplier_skill[$i])) {
                            continue;
                        }
                        $skill=array();
                        $skill['name']=$supplier_skill[$i];
                        $insert_id=$this->FrontEndModel->insert('skills', $skill);
                        $supplier_skills['skill_id']=$insert_id;
                        $this->FrontEndModel->insert('supplier_skills', $supplier_skills);
                    }
                }

                $this->session->set_flashdata('success_msg', 'Supplier info updated successfully');
                redirect('profile/update/'.$userId);
            //general user
            }
        }

        public function supplierInfo($uniqueId)
        {
            $data['countries']=$this->FrontEndModel->get('countries');
            $data['industry']=$this->FrontEndModel->get('industry');
            $data['userInfo'] = $this->FrontEndModel->searchSupplier(['user_id'=>$this->loggedUserId]);

            $main['content'] = $this->load->view('frontend/supplier/profile_update', $data, true);
            $this->load->view('frontend/templates/master', $main);
        }
    }
