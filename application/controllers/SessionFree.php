<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SessionFree extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function linkedinCallback()
    {
        sendEmail('subject', 'body', 'from@example.com', 'to@example.com');
        $data = $_REQUEST['code'];
        sendEmail('subject', $data, 'from@example.com', 'to@example.com');
    }

    public function linkedinRedirect()
    {
        
        header('location:https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86ti3s04ywgxxy&redirect_uri=https://wh.rssoft.win/supplier/callback/linkedin&scope=r_emailaddress%20w_share');
        die;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86ti3s04ywgxxy&redirect_uri=https://wh.rssoft.win/supplier/callback/linkedin&scope=r_emailaddress%20w_share");
        //curl_setopt($ch, CURLOPT_POST, 0);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "");


        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
     
        $server_output = curl_exec($ch);
        echo $server_output;
        curl_close($ch);
        //$this->load->view('test');
    }

    public function errorPage()
    {
        $this->load->view('errors/html/error_404');
    }
}
