<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class PageController extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('PageModel');
		
	}
	public function addPage() {
		$post=$this->input->post();
		if($post){
			$clean=$this->security->xss_clean($post);
			$id=$this->PageModel->insert('pages',$clean);
			if($id){
				$this->session->set_userdata('log_scc','Page added successfully.');
				redirect('view_page');
			}else{
				$this->session->set_userdata('log_err','Page not added.');
				redirect('view_page');
			}
		}else{
			$data['page']=$this->PageModel->getPages('pages');
			$main['page']  = $this->load->view('backend/pages/add_page',$data,true);
			$main['title']  = 'Manage Pages';
			$this->load->view('backend/index',$main);
		}

	}

	public function viewPage(){
		$data['page']=$this->PageModel->getPages('pages');
		//echo "<pre>";print_r($data);die;

		if($data['page'])
		{
			$main['page']  = $this->load->view('backend/pages/view_pages',$data,true);
			$main['title']  = 'Manage Pages';
			$this->load->view('backend/index',$main);
		}
	}
	public function editPage($id){
		$data['get_info']=$this->PageModel->getData('pages','id',$id);
		//echo "<pre>";print_r($data);die;
		if($data['get_info'])
		{
			$main['page']  = $this->load->view('backend/pages/edit_page',$data,true);
			$main['title']  = 'Manage Pages';
			$this->load->view('backend/index',$main);

		}else{
			$this->session->set_userdata('log_err','Page not found.');
			redirect('view_page');
		}
	}

	public function updatePage() {
		$post=$this->input->post();
		$clean=$this->security->xss_clean($post);
		$clean['page_title'] = str_replace(' ','_',$clean['page_title']);
		$id=$this->PageModel->updateData('pages', 'id', $clean['id'], $clean);
		if($id){
			$this->session->set_userdata('log_scc','Page updated successfully.');
			redirect('edit_page/'.$clean['id']);
		}else{
			$this->session->set_userdata('log_err','Page not updated.');
			redirect('edit_page/'.$clean['id']);
		}
	}

	public function deletePage($id){

		$success=$this->PageModel->deleteData('pages','id',$id);
		if($success){
			$this->session->set_userdata('log_scc','Page deleted successfully.');
		}else{
			$this->session->set_userdata('log_scc','Page not deleted.');
		}
		redirect('view_page');	
	}




}