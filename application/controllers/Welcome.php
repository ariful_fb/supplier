<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Welcome extends CI_Controller
{


    /**
     * Constructor of Controller
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    /**
     * Create from display view
     *
     * @return Response
     */
    public function index()
    {
        $this->load->view('ajaxView');
    }


    /**
     * response of ajax json
     *
     * @return Response
     */
    public function ajaxPro()
    {
        $query = $this->input->get('query');
        $this->db->like('name', $query);


        $data = $this->db->get("tags")->result();


        echo json_encode($data);
    }
}
