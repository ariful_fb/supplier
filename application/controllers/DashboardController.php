<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardController extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DashboardModel'); 
		$loggedUserId = $this->session->userdata('id');
		if (!isset($loggedUserId)) {
			redirect('login');
		}
	}


	function index()
	{	

		$this->load->view('backend/index');
	}




}
