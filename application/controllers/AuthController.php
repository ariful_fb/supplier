<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AuthModel');
        $this->load->model('FrontEndModel');
        $this->load->library('form_validation');
    }


    public function index()
    {

        $this->session->unset_userdata('city');
        $this->session->unset_userdata('state');
        $this->session->unset_userdata('skill');
        $this->session->unset_userdata('keyword');
        $data['suppliers'] = $this->FrontEndModel->getAllSupplier();
        $data['totalConnect'] = $this->FrontEndModel->getConnect();
        $main['content'] = $this->load->view('frontend/index', $data, true);
        $this->load->view('frontend/templates/master', $main);
    }

    public function login()
    {
        $loggedUserId = $this->session->userdata('id');
        if (isset($loggedUserId)) {
            redirect('dashboard');
        }
        $this->load->view('backend/auth/login');
    }


    //////////////////////
    //admin site login //
    /////////////////////
    public function check_login()
    {

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', 'The login was unsucessful');
            redirect('login');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $clean['password']=md5($clean['password']);
            $userInfo = $this->AuthModel->checkLogin($clean);
            unset($userInfo->password);
            if ($userInfo) {
                foreach ($userInfo as $key => $val) {
                    $this->session->set_userdata($key, $val);
                }

                redirect('admin/dashboard');
            } else {
                $this->session->set_flashdata('error_msg', 'Incorrect Email or Password!');
                redirect('login');
            }
        }
    }


    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

    public function emailCheck()
    {
        $this->form_validation->set_rules('user_email', 'user_email', 'required|valid_email|is_unique[users.user_email]');
        if ($this->form_validation->run()==false) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_sin()
    {


        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $clean['password']=md5($clean['password']);
        $userInfo = $this->AuthModel->checkSignin($clean);

        if ($userInfo) {
            foreach ($userInfo as $key => $val) {
                $this->session->set_userdata($key, $val);
            }
            echo "1";
        } else {
            echo "2";
        }
    }

    public function signIn()
    {

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', 'The login was unsucessful');
            redirect('login');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            if (isset($clean['password'])) {
                $clean['password']=md5($clean['password']);
            }
            $userInfo = $this->AuthModel->checkSignin($clean);
            unset($userInfo->password);
            if ($userInfo) {
                foreach ($userInfo as $key => $val) {
                    $this->session->set_userdata($key, $val);
                }

                redirect('supplier/home');
            } else {
                $this->session->set_flashdata('error_msg', 'Incorrect Email or Password!');
                redirect('login');
            }
        }
    }

    //user site //

    public function signUp()
    {

        $this->form_validation->set_rules('nick_name', 'nick_name', 'required|is_unique[users.nick_name]');
        $this->form_validation->set_rules('user_email', 'user_email', 'required|is_unique[users.user_email]');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            echo "2";
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $data['password']=md5($clean['password']);
            $data['nick_name']=$clean['nick_name'];
            $data['user_email']=$clean['user_email'];
            $data['privacy1']=$clean['privacy1'];
            $data['privacy2']=$clean['privacy2'];
            $data['privacy3']=$clean['privacy3'];
            $data['role']=$clean['role'];
            $data['uniqueId'] = generateRandomString();
            $res=$this->AuthModel->insert('users', $data);
            $uniqueId=$this->FrontEndModel->getData('users','user_id',$res);
            if ($uniqueId) {
                echo  $uniqueId['uniqueId'];
            } else {
                echo "0";
            }
        }
    }

    public function supplier_login()
    {
        $authCode =$this->input->get('code');
        $res=$this->FrontEndModel->getData('email_confirmation', 'authCode', $authCode);
        if ($res) {
            $data['authCode']=$res['authCode'];
            $data['industry']=$this->FrontEndModel->get('industry');
            $data['countries']=$this->FrontEndModel->get('countries');
            $this->load->view('frontend/auth/supplier_registration1', $data);
        } else {
            $this->load->view('frontend/auth/invalid');
        }
    }
    public function addSupplier()
    {

        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $authCode=$clean['authCode'];

        $supplier = [
            'nick_name'  => $clean['nick_name'],
            'user_email' =>$clean['user_email'],
            'password'   => md5($clean['password']),
            'role'       => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        // $supplier['privacy1']=$clean['privacy1'];
        // $supplier['privacy2']=$clean['privacy2'];
        // $supplier['privacy3']=$clean['privacy3'];
        $result=$this->AuthModel->insert('users', $supplier);
        if ($result) {
            $supplier_info['supplier_id']= $result;
            $supplier_info['main_country']=$clean['main_country'];
            $supplier_info['main_city']=$clean['main_city'];
            $supplier_info['company_name']=$clean['company_name'];
            $supplier_info['industry']=$clean['industry'];
            $supplier_info['other_countries']=$clean['other_countries'];
            $supplier_info['size']=$clean['size'];
            $supplier_info['company_core_business']=$clean['company_core_business'];
            $supplier_info['main_activities']=$clean['main_activities'];
            $supplier_info['reference']=$clean['reference'];
            $supplier_info['contact_phone']=$clean['contact_phone'];
            $supplier_info['contact_name']=$clean['contact_name'];
            //$supplier_info['language']=$clean['language'];
            $this->AuthModel->insert('supplier_info', $supplier_info);
            $supplier_skill=$clean['skill'];
            if (count($clean['add_skill'])) {
                for ($i=0; $i <count($clean['add_skill']); $i++) {
                    $add_skill=array();
                    $add_skill['skill_id']=$clean['add_skill'][$i];
                    $add_skill['supplier_id']=$result;
                    $this->AuthModel->insert('supplier_skills', $add_skill);
                }
            }
            $supplier_skills['supplier_id']=$result;
            if (count($supplier_skill)) {
                for ($i=0; $i <count($supplier_skill); $i++) {
                    $skill=array();
                    $skill['name']=$supplier_skill[$i];
                    $insert_id=$this->AuthModel->insert('skills', $skill);
                    $supplier_skills['skill_id']=$result;
                    $this->AuthModel->insert('supplier_skills', $supplier_skills);
                }
            }
            $data['authCode']='';
            $this->FrontEndModel->updateData('email_confirmation', 'authCode', $authCode, $data);

            //on new registration admin will get notified by email
            $emailTempData = [
                'uEmail'    => $clean['user_email'],
                'uCompany' => $clean['industry'],
            ];
            $body = $this->load->view('email_templates/supplier_reg_to_admin', $emailTempData, true);
            $subject = 'Registration confirmation - Supplier';
            sendEmail($subject, $body, $clean['user_email'], ADMIN_EMAIL);

            redirect('success');
        }
    }

    public function success()
    {
        $this->load->view('frontend/auth/success');
    }

    public function supplierInfo($uniqueId)
    {
        $data['countries']=$this->FrontEndModel->get('countries');
        $data['industry']=$this->FrontEndModel->get('industry');
        $uniqueId = $this->FrontEndModel->getData('users','uniqueId', $uniqueId);
        if($uniqueId){
            $data['supplierId']=$uniqueId['uniqueId'];
            $main['content'] = $this->load->view('frontend/auth/supplier_registration_info',$data, true);
            $this->load->view('frontend/templates/master', $main);
        }else{
            $this->session->set_userdata('error_msg','Something Wrong!!');
            redirect('home');
        }
        
    }

    public function addSupplierInfo()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        if ($clean['supplier_id']) {
            $uniqueId = $this->FrontEndModel->getData('users','uniqueId', $clean['supplier_id']);
            $supplierId = $uniqueId['user_id'];
            $supplier_info['supplier_id']= $supplierId;
            $supplier_info['email']=$clean['email'];
            $supplier_info['main_country']=$clean['main_country'];
            $supplier_info['main_city']=$clean['main_city'];
            $supplier_info['company_name']=$clean['company_name'];
            $supplier_info['industry']=$clean['industry'];
            $supplier_info['other_countries']=!empty($clean['other_countries'])?json_encode($clean['other_countries']):'';
            $supplier_info['size']=$clean['size'];
            $supplier_info['company_core_business']=$clean['company_core_business'];
            $supplier_info['main_activities']=$clean['main_activities'];
            $supplier_info['reference']=$clean['reference'];
            $supplier_info['contact_phone']=$clean['contact_phone'];
            $supplier_info['contact_name']=$clean['contact_name'];

            //upload Logo
            $config['upload_path']          = './assets/images/users/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100000;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('userfile'))
            {
                $error = array('error' => $this->upload->display_errors());
                pr($error);
            }
            else
            {
                $data =$this->upload->data();
                $supplier_info['logo']="assets/images/users/".$data['file_name'];
            }
            $hasInfo = $this->FrontEndModel->getData('supplier_info','supplier_id', $supplierId);
            if($hasInfo){
                $this->session->set_userdata('error_msg',' Data Not Added.');
                redirect('supplier_registration_info/'.$clean['supplier_id']);
            }

            $hasInsert = $this->FrontEndModel->insertSupplier('supplier_info',$supplier_info);
            if($hasInsert){
                if (isset($clean['add_skill'])) {
                    for ($i=0; $i <count($clean['add_skill']); $i++) {
                        $add_skill=array();
                        $add_skill['skill_id']=$clean['add_skill'][$i];
                        $add_skill['supplier_id']=$supplierId;
                        $this->AuthModel->insert('supplier_skills', $add_skill);
                    }
                }
                $supplier_skill=$clean['skill'];
                $supplier_skills['supplier_id']=$supplierId;
                if (isset($supplier_skill) && !empty($supplier_skill[0])) {
                    for ($i=0; $i <count($supplier_skill); $i++) {
                        if(is_numeric($supplier_skill[$i]))continue;
                        $skill=array();
                        $skill['name']=$supplier_skill[$i];
                        $insert_id=$this->AuthModel->insert('skills', $skill);
                        $supplier_skills['skill_id']=$insert_id;
                        $this->AuthModel->insert('supplier_skills', $supplier_skills);
                    }
                }
            }else{
                $this->session->set_userdata('error_msg',' Data Not Added.');
                redirect('supplier_registration_info/'.$clean['supplier_id']);
            }
            $emailTempData = [
                'uEmail'    => $clean['user_email'],
                'uCompany' => $clean['industry'],
            ];
            $body = $this->load->view('email_templates/supplier_reg_to_admin', $emailTempData, true);
            $subject = 'Registration confirmation - Supplier';
            sendEmail($subject, $body, $clean['user_email'], ADMIN_EMAIL);

            $this->session->set_userdata('success_msg','Info Added Successfully. You need Administration approval to sign In.');
            redirect('home');


        }
    }



    public function signOut()
    {
        $this->session->sess_destroy();
        redirect('home');
    }





    public function check_singin()
    {

        $this->form_validation->set_rules('nick_name', 'nick_name', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('error_msg', 'The login was unsucessful');
            redirect('login');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $clean['password']=md5($clean['password']);
            $userInfo = $this->AuthModel->checkLogin($clean);
            unset($userInfo->password);
            if ($userInfo) {
                foreach ($userInfo as $key => $val) {
                    $this->session->set_userdata($key, $val);
                }

                redirect('admin/dashboard');
            } else {
                $this->session->set_flashdata('error_msg', 'Incorrect Email or Password!');
                redirect('login');
            }
        }
    }


    public function showStaticPage()
    {
        $pageName=$this->uri->segment(2);
        $data['page']=$this->FrontEndModel->getData('pages', 'page_title', $pageName);
        if(empty( $data['page']))redirect('404_override');

        $main['content']  = $this->load->view('frontend/pages/page', $data, true);
        $main['title']  = 'Supplier';
        $this->load->view('frontend/templates/master', $main);
    }

    public function demoEvaluation()
    {
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        $data['dimension'] = json_encode($clean);
        $insertId=$this->FrontEndModel->insert('evaluation_demo',$data);
        $this->session->insertEvaluationId = $insertId;
        print_r($data);die;
    }



    //*****************
    //Forgot Password
    //****************
    public function forgot_password()
    {
        $this->load->view('backend/auth/forgot_password');
    }

    public function sendResetPassEmail()
    {
        //validate and check input email
        $email = $this->input->post('email') ? $this->input->post('email') : '';

        //if email not exists in db redirect with error msg
        if (!$this->emailExist($email)) {
            $this->session->set_flashdata('error_message', 'This email not recorded');
            redirect('forgot_password');
        }

        //unique authentication code for reset pass
        $authCode = md5(uniqid(mt_rand(), true));
        //$resetLink = $_SERVER['HTTP_HOST'] . "/CI/UserController/passwordResetForm/?authCode={$authCode}&email={$email}";
        $resetLink =  base_url()."AuthController/passwordresetform?authCode={$authCode}&email={$email}";

        //valid email exist in DB
        $email = htmlspecialchars($email);
        $user  = $this->AuthModel->infoByEmail($email);

        //save the authCode against the requested email
        if (!empty($user)) {
            $this->AuthModel->saveAuthCode($authCode, $email);
            $Mail_Data['email'] = $email;
            $Mail_Data['username'] = $user['nick_name'];
            $Mail_Data['resetLink']  = $resetLink;
            $this->User_Pass_Reset($Mail_Data);
        }
        $this->session->set_flashdata('success_message', 'Password reset link sent to your email');
        redirect('forgot_password');
    }

    public function emailExist($email)
    {
        return $this->AuthModel->userEmailCheck($email);    //false->not exist //true->exist
    }

    public function validateInputEmail($email)
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
        if ($this->form_validation->run() == true) {
            return true;
        } else {
            return false;
        }
    }

    /* Data process for user password reset */

    public function User_Pass_Reset($Mail_Data)
    {
        $mail_data['to']            =  $Mail_Data['email'];
        $username                   =  $Mail_Data['username'];
        $template = $this->AuthModel->getEmailTempltateById('2');
        if ($template) {
            $subject                = $template->email_template_subject;
            $template_message       = $template->email_template;
            $actual_link            = $Mail_Data['resetLink'];
            $find                   = array( "{{actual_link}}","{{username}}");
            $replace                = array($actual_link,$username);
            $message                = str_replace($find, $replace, $template_message);
            $mail_data['subject']   = $subject;
            $mail_data['message']   = $message;
            $this->sendEmail($mail_data);
        }
    }

    /* Send Email Function */

    public function sendEmail($mail_data)
    {


        $mailTo        =   $mail_data['to'];
        $mailSubject   =   $mail_data['subject'];
        $message       =   $mail_data['message'];
        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from("info@look4supplier.com", "look4supplier");
        $this->email->to($mailTo);
        $this->email->subject($mailSubject);
        $message=$this->email->message($message);
        $this->email->send();
    }

    public function passwordResetForm()
    {

        if (isset($this->session->userdata['id'])) {
            redirect('login');
        }

        $arr['authCode'] = $_GET['authCode'];
        $arr['email'] = $_GET['email'];
        $this->load->view('backend/auth/password_reset', $arr);
    }

    public function passwordSuccess()
    {

        $this->load->view('backend/auth/password_success');
    }

    public function updatePassword()
    {
        $email = $_POST['email'];
        $password = md5($_POST['new_pass']);
        $authCode = $_POST['authCode'];
        $arr['email'] =  $email;
        $arr['authCode'] = $authCode;

        $this->form_validation->set_rules('new_pass', 'New Password', 'required|min_length[6]');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('conf_pass', 'Confirm Password', 'required|matches[new_pass]');
        //if form validation fails return to form again with error message
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('err_message_pass', 'Password do not match,Please try again.');
            $this->load->view('backend/auth/password_reset', $arr);
        } else {
            $user = $this->AuthModel->infoByEmail($email);
            $update_data = array(
                'authcode' => '',
                'password' => $password
            );
            //if user exists and authCode matched then reset password & authcode
            $databaseauthCode = $this->FrontEndModel->getColumValue('users','user_email',$email,'authCode');

            if ($user && trim($databaseauthCode)==$authCode) {
                $this->db->where('user_email', $email);
                $this->db->update('users', $update_data);
                $this->session->set_userdata('success_msg', 'You have successfully changed your password.');
                redirect('passwordSuccess');
            } else {
                $this->session->set_userdata('error_msg', 'Your password doesn\'t changed !! Try another reset link');
                redirect('passwordSuccess');
            }
        }
    }

   /**
     * Contact us functionality
     * ueses for page view and to save message
     *
     * @return string save message on
     */

   public function contactUs()
   {
    $post = $this->input->post();
    if (count($post)) {
        $dataToInsert = [
            'name'           => $post['name'],
            'email'          => $post['email'],
            'contact_method' => $post['contact_method'],
            'message'        => $post['message'],
            'contacted'      => 0, 
                //not contacted
        ];
        $this->AuthModel->insert('contact_user', $dataToInsert);
        sendEmail($subject = 'Contact User', $post['message'], $post['email'],ADMIN_EMAIL);
        echo '1';
    } else {
        $main['content'] = $this->load->view('frontend/pages/contact_us', '', true);
        $this->load->view('frontend/templates/master', $main);
    }
}

public function test()
{
    $main['content'] = $this->load->view('test', '', true);
    $this->load->view('frontend/templates/master', $main);
}


}
