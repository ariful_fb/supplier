<?php
defined('BASEPATH') or exit('No direct script access allowed');

class FrontendController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('FrontEndModel');
        $this->load->library('form_validation');
    }


    
    public function searchSkill()
    {
        $query = $this->input->get('query');
        $this->db->select('name as name');
        $this->db->like('name', $query);
        $data = $this->db->get("skills")->result();
        echo json_encode($data);
    }


    /**
     * Search for supplier based on main_city, state, skill or/and keyword
     * I've just written too too much costly function
     *
     * @return string page items
     */
    public function searchSupplier($searchVal = '', $param = '')
    {
        searchSupplier($searchVal, $param);
    }

    public function hasReliability(){
        $post = $this->input->post('reliability');
        if($post)
            echo "true";
        else 
            echo "false";
    }
    public function hasFlexibility(){
        $post = $this->input->post('flexibility');
        if($post)
            echo "true";
        else 
            echo "false";
    }
    public function hasCost()
    {
        $post = $this->input->post('cost');
        if($post)
            echo "true";
        else 
            echo "false";
    }
    public function hasGlobal_quality()
    {
        $post = $this->input->post('global_quality');
        if($post)
            echo "true";
        else 
            echo "false";
    }
    public function hasPeople_skills()
    {
        $post = $this->input->post('people_skills');
        if($post)
            echo "true";
        else 
            echo "false";
    }

    public function userData()
    {
        $link = $this->input->post('link');
        $post = $this->input->post('data');
        //prdie($post);
        $data['nick_name'] = $post[0]['value'];
        $data['privacy1'] = $post[1]['value'];
        $data['privacy2'] = $post[2]['value'];
        $data['privacy3'] = $post[3]['value'];
        $insertId = $this->FrontEndModel->insert('users',$data);
        $this->session->insertIdUser = $insertId;
        echo $link;

    }


}
