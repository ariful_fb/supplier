<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EvaluationController extends CI_Controller
{
    public $loggedUserId;
    public function __construct($value = '')
    {
        parent::__construct();
        $this->loggedUserId = $this->session->userdata('id');

        $this->load->model('EvaluationModel');
        $this->load->model('FrontEndModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $this->load->view('frontend/templates/master');
    }

    /**
     * All evaluation will be seen by admin
     *
     * @return void
     */
    public function allEvaluation()
    {
        $data['evaluation']=$this->EvaluationModel->allEvaluation('evaluation');
        $main['title']  ='Evaluation';
        $main['page']  = $this->load->view('backend/evaluation/all_evaluation', $data, true);
        $this->load->view('backend/index', $main);
    }

    public function newEvaluation()
    {
        $data['evaluation']=$this->EvaluationModel->newEvaluation('evaluation');
        $main['title']  ='Evaluation';
        $main['page']  = $this->load->view('backend/evaluation/new_evaluation', $data, true);
        $this->load->view('backend/index', $main);
    }

    public function rejectEvaluation($id){
        $data['is_approved']=0;
        $this->AdminModel->updateData('evaluation','id',$id,$data);
        redirect('evaluation/all');
    }
    public function approveEvaluation($id){
        $data['is_approved']=1;
        $this->AdminModel->updateData('evaluation','id',$id,$data);
        redirect('admin/new_evaluation');
    }

    
    public function addEvaluation()
    {

        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        //echo "<pre>";print_r($clean);die;
        if (!$post) {
            $main['page']  = $this->load->view('frontend/evaluation/add_evaluation', '', true);
            $this->load->view('frontend/index', $main);
        } else {
            //$evaluation['username']=$clean['username'];
            $name['nick_name']=$clean['username'];
            $hasName=$this->FrontEndModel->getData('users', 'nick_name',$name['nick_name']);

            if(!$hasName){
                $name_insert_id=$this->FrontEndModel->insert('users', $name);
            }else{
                $name_insert_id=$hasName['id'];
            }
            $evaluation['supplier_id']= $name_insert_id;
            $evaluation['disclaimer']=$clean['disclaimer'];
            $evaluation['inductions']=$clean['induction'];
            $evaluation['activity_type']=$clean['activity_type'];
            $evaluation['activity_duration']=$clean['activity_duration'];
            $evaluation['budget']=$clean['budget'];
            $evaluation['customer_company']=$clean['customer_company'];
            $evaluation['avg_rating']=($clean['reliability']+$clean['flexibility']+$clean['cost']+$clean['global_quality']+$clean['people_skills']+$clean['supplier_people']+$clean['adapt']+$clean['engagement']+$clean['knowledge']+$clean['propositivity']+$clean['proactivity'])/11;
            $evaluation_insert_id=$this->FrontEndModel->insert('evaluation', $evaluation);
            if( $evaluation_insert_id){
                $add_skill=$clean['add_skill'];
                $suppler_skill['supplier_id']=$name_insert_id;
                for ($i=0; $i <count($add_skill) ; $i++) {
                    $suppler_skill['skill_id']=$add_skill[$i];
                    $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
                }
                $skill=$clean['skill'];
                unset($skill[0]);
                for ($i=1; $i <count($skill) ; $i++) {
                    $skills['skill_id']=$skill[$i];
                    $this->FrontEndModel->insert('skills', $skills);
                }


                $evaluation_rating['reliability']=$clean['reliability'];
                $evaluation_rating['flexibility']=$clean['flexibility'];
                $evaluation_rating['cost']=$clean['cost'];
                $evaluation_rating['global_quality']=$clean['global_quality'];
                $evaluation_rating['people_skills']=$clean['people_skills'];
                $evaluation_rating['supplier_people']=$clean['supplier_people'];
                $evaluation_rating['adapt']=$clean['adapt'];
                $evaluation_rating['engagement']=$clean['engagement'];
                $evaluation_rating['knowledge']=$clean['knowledge'];
                $evaluation_rating['propositivity']=$clean['propositivity'];
                $evaluation_rating['proactivity']=$clean['proactivity'];
                $this->FrontEndModel->insert('evaluation_rating', $evaluation_rating);
            }




            $this->session->set_flashdata('success_msg', 'Evaluation Added Successfully');
            redirect('home');
        }
    }


    public function autoSearch()
    {
        $query = $this->input->get('query');
        $this->db->select('nick_name as name');
        $this->db->like('nick_name', $query);
        $data = $this->db->get("users")->result();
        echo json_encode( $data);
    }
}
