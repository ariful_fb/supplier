<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EvaluationController extends CI_Controller
{
    public $loggedUserId;
    public function __construct($value = '')
    {
        parent::__construct();
        $this->loggedUserId = $this->session->userdata('id');

        $this->load->model('EvaluationModel');
        $this->load->model('FrontEndModel');
        $this->load->model('AdminModel');
    }

    public function index()
    {
        $this->load->view('frontend/templates/master');
    }

    /**
     * All evaluation will be seen by admin
     *
     * @return void
     */
    public function allEvaluation()
    {
        $data['evaluation']=$this->EvaluationModel->allEvaluation('evaluation');
        $main['title']  ='Evaluation';
        $main['page']  = $this->load->view('backend/evaluation/all_evaluation', $data, true);
        $this->load->view('backend/index', $main);
    }

    public function newEvaluation()
    {
        $data['evaluation']=$this->EvaluationModel->newEvaluation('evaluation');

        //echo "<pre>";print_r($data);die;
        $main['title']  ='Evaluation';
        $main['page']  = $this->load->view('backend/evaluation/new_evaluation', $data, true);
        $this->load->view('backend/index', $main);
    }

    public function rejectEvaluation($id)
    {
        $data['is_approved']=0;
        $this->AdminModel->updateData('evaluation', 'id', $id, $data);
        redirect('evaluation/all');
    }
    public function approveEvaluation($id)
    {
        $data['is_approved'] = 1;
        //if supplier is not approved than first approved supplier than supplier evaluation
        $supplierId = $this->AdminModel->getColomData('evaluation', 'id', $id,'supplier_id');
        if($supplierId){
            $isApproved = $this->AdminModel->getColomData('users', 'user_id', $supplierId,'approved');
            if($isApproved == 0){
                $approved['approved'] = 1;
                $this->AdminModel->updateData('users', 'user_id', $supplierId, $approved);
            }
        }
        //prdie($isApproved);
        $this->AdminModel->updateData('evaluation', 'id', $id, $data);
        redirect('admin/new_evaluation');
    }


    public function addEvaluation()
    {

        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        //prdie($clean);
        if (!$post) {
            $main['page']  = $this->load->view('frontend/evaluation/add_evaluation', '', true);
            $this->load->view('frontend/index', $main);
        } else {

            $name['company_name']=$clean['username'];
            $hasName=$this->FrontEndModel->getData('supplier_info', 'company_name', $name['company_name']);

            if (!$hasName) {

                $nickName['role'] = 3;
                $name_insert_id=$this->FrontEndModel->insert('users', $nickName);
                $name['supplier_id']= $name_insert_id;
                $this->FrontEndModel->insert('supplier_info', $name);
            } else {
                $supplierInfo=$this->FrontEndModel->getData('users', 'user_id', $hasName['supplier_id']);
                $name_insert_id=$supplierInfo['user_id'];
                $supplier_email=$supplierInfo['user_email'];
            }

            $evaluation['creator_id']= $this->session->user_id;
            if(empty($evaluation['creator_id'])){
                $this->session->set_userdata('error_msg','User Session Data Missing.');
                redirect('home');
            }
            $evaluation['supplier_id']= $name_insert_id;
            $evaluation['inductions']=strip_tags($clean['induction']);
            $evaluation['activity_type']=$clean['activity_type'];
            $evaluation['activity_duration']=$clean['activity_duration'];
            $evaluation['budget']=$clean['budget'];
            $evaluation['customer_company']=$clean['customer_company'];
            $evaluation['avg_rating']=($clean['reliability'] + $clean['flexibility'] + $clean['cost']+ $clean['global_quality'] + $clean['people_skills']) / 5;

             //get supplier info
            $Rating=$this->FrontEndModel->getRating($name_insert_id);
            if ($Rating) {
                $Rating=$Rating[0];
            }
            $rating['rating']=($Rating['totRating'] + $evaluation['avg_rating']) /  ($Rating['totEvaluation'] + 1);
            $this->AdminModel->updateData('users', 'user_id', $name_insert_id, $rating);


            $evaluation_insert_id=$this->FrontEndModel->insert('evaluation', $evaluation);

            if ($evaluation_insert_id) {
                $suppler_skill['user_id'] =$this->session->user_id;
                $suppler_skill['evaluation_id'] =$evaluation_insert_id;
                //$add_skill = empty($clean['add_skill'])?array():$clean['add_skill'];
                $suppler_skill['supplier_id'] = $name_insert_id;

                // for ($i=0; $i <count($add_skill); $i++) {
                //     $suppler_skill['skill_id'] = $add_skill[$i];
                //     $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
                // }

                //Add Skill
                $skill=$clean['skill'];
                if (!empty($skill[0])) {
                    for ($i=0; $i < count($skill); $i++) {
                        if(is_numeric($skill[$i])){
                            $suppler_skill['skill_id'] = $skill[$i];
                            $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
                        }else{
                            $skills['name'] = $skill[$i];
                            $insert_id = $this->FrontEndModel->insert('skills', $skills);

                            $suppler_skill['skill_id'] = $insert_id;
                            $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
                        }
                        
                    }
                }


                $evaluation_rating['reliability']=$clean['reliability'];
                $evaluation_rating['flexibility']=$clean['flexibility'];
                $evaluation_rating['cost']=$clean['cost'];
                $evaluation_rating['global_quality']=$clean['global_quality'];
                $evaluation_rating['people_skills']=$clean['people_skills'];
                $evaluation_rating['supplier_people']=$clean['supplier_people'];
                $evaluation_rating['adapt']=$clean['adapt'];
                $evaluation_rating['engagement']=$clean['engagement'];
                $evaluation_rating['knowledge']=$clean['knowledge'];
                $evaluation_rating['propositivity']=$clean['propositivity'];
                $evaluation_rating['proactivity']=$clean['proactivity'];
                $evaluation_rating['evaluation_id']=$evaluation_insert_id;
                $this->FrontEndModel->insert('evaluation_rating', $evaluation_rating);
            }


            $body = $this->load->view('email_templates/new_eval_to_supplier', '', true);
            $subject = 'New Evaluation Added';
            sendEmail($subject, $body, ADMIN_EMAIL, 'ariful.fb@gmail.com');

            $body = $this->load->view('email_templates/new_eval_to_supplier', '', true);
            sendEmail($subject, $body, ADMIN_EMAIL, ADMIN_EMAIL);

            $this->session->set_flashdata('success_msg', 'Evaluation Added Successfully');
            redirect('user/home');
        }
    }


    public function autoSearch()
    {
        $query = $this->input->get('query');
        $this->db->select('company_name as name');
        $this->db->like('company_name', $query);
        $data = $this->db->get("supplier_info")->result();
        echo json_encode($data);
    }

    public function evalRecapData($evaluationId)
    {
        $evalData =  $this->FrontEndModel->search('evaluation_rating', ['evaluation_id'=>$evaluationId]);
        $html = '';
        if (count($evalData)) {
            $data = $evalData[0];
            $processData['reliability'] = round((($data['reliability']/5)*100), 2);
            $processData['flexibility'] = round((($data['flexibility']/5)*100), 2);
            $processData['cost'] = round((($data['cost']/5)*100), 2);
            $processData['global_quality'] = round((($data['global_quality']/5)*100), 2);
            $processData['people_skills'] = round((($data['people_skills']/5)*100), 2);

            $html .= '<table class="table table-borderless">
            <tbody>';

            foreach ($processData as $key => $value) {
                $html .= '<tr>
                <td>'.$key.'</td>

                <td width="170px">
                <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: '.$value.'%;" aria-valuenow="'.$value.'" aria-valuemin="0" aria-valuemax="100">'.$value.'%</div>
                </div>
                </td>

                <td>'.$value.'%</td>
                </tr>';
            }

            $html .= '</tbody></table>';
        } else {
            $html .= 'Evaluation not found';
        }
        echo $html;
    }
}
