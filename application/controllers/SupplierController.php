<?php
defined('BASEPATH') or exit('No direct script access allowed');
class SupplierController extends CI_Controller
{
    public $role,$userId;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('FrontEndModel');
        $this->role = $this->session->role;
        $this->userId = $this->session->user_id;
    }

    public function index()
    {
        $this->load->view('welcome_message');
    }


    /**
     * Supplier search from front page
     * Ajax call
     *
     * @return void
     */
    public function search()
    {
        $post = $this->input->post();
        $clean = $this->security->xss_clean($post);
        $data['paramCount'] = count(array_filter($clean));
        $supByCity = $this->FrontEndModel->search('supplier_info', ['main_city' => $clean['city'] ]);
        $supByState = $this->FrontEndModel->search('supplier_info', ['state' => $clean['state'] ]);
        $supIdsBySkill = $this->FrontEndModel->search('supplier_skills', ['skill_id' => $clean['skill'] ]);
        $supIdsBySkill = array_column($supIdsBySkill, 'supplier_id');

        $whereInParam = [
            'key'=>'supplier_id',
            'val'=>$supIdsBySkill
        ];

        $supByUserIds = $this->FrontEndModel->searchSupplier([], $whereInParam);
        $supByKeyword = $this->FrontEndModel->supplierByKeyword($clean['keyword']);

        $finalSupArr = [];
        foreach ($supByCity as $supplier) {
            $supplier['occurance'] = isset($supplier['occurance']) ? $supplier['occurance']+1 : 1 ;
            
            $finalSupArr[$supplier['supplier_id']] = $supplier;
        }
        foreach ($supByState as $supplier) {
            $supplier['occurance'] = isset($supplier['occurance']) ? $supplier['occurance']+1 : 1;
            $finalSupArr[$supplier['supplier_id']] = $supplier;
        }
        foreach ($supByUserIds as $supplier) {
            $supplier['occurance'] = isset($supplier['occurance']) ? $supplier['occurance']+1 : 1 ;
            $finalSupArr[$supplier['supplier_id']] = $supplier;
        }
        foreach ($supByKeyword as $supplier) {
            $supplier['occurance'] = isset($supplier['occurance']) ? $supplier['occurance']+1 :1 ;
            $finalSupArr[$supplier['supplier_id']] = $supplier;
        }
        usort($finalSupArr, [$this, 'comp']);
        
        $data['suppliers'] = $finalSupArr;//$this->FrontEndModel->searchSupplier($params);

        $data['content'] = $this->load->view('frontend/supplier/search_result', $data, true);
        echo $data['content'];
    }


    


    public function comp($arr1, $arr2)
    {
        //return $arr1['occurance'] > $arr2['occurance'] ? $arr1['occurance'] : $arr2['occurance'];
        return $arr1 > $arr2 ? $arr1 : $arr2;
    }

    /**
     * Supplier Details
     *
     * @param integer $supplierId supplier id
     *
     * @return void
     */
    /*public function supplierDetails($supplierId)
    {

        $params = ['user_id'=>$supplierId];
        $data['supplier'] = $this->FrontEndModel->searchSupplier($params);

        if (!$data['supplier']) {
            show_404();
        } else {
            $data['supplier']=$data['supplier'][0];
            $data['radarData'] = $this->radarDiagramData($supplierId);
        }
        $main['content'] = $this->load->view('frontend/supplier/supplier_info', $data, true);
        $this->load->view('frontend/templates/master', $main);
    }*/

    public function supplierInfo($supplierId)
    {
        $this->session->supplierId=$supplierId;
        $data['supplier'] = $this->FrontEndModel->getSupplierInfo($supplierId);
        $data['skill_data'] = $this->FrontEndModel->getSupplierSkills($supplierId);
        $data['user_evaluation'] = $this->FrontEndModel->getSupplierEvaluation($supplierId);

        for ($i=0; $i < count($data['user_evaluation']); $i++) {
            $data['user_evaluation'][$i]['skill']=$this->FrontEndModel->getEvaluationSkill($data['user_evaluation'][$i]['id']);
        }


        $param=true;
        $searchVal['city']=$this->session->userdata('city');
        $searchVal['state']=$this->session->userdata('state');
        $searchVal['skill']=$this->session->userdata('skill');
        $searchVal['keyword']=$this->session->userdata('keyword');
        
        $res=searchSupplier($searchVal, $param);
        if (!empty($res)) {
            $data['suppliers'] = $res['suppliers'];
        } else {
            $data['suppliers'] = $this->FrontEndModel->getAllSupplier();
        }
        
        if (!$data['supplier']) {
            show_404();
        } else {
            $data['supplier'] = $data['supplier'][0];
            $otherCountries = $data['supplier']['other_countries'];
            $countryIds = json_decode($otherCountries);
            $data['otherCountries'] = $this->getOtherCountries($countryIds);
            $data['radarData'] = $this->radarDiagramData($supplierId);
            $data['recapData'] = $this->recapData($supplierId);
            $data['supplierId'] = $supplierId;
        }

        $main['content'] = $this->load->view('frontend/supplier/supplier_info', $data, true);
        $this->load->view('frontend/templates/master', $main);
    }

    /**
     * Get other countries for a supplier from supplier_info table.
     *
     * @param array $countryIds countries
     *
     * @return array             country names
     */
    public function getOtherCountries($countryIds)
    {

        if (!is_array($countryIds) || !count($countryIds)) {
            return "Not present";
        } else {
            $otherCountries = $this->FrontEndModel->search('countries', [], "id", $countryIds);
            $otherCountries = array_column($otherCountries, 'country_name');
            return implode(',', $otherCountries);
        }
    }

    public function evaluationDetails($evaluationId)
    {

        /*if ($this->role==3) {
            $data['evaluation']=$this->FrontEndModel->getEvaluation($evaluationId, $this->userId);
        } else {
            $data['evaluation']=$this->FrontEndModel->getEvaluation($evaluationId);
        }*/

        $data['evaluation']=$this->FrontEndModel->getEvaluation($evaluationId);
        
        if (empty($data['evaluation'])) {
            redirect('supplier/home');
        }
        $data['skills'] = $this->FrontEndModel->getEvaluationSkill($evaluationId);

        $main['content'] = $this->load->view('frontend/user/details', $data, true);
        $this->load->view('frontend/templates/master', $main);
    }

    public function supplierIndex()
    {
        /*$supplierId = $this->session->user_id;
        $data['supplier'] = $this->FrontEndModel->getSupplierInfo($supplierId);
        $data['skill_data'] = $this->FrontEndModel->getSupplierSkills($supplierId);
        $data['user_evaluation'] = $this->FrontEndModel->getSupplierEvaluation($supplierId);

        for ($i=0; $i < count($data['user_evaluation']); $i++) {
            $data['user_evaluation'][$i]['skill']=$this->FrontEndModel->getSupplierSkills($data['user_evaluation'][$i]['creator_id'], $data['user_evaluation'][$i]['id']);
        }
        $data['supplier']=$data['supplier'][0];
        $data['radarData'] = $this->radarDiagramData($supplierId);
        $data['supplierId'] = $supplierId;
        $main['content'] = $this->load->view('frontend/supplier/home', $data, true);
        $this->load->view('frontend/templates/master', $main);*/
        $this->session->supplierId=$supplierId=$this->session->user_id;
        ;
        $data['supplier'] = $this->FrontEndModel->getSupplierInfo($supplierId);
        $data['skill_data'] = $this->FrontEndModel->getSupplierSkills($supplierId);
        $data['user_evaluation'] = $this->FrontEndModel->getSupplierEvaluation($supplierId);

        for ($i=0; $i < count($data['user_evaluation']); $i++) {
            $data['user_evaluation'][$i]['skill']=$this->FrontEndModel->getEvaluationSkill($data['user_evaluation'][$i]['id']);
        }


        $param=true;
        $searchVal['city']=$this->session->userdata('city');
        $searchVal['state']=$this->session->userdata('state');
        $searchVal['skill']=$this->session->userdata('skill');
        $searchVal['keyword']=$this->session->userdata('keyword');
        
        $res=searchSupplier($searchVal, $param);
        if (!empty($res)) {
            $data['suppliers'] = $res['suppliers'];
        } else {
            $data['suppliers'] = $this->FrontEndModel->getAllSupplier();
        }
        
        if (!$data['supplier']) {
            show_404();
        } else {
            $data['supplier'] = $data['supplier'][0];
            $otherCountries = $data['supplier']['other_countries'];
            $countryIds = json_decode($otherCountries);
            $data['otherCountries'] = $this->getOtherCountries($countryIds);
            $data['radarData'] = $this->radarDiagramData($supplierId);
            $data['recapData'] = $this->recapData($supplierId);
            $data['supplierId'] = $supplierId;
        }

        $main['content'] = $this->load->view('frontend/supplier/home', $data, true);
        $this->load->view('frontend/templates/master', $main);
    }

    public function messageToContact()
    {
        /*get approved supplier*/
        $post=$this->input->post();
        if (!$post) {
            $data['supplier_email']=$this->FrontEndModel->getWhereTwo('users', 'role', SUPPLIER, 'approved', 1);
            $main['content'] = $this->load->view('frontend/message/message_to_contact', $data, true);
            $this->load->view('frontend/templates/master', $main);
        } else {
            $clean=$this->security->xss_clean($post);
            $emailList=$clean['email_list'];

            $data['d']=1;
            $body = $this->load->view('email_templates/request_sup_to_sup', '', true);
            $subject = 'Registration confirmation - USER';
            for ($i=1; $i < count($emailList); $i++) {
                sendEmail($subject, $body, $this->session->user_email, $emailList[$i]);
            }
            $this->session->set_flashdata('success_msg', 'Send message successfull.');
            redirect('message_to_contact');
        }
    }

    public function addAnswer()
    {

        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        $detailsId=$clean['evaluationId'];
        unset($clean['evaluationId']);
        $this->FrontEndModel->updateData('evaluation', 'id', $detailsId, $clean);
        redirect('user_evaluation/details/'.$detailsId);
    }

    public function addRequest()
    {
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        //print_r($clean);die;
        $detailsId=$clean['evaluation_id'];
        $this->FrontEndModel->insert('notifications', $clean);

        $emailTempData = [
            'reason'    => $clean['reason'],
            'evaluation_id' => $clean['evaluation_id'],
        ];
        $body = $this->load->view('email_templates/gdrp_admin_evaluation', $emailTempData, true);
        $subject = 'Request To Delete Evaluation';
        $this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = true;
        $config['mailtype'] ='html';

        $this->email->initialize($config);
        $this->email->from('arifhstu41@gmail.com', 'look4supplier');
        $this->email->to('ariful.fb@gmail.com');


        $this->email->subject($subject);
        $this->email->message($body);

        $this->email->send();

        redirect('user_evaluation/details/'.$detailsId);
    }

    /**
     * Produce data for radar diagram
     *
     * @param integer $supplierId supplier id
     *
     * @return string json encoded string
     */
    /*public function radarDiagramData($supplierId)
    {
        //get all evaluation ids by supplier id
        $evaluation = $this->FrontEndModel->search('evaluation', ['supplier_id'=>$supplierId]);
        $evaluationIds = array_column($evaluation, 'id');

        //get all from evaluation_rating table
        $whereInKey = 'evaluation_id';
        $whereInValue = $evaluationIds;
        $allRatingData = $this->FrontEndModel->search('evaluation_rating', [], $whereInKey, $whereInValue);

        //count all 5's,4's rating
        $ratings = [];
        foreach ($allRatingData as $data) {
            unset($data['id']);
            unset($data['evaluation_id']);
            foreach ($data as $key => $value) {
                if ($value) {
                    $ratings[] = $value;
                }
            }
        }
        $ratingOccurance = array_count_values($ratings);
        krsort($ratingOccurance);
        $keys = ['', 'Reliability', 'Flexibility', 'Cost/quality', 'Global quality of the work', 'Supplier’s people skills'];

        foreach (range(1, 5) as $range) {
            if (array_key_exists($range, $ratingOccurance)) {
                //copy old key values then store and unset
                $ratingOccurance[$keys[$range]] = $ratingOccurance[$range];
                unset($ratingOccurance[$range]);
            } else {
                $ratingOccurance[$keys[$range]] = 0;
            }
        }
        return json_encode(($ratingOccurance));
    }
    */
    public function radarDiagramData($supplierId)
    {
        //get all approved evaluation ids by supplier id
        $conditions = [
            'supplier_id'=>$supplierId,
            'is_approved'=>1
        ];
        $evaluation = $this->FrontEndModel->search('evaluation', $conditions);
        $evaluationIds = array_column($evaluation, 'id');

        //get all from evaluation_rating table
        $whereInKey = 'evaluation_id';
        $whereInValue = $evaluationIds;
        if (count($whereInValue)) {
            $allRatingData = $this->FrontEndModel->search('evaluation_rating', [], $whereInKey, $whereInValue);
        } else {
            $allRatingData = [];
        }

        $numOfEval = count($allRatingData);
        $ratingData = array(
            'reliability'    => 0,
            'flexibility'    => 0,
            'cost'           => 0,
            'global_quality' => 0,
            'people_skills'  => 0,
        );
        foreach ($allRatingData as $data) {
            $ratingData['reliability']    += $data['reliability'];
            $ratingData['flexibility']    += $data['flexibility'];
            $ratingData['cost']           += $data['cost'];
            $ratingData['global_quality'] += $data['global_quality'];
            $ratingData['people_skills']  += $data['people_skills'];
        }

        if ($numOfEval) {
            foreach ($ratingData as $key => $value) {
                $ratingData[$key] = round((float)$value/$numOfEval, 2);
            }
        }
        return json_encode($ratingData);
    }

    /**
     * Dumbest function
     *
     * @param supplier $supplierId supplier ID
     *
     * @return array                rating count in range
     */
    public function recapData($supplierId)
    {
        //$data = $this->FrontEndModel->recapData($supplierId);
        $data[1] = $this->FrontEndModel->getRecapAvg($supplierId, 'avg_rating BETWEEN 0.11 AND 1.41');//0.1-1.4
        $data[2] = $this->FrontEndModel->getRecapAvg($supplierId, 'avg_rating BETWEEN 1.51 AND 2.41');//1.5-2.4
        $data[3] = $this->FrontEndModel->getRecapAvg($supplierId, 'avg_rating BETWEEN 2.51 AND 3.41');//2.5-3.4
        $data[4] = $this->FrontEndModel->getRecapAvg($supplierId, 'avg_rating BETWEEN 3.51 AND 4.41');//3.5-4.4
        $data[5] = $this->FrontEndModel->getRecapAvg($supplierId, 'avg_rating BETWEEN 4.51 AND 5');  //4.5-5.0
        return $data;
    }

    public function test()
    {
        /*$main['content'] = $this->load->view('test', [], true);
        $this->load->view('frontend/templates/master', $main);*/
        $this->load->view('test');
    }
}
