<?php 
//defined('BASEPATH') OR exit('No direct script access allowed');
class User_Authentication extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AuthModel');
        $this->load->model('FrontEndModel');
        $this->load->model('AdminModel');
        $this->load->library('facebook');
        $this->load->library('google');

        $this->load->config('linkedin');
        $this->load->model('user');

        $this->load->library('form_validation');    
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    public function index(){
        $userData = array();
        
        //Include the linkedin api php libraries
        include_once APPPATH."libraries/oauth/http.php";
        include_once APPPATH."libraries/oauth/oauth_client.php";
        
        
        //Get status and user info from session
        $oauthStatus = $this->session->userdata('oauth_status');
        $sessUserData = $this->session->userdata('userData');
        
        if(isset($oauthStatus) && $oauthStatus == 'verified'){
            //User info from session
            $userData = $sessUserData;
        }elseif((isset($_REQUEST["oauth_init"]) && $_REQUEST["oauth_init"] == 1) || (isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier']))){
            $client = new oauth_client_class;
            $client->client_id = $this->config->item('linkedin_api_key');
            $client->client_secret = $this->config->item('linkedin_api_secret');
            $client->redirect_uri = base_url().$this->config->item('linkedin_redirect_url');
            $client->scope = $this->config->item('linkedin_scope');
            $client->debug = false;
            $client->debug_http = true;
            $application_line = __LINE__;
            
            //If authentication returns success
            if($success = $client->Initialize()){
                if(($success = $client->Process())){
                    if(strlen($client->authorization_error)){
                        $client->error = $client->authorization_error;
                        $success = false;
                    }elseif(strlen($client->access_token)){
                        $success = $client->CallAPI('http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 
                            'GET',
                            array('format'=>'json'),
                            array('FailOnAccessError'=>true), $userInfo);
                    }
                }
                $success = $client->Finalize($success);
            }
            
            if($client->exit) exit;

            if($success){
                //Preparing data for database insertion
                $first_name = !empty($userInfo->firstName)?$userInfo->firstName:'';
                $last_name = !empty($userInfo->lastName)?$userInfo->lastName:'';
                $userData = array(
                    'oauth_provider'=> 'linkedin',
                    'oauth_uid'     => $userInfo->id,
                    'first_name'     => $first_name,
                    'last_name'     => $last_name,
                    'email'         => $userInfo->emailAddress,
                    'locale'         => $userInfo->location->name,
                    'profile_url'     => $userInfo->publicProfileUrl,
                    'picture_url'     => $userInfo->pictureUrl
                );
                
                //Insert or update user data
                $userID = $this->user->checkUser($userData);
                
                //Store status and user profile info into session
                $this->session->set_userdata('oauth_status','verified');
                $this->session->set_userdata('userData',$userData);
                
                //Redirect the user back to the same page
                redirect('/user_authentication');

            }else{
             $data['error_msg'] = 'Some problem occurred, please try again later!';
         }
     }elseif(isset($_REQUEST["oauth_problem"]) && $_REQUEST["oauth_problem"] <> ""){
        $data['error_msg'] = $_GET["oauth_problem"];
    }else{
        $data['oauthURL'] = base_url().$this->config->item('linkedin_redirect_url').'?oauth_init=1';
    }

    $data['userData'] = $userData;

        // Load login & profile view
    $this->load->view('user_authentication/index',$data);
}


public function facebookLogin(){
    $userData = array();
        // Check if user is logged in

    $this->facebook->is_authenticated();
            // Get user facebook profile details
    $fbUserProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,locale,cover,picture');
            // Preparing data for database insertion
        //print_r($fbUserProfile);
       // echo "llkkkk=".$this->facebook->is_authenticated();die;
    $userData['oauth_provider'] = 'facebook';
    $userData['oauth_uid'] = $fbUserProfile['id'];
    $userData['user_email'] = $fbUserProfile['email'];

            // Insert or update user data
    if($this->session->insertIdUser){

        $userID = $this->AuthModel->checkUser($userData);
    }else{
        $userID = $this->AuthModel->checkUser2($userData);
    }

            // Check user data insert or update status
    if(!empty($userID)){
            // $this->session->set_userdata('email',$userData['email']);
        $userInfo=$this->AuthModel->socialLogin($userID);
        foreach ($userInfo as $key => $val) {
            $this->session->set_userdata($key, $val);
        }
        if($this->session->insertEvaluationId)
            $this->insertEvaluation($this->session->insertEvaluationId,$this->session->user_id);
            //$this->session->set_userdata('user_id', $userID);
        redirect('user/home');
    }else{
        $this->session->set_userdata('error_msg', 'Login Unseccessfull.Please Create an Account.');
        redirect('home');
    }

}





public function googleLogin(){
        //redirect to profile page if user already logged in
        //echo "gg= ".$this->session->insertEvaluationId;die;

    if(isset($_GET['code'])){
            //authenticate user
        $this->google->getAuthenticate();

            //get user info from google
        $gpInfo = $this->google->getUserInfo();

            //preparing data for database insertion
        $userData['oauth_provider'] = 'google';
        $userData['oauth_uid']      = $gpInfo['id'];
        $userData['user_email']          = $gpInfo['email'];
            //insert or update user data to the database
        if($this->session->insertIdUser){

            $userID = $this->AuthModel->checkUser($userData);
        }else{
            $userID = $this->AuthModel->checkUser2($userData);
        }


        if(!empty($userID)){
            $userInfo=$this->AuthModel->socialLogin($userID);
                //print_r($userInfo);die;
            foreach ($userInfo as $key => $val) {
                $this->session->set_userdata($key, $val);
            }
            if($this->session->insertEvaluationId)
                $this->insertEvaluation($this->session->insertEvaluationId,$this->session->user_id);
            redirect('user/home');
        }else{
            $this->session->set_userdata('error_msg', 'Login Unseccessfull.Please Create an Account.');
            redirect('home');
        }
    } 


}


public function insertEvaluation($id,$userId){
    $evaluationData = $this->FrontEndModel->getData('evaluation_demo','id',$id);
    $eva = json_decode($evaluationData['dimension']);
    $clean = json_decode(json_encode($eva), True);
    $this->FrontEndModel->deleteData('evaluation_demo','id',$id);


    $name['company_name']=$clean['username'];
    $hasName=$this->FrontEndModel->getData('supplier_info', 'company_name', $name['company_name']);

    if (!$hasName) {

        $nickName['role'] = 3;
        $name_insert_id=$this->FrontEndModel->insert('users', $nickName);
        $name['supplier_id']= $name_insert_id;
        $this->FrontEndModel->insert('supplier_info', $name);
    } else {
        $supplierInfo=$this->FrontEndModel->getData('users', 'user_id', $hasName['supplier_id']);
        $name_insert_id=$supplierInfo['user_id'];
        $supplier_email=$supplierInfo['user_email'];
    }



    $evaluation['creator_id']= $userId;
    if(empty($evaluation['creator_id'])){
        $this->session->set_userdata('error_msg','User Session Data Missing');
        redirect('home');
    }
    $evaluation['supplier_id']= $name_insert_id;
    $evaluation['inductions']=strip_tags($clean['induction']);
    $evaluation['activity_type']=$clean['activity_type'];
    $evaluation['activity_duration']=$clean['activity_duration'];
    $evaluation['budget']=$clean['budget'];
    $evaluation['customer_company']=$clean['customer_company'];
    $evaluation['avg_rating']=($clean['reliability'] + $clean['flexibility'] + $clean['cost']+ $clean['global_quality'] + $clean['people_skills']) / 5;

             //get supplier info
    $Rating=$this->FrontEndModel->getRating($name_insert_id);
    if ($Rating) {
        $Rating=$Rating[0];
    }
    $rating['rating']=($Rating['totRating'] + $evaluation['avg_rating']) /  ($Rating['totEvaluation'] + 1);

    $evaluation_insert_id=$this->FrontEndModel->insert('evaluation', $evaluation);

    if ($evaluation_insert_id) {
        $suppler_skill['user_id'] =$userId;
        $suppler_skill['evaluation_id'] =$evaluation_insert_id;
       //$add_skill = empty($clean['add_skill'])?array():$clean['add_skill'];
        $suppler_skill['supplier_id'] = $name_insert_id;

       /*for ($i=0; $i <count($add_skill); $i++) {
        $suppler_skill['skill_id'] = $add_skill[$i];
        $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
    }*/

         //Add Skill
    $skill=$clean['skill'];
    if (!empty($skill[0])) {
        for ($i=0; $i < count($skill); $i++) {
            if(is_numeric($skill[$i])){
                $suppler_skill['skill_id'] = $skill[$i];
                $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
            }else{
                $skills['name'] = $skill[$i];
                $insert_id = $this->FrontEndModel->insert('skills', $skills);

                $suppler_skill['skill_id'] = $insert_id;
                $this->FrontEndModel->insert('supplier_skills', $suppler_skill);
            }

        }
    }
    $evaluation_rating['reliability'] = $clean['reliability'];
    $evaluation_rating['flexibility'] = $clean['flexibility'];
    $evaluation_rating['cost'] = $clean['cost'];
    $evaluation_rating['global_quality'] = $clean['global_quality'];
    $evaluation_rating['people_skills'] = $clean['people_skills'];
    $evaluation_rating['supplier_people'] = $clean['supplier_people'];
    $evaluation_rating['adapt'] = $clean['adapt'];
    $evaluation_rating['engagement'] = $clean['engagement'];
    $evaluation_rating['knowledge'] = $clean['knowledge'];
    $evaluation_rating['propositivity'] = $clean['propositivity'];
    $evaluation_rating['proactivity'] = $clean['proactivity'];
    $evaluation_rating['evaluation_id'] = $evaluation_insert_id;
    $this->FrontEndModel->insert('evaluation_rating', $evaluation_rating);
}

    //echo "ok";die;
$body = $this->load->view('email_templates/new_eval_to_supplier', '', true);
$subject = 'New Evaluation Added';
sendEmail($subject, $body, ADMIN_EMAIL, 'ariful.fb@gmail.com');

$body = $this->load->view('email_templates/new_eval_to_supplier','', true);
sendEmail($subject, $body, ADMIN_EMAIL, ADMIN_EMAIL);

$this->session->set_flashdata('success_msg', 'Evaluation Added Successfully');
redirect('user/home');

}











}