<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
        $this->load->model('FrontEndModel');
        $this->load->library('form_validation');
    }

    public function viewNotifications()
    {
        $data['notification']=$this->AdminModel->getNotification();
        $main['page']  = $this->load->view('backend/notification/notification', $data, true);
        $this->load->view('backend/index', $main);
    }

    public function deleteNotification($id)
    {
        $chkValid=$this->AdminModel->getData('notifications', 'id', $id);
        if ($chkValid) {
            if ($this->AdminModel->deleteData('notifications', 'id', $id)) {
                //$evaluation=$this->AdminModel->getData('evaluation', 'evaluation_id', $chkValid['evaluation_id']);
                $this->session->set_userdata('success_msg', 'Delete Successful.');
            }
        } else {
            $this->session->set_userdata('error_msg', 'Something Wrong!');
        }
        redirect('admin/notifications');
    }

    public function notificationDetails($id)
    {
        $notification = $this->FrontEndModel->getData('notifications', 'id', $id);
        echo json_encode($notification);
    }

    public function manageEvaluation()
    {
        $data['evaluation']=$this->AdminModel->get('evaluation');
        $main['page']  = $this->load->view('backend/evaluation/manage_evaluation', $data, true);
        $this->load->view('backend/index', $main);
    }

    public function evaluationDetails($id)
    {
        $data['evaluation']=$this->FrontEndModel->getEvaluation($id);
        $conditions = ['evaluation_id'=>$id];
        $data['skills'] = $this->FrontEndModel->getEvaluationSkill($id);
        $main['page']  = $this->load->view('backend/evaluation/details', $data, true);
        $this->load->view('backend/index', $main);
    }
    public function deleteRequest($id)
    {
        $this->deleteEvaluation($evalId,$deleteFromNotification=1);
        $this->session->set_userdata('success_msg', 'Delete Successful.');
        redirect('admin/notifications');

    }

    public function deleteEvaluation($evalId,$deleteFromNotification='')
    {
        //get related supplier
        $evaluation = $this->FrontEndModel->search('evaluation', ['id'=>$evalId]);
        if (!$evaluation) {
            return 0;
        }
        $supplierId = $evaluation[0]['supplier_id'];
        //delete data from evaluation_rating table
        $this->AdminModel->deleteData('evaluation_rating', 'evaluation_id', $evalId);
        //delete data from evaluation table
        $this->AdminModel->deleteData('evaluation', 'id', $evalId);
        //Get updated average rating of supplier
        $avgRating = $this->FrontEndModel->supplierRating($supplierId);
        //update suppliers average evaluation
        $user = $this->FrontEndModel->search('users', ['user_id'=>$supplierId]);
        $user[0]['rating'] = $avgRating;
        $this->AdminModel->updateData('users', 'user_id', $supplierId, $user[0]);
    }


    /**
     * All approved suppliers
     *
     * @return void
     */
    public function approvedSuppliers()
    {
        $data['approved_suppliers']=$this->AdminModel->getApprovedSupplier();
        $data['industry']=$this->FrontEndModel->get('industry');
        

        $main['page']  = $this->load->view('backend/supplier/approved_supplier', $data, true);
        $main['title']  = 'Supplier';
        $this->load->view('backend/index', $main);
    }

    /**
     * All new suppliers list
     *
     * @return void
     */
    public function newSupplier()
    {
        $data['new_suppliers']=$this->AdminModel->getNewSupplier();
        $main['page']  = $this->load->view('backend/supplier/new_supplier', $data, true);

        $main['title']  = 'Supplier';
        $this->load->view('backend/index', $main);
    }
    


    /**
     * Change supplier status to approved,not approved, rejected
     * ajax call
     *
     * @param integer $userId user id to perform action
     * @param integer $status 0=not approved, 1=approved, 2=rejected
     *
     * @return string      1==status changed, 0== not changed
     */
    public function supplierStatus($userId, $status)
    {
        $user = $this->FrontEndModel->search('users', ['user_id'=>$userId]);
        if (!$user) {
            show_404();
        } else {
            $dataToUpdate = [
                'approved' => $status,
            ];
            $updStatus = $this->AdminModel->updateData('users', 'user_id', $userId, $dataToUpdate);
            if ($status==1) {
                mail($user[0]['user_email'], 'Supplier Evaluation', 'Your registration has been approved');
            }
            echo $updStatus ? '1' : '0';
        }
    }

    public function hideSupplier($id)
    {
        $data['supplier_hidden']=1;
        $this->AdminModel->updateData('users', 'user_id', $id, $data);
        redirect('admin/approved_suppliers');
    }

    public function showSupplier($id)
    {
        $data['supplier_hidden']=0;
        $this->AdminModel->updateData('users', 'user_id', $id, $data);
        redirect('admin/approved_suppliers');
    }


    public function deleteSupplier($id, $ajaxReq = 0)
    {
        $chkValid=$this->AdminModel->getData('users', 'user_id', $id);
        if ($chkValid) {
            if ($this->AdminModel->deleteData('users', 'user_id', $id)) {
                //if supplier info exists then delete
                $supplierInfo=$this->AdminModel->getData('supplier_info', 'supplier_id', $id);
                if ($supplierInfo) {
                    $this->AdminModel->deleteData('supplier_info', 'supplier_id', $id);
                }
                
                //terminate if ajax request
                if ($ajaxReq==1) {
                    return 0;
                }
                $this->session->set_userdata('success_msg', 'Delete Successful.');
            } else {
                $this->session->set_userdata('error_msg', 'User not ddeleted');
            }
        } else {
            $this->session->set_userdata('error_msg', 'Something Wrong!');
        }
        redirect('admin/approved_suppliers');
    }

    public function updateSupplier()
    {
        $insertId = array();
        $deleteId = array();
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        if (!empty($clean['password'])) {
            $data['password']=md5($clean['password']);
        }
        if (!isset($clean['skill'])) {
            $clean['skill']=array();
        }
        $s_skill=$this->FrontEndModel->getSupplierSkills($clean['user_id'], 'rt_array');
        //Insert new skill
        $supplierSkill=array_column($s_skill, 'id');
        $insertId = array_diff($clean['skill'], $supplierSkill);
        if (!empty($insertId)) {
            $i=0;
            foreach ($insertId as $key => $value) {
                $insert_batch[$i]['skill_id'] = $value;
                $insert_batch[$i]['supplier_id'] = $clean['user_id'];
                $i++;
            }
            $this->FrontEndModel->insert_batch('supplier_skills', $insert_batch);
        }
        $deleteId = array();
        $deleteId = array_diff($supplierSkill, $clean['skill']);

        if (!empty($deleteId)) {
            foreach ($deleteId as $key => $value) {
               // $this->FrontEndModel->deleteSupplier($clean['user_id'], $value);
                $this->FrontEndModel->deleteData('supplier_skills', 'id', $value);
            }
        }
        $data['nick_name']=$clean['nick_name'];
        $data['user_email']=$clean['user_email'];
        $data['role']=3;
        $userId=$clean['user_id'];
        unset($clean['nick_name']);
        unset($clean['password']);
        unset($clean['user_email']);
        unset($clean['user_id']);
        unset($clean['skill']);
        $clean['other_countries']=json_encode($clean['countries']);
        unset($clean['countries']);

        $res=$this->AdminModel->updateData('users', 'user_id', $userId, $data);

        if ($res) {
            $res=$this->FrontEndModel->getData('supplier_info', 'supplier_id', $userId);

            if ($res) {
                $this->AdminModel->updateData('supplier_info', 'supplier_id', $userId, $clean);
            } else {
                $clean['supplier_id']=$userId;
                $this->FrontEndModel->insert('supplier_info', $clean);
            }
            $this->session->set_userdata('success_msg', 'User updated successfully.');
        } else {
            $this->session->set_userdata('error_msg', 'User not updated.');
        }
        redirect('admin/approved_suppliers');
    }

    //Users
    public function manageUsers()
    {
        $data['all_users']=$this->AdminModel->getUsers();
        $main['page']  = $this->load->view('backend/user/all_user', $data, true);
        $main['title']  = 'Manage User';
        $this->load->view('backend/index', $main);
    }

    public function rejectUser($id)
    {

        $data['approved']=0;
        $this->AdminModel->updateData('users', 'user_id', $id, $data);
        redirect('admin/manage_users');
    }

    public function rejectEvaluation($evalId)
    {

        //get related supplier
        $evaluation = $this->FrontEndModel->search('evaluation', ['id'=>$evalId]);
        if (!$evaluation) {
            return 0;
        }
        $supplierId = $evaluation[0]['supplier_id'];

        $avgRating =  $this->FrontEndModel->averageRating($supplierId);

        //delete data from evaluation_rating table
        $this->AdminModel->deleteData('evaluation_rating', 'evaluation_id', $evalId);

        //delete data from evaluation table
        $this->AdminModel->deleteData('evaluation', 'id', $evalId);

        //Get updated average rating of supplier
        $avgRating = $this->FrontEndModel->supplierRating($supplierId);

        //update suppliers average evaluation
        $user = $this->FrontEndModel->search('users', ['user_id'=>$supplierId]);
        $user[0]['rating'] = $avgRating;
        $this->AdminModel->updateData('users', 'user_id', $supplierId, $user[0]);
    }

    public function getSupplierInfo()
    {
        $supplierId=$this->input->post('supplierId');
        $data[0]=$this->AdminModel->getSupplierInfo($supplierId);
        $data[1]=$this->FrontEndModel->getSupplierSkills($supplierId, 'rt_array');
        $data[2] =  json_decode($data[0]['other_countries']);
        if($data[2])
            $data[3]=$this->AdminModel->getCountries($data[2]);
        else
            $data[3]='';
        echo json_encode($data);
    }

    public function addUser()
    {
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        $clean['password']=md5($clean['password']);
        $clean['role']=1;
        $res=$this->AdminModel->insert('users', $clean);

        if ($res) {
            $this->session->set_userdata('success_msg', 'User added successfully.');
        } else {
            $this->session->set_userdata('error_msg', 'User not added.');
        }
        redirect('admin/manage_users');
    }
    public function updateUser()
    {
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        if (!empty($clean['password'])) {
            $clean['password']=md5($clean['password']);
        } else {
            unset($clean['password']);
        }
        $res=$this->AdminModel->updateData('users', 'user_id', $clean['user_id'], $clean);
        if ($res) {
            $this->session->set_userdata('success_msg', 'User updated successfully.');
        } else {
            $this->session->set_userdata('error_msg', 'User not updated.');
        }
        redirect('admin/manage_users');
    }


    public function viewTags()
    {
        $data['tags']=$this->AdminModel->get('tags');
        $main['page']  = $this->load->view('backend/pages/google_tag', $data, true);
        $this->load->view('backend/index', $main);
    }
    public function updateTag()
    {
        $post=$this->input->post();
        $clean=$this->security->xss_clean($post);
        $res=$this->AdminModel->updateData('tags', 'id', $clean['id'], $clean);
        if ($res) {
            $this->session->set_userdata('success_msg', 'Tag updated successfully.');
        } else {
            $this->session->set_userdata('error_msg', 'Tag not updated.');
        }
        redirect('admin/tags');
    }


    public function emailCheck()
    {
        $this->form_validation->set_rules('user_email', 'user_email', 'required|valid_email|is_unique[users.user_email]');
        if ($this->form_validation->run()==false) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function passwordCheck()
    {
        $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]|max_length[12]');
        if ($this->form_validation->run()==false) {
            echo "false";
        } else {
            echo "true";
        }
    }
    public function nameCheck()
    {
        $this->form_validation->set_rules('nick_name', 'nick_name', 'required|is_unique[users.nick_name]');
        if ($this->form_validation->run()==false) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function editEmailCheck()
    {
        echo "true";
        $email=$this->input->post('user_email');
        $user_id=$this->input->post('user_id');
        echo "dsf".$user_id;
        die;
        $data=$this->AdminModel->getData('users', 'user_id', $user_id);
        if ($data['user_email']!=$email) {
            $checkExist=$this->AdminModel->getData('users', 'user_email', $email);
            if (empty($checkExist)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }
    public function editNameCheck()
    {
        echo "true";
        $this->form_validation->set_rules('nick_name', 'nick_name', 'required|is_unique[users.nick_name]');
        if ($this->form_validation->run()==false) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function deleteUser($id)
    {
        $chkValid=$this->AdminModel->getData('users', 'user_id', $id);
        if ($chkValid) {
            if ($this->AdminModel->deleteData('users', 'user_id', $id)) {
                $this->session->set_userdata('success_msg', 'Delete Successful.');
            }
        } else {
            $this->session->set_userdata('error_msg', 'Something Worng!');
        }
        redirect('admin/manage_users');
    }

    public function sendRegConfirmMail()
    {
        $post = $this->input->get();
        $userEmail = $post['email'];

        //generate link
        $code = sha1(mt_rand(0, time()));
        $data['link'] = base_url().'supplier/complete/registration/?code='.$code;

        //email template
        $emailTemplate = $this->load->view('email_templates/registration_confirmation', $data, true);

        //send mail
        sendEmail('Registration invitation', $emailTemplate, 'info@look4supplier.com', $userEmail);

        //save info to database
        $dataToInsert = [
            'email' => $userEmail,
            'authCode' => $code,
        ];
        $this->AdminModel->insert('email_confirmation', $dataToInsert);
    }

    public function supplierDetails($id)
    {
        $supplier = $this->FrontEndModel->searchSupplier(['user_id'=>$id]);
        echo json_encode($supplier[0]);
    }

    /**
     * List of all gdpr requests(View part).
     *
     * @return void
     */
    public function deleteGDPRReqs()
    {

        $data['requests'] = $this->FrontEndModel->search('users', ['delete_gdpr'=>1]);
        $main['page']  = $this->load->view('backend/user/del_gdpr_list', $data, true);
        $main['title']  = 'Delete GDPR';
        $this->load->view('backend/index', $main);
    }

    /**
     * Delete GDPR (ajax request)
     *
     * @param int $userId user id to delete
     *
     * @return bool $ret deleted  or not
     */
    public function deleteGDPR($userId)
    {
         //get all evaluation ids
        $evalIds = $this->FrontEndModel->search('evaluation', ['creator_id'=>$userId]);
        $evalIds = array_column($evalIds, 'id');

         //delete user's evaluation(evaluation table)
        $this->FrontEndModel->delete('evaluation', 'id', $evalIds);

         //delete data from rating table(evaluation_rating)
        $this->FrontEndModel->delete('evaluation_rating', 'evaluation_id', $evalIds);

         //delete user's account
        $ret = $this->FrontEndModel->deleteData('users', 'user_id', $userId);
        return $ret;
    }

    public function getskills()
    {
        $skills['results'] = $this->FrontEndModel->getSkills('skills');
        echo json_encode($skills);
    } 

    public function getCountries()
    {
        $countries['results'] = $this->FrontEndModel->getCountries('countries');
        echo json_encode($countries);
    }
}
