<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//admin site
$route['admin'] 		= 'AuthController/login';
$route['login'] 					= 'AuthController/login';
$route['check_login'] 				= 'AuthController/check_login';
$route['admin/dashboard'] 			= 'DashboardController/index';
$route['logout'] 					= 'AuthController/logout';


//evaluation
$route['admin/manage_evaluation'] 					= 'AdminController/manageEvaluation';
$route['admin/reject_evaluation/(:any)'] 			= 'AdminController/rejectEvaluation/$1';
$route['admin/approve_evaluation/(:any)'] 			= 'AdminController/approveEvaluation/$1';

//suplier
$route['admin/approved_suppliers'] 			= 'AdminController/approvedSuppliers';
$route['admin/new_suppliers'] 			= 'AdminController/newSupplier';
$route['admin/approve_supplier/(:any)'] 			= 'AdminController/approvedSupplier/$1';
$route['admin/delete_supplier/(:any)'] 			= 'AdminController/deleteSupplier/$1';
$route['admin/update_supplier'] 			= 'AdminController/updateSupplier';
$route['admin/reject_supplier/(:any)'] 			= 'AdminController/rejectSupplier/$1';

//user
$route['admin/manage_users'] 			= 'AdminController/manageUsers';
$route['admin/add_user'] 			= 'AdminController/addUser';
$route['admin/update_user'] 			= 'AdminController/updateUser';
$route['admin/delete_user/(:any)'] 			= 'AdminController/deleteUser/$1';
$route['admin/edit_users/(:any)'] 			= 'AdminController/editUsers/$1';








$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
