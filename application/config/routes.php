<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|   example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|   https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|   $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|   $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|   $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|       my-controller/my-method -> my_controller/my_method
*/

$route['default_controller'] = 'AuthController';
$route['404_override'] = 'SessionFree/errorPage';
$route['translate_uri_dashes'] = false;


//admin site
$route['admin'] = 'AuthController/login';
$route['login'] = 'AuthController/login';
$route['check_login'] = 'AuthController/check_login';
$route['admin/dashboard'] = 'DashboardController/index';
$route['logout'] = 'AuthController/logout';


//evaluation
$route['admin/notifications'] = 'AdminController/viewNotifications';
$route['delete/notification/(:any)']        = 'AdminController/deleteNotification/$1';
$route['delete/evaluation/(:any)']        = 'AdminController/deleteRequest/$1';

$route['admin/manage_evaluation'] = 'AdminController/manageEvaluation';
$route['admin/reject_evaluation/(:any)'] = 'AdminController/rejectEvaluation/$1';
//$route['admin/approve_evaluation/(:any)'] = 'AdminController/approveEvaluation/$1';
$route['admin/view_evaluation/(:any)'] = 'AdminController/evaluationDetails/$1';

//supplier
$route['admin/new_suppliers']                 = 'AdminController/newSupplier';
$route['admin/approved_suppliers']            = 'AdminController/approvedSuppliers';
$route['admin/delete_supplier/(:any)']        = 'AdminController/deleteSupplier/$1';
$route['admin/update_supplier']               = 'AdminController/updateSupplier';
$route['admin/hide/(:any)']                   = 'AdminController/hideSupplier/$1';
$route['admin/show/(:any)']                   = 'AdminController/showSupplier/$1';
$route['supplier/mail/confirmation']          = 'AdminController/sendRegConfirmMail';
$route['supplier/approve/(:any)']             = 'AdminController/approveSupplier/$1';
$route['supplier/reject/(:any)']              = 'AdminController/rejectSupplier/$1';
$route['supplier/change-status/(:any)/(:any)']= 'AdminController/supplierStatus/$1/$2';

//user
$route['admin/manage_users']            = 'AdminController/manageUsers';
$route['admin/add_user']            = 'AdminController/addUser';
$route['admin/reject_user/(:any)']          = 'AdminController/rejectUser/$1';
$route['admin/update_user']             = 'AdminController/updateUser';
$route['admin/delete_user/(:any)']          = 'AdminController/deleteUser/$1';
$route['admin/edit_users/(:any)']           = 'AdminController/editUsers/$1';
$route['admin/delete/gdpr_request'] = 'AdminController/deleteGDPRReqs';


$route['admin/new_evaluation'] = 'EvaluationController/NewEvaluation';
$route['admin/reject_evaluation/(:any)'] = 'EvaluationController/rejectEvaluation/$1';
$route['admin/approve_evaluation/(:any)'] = 'EvaluationController/approveEvaluation/$1';

$route['admin/tags'] = 'AdminController/viewTags';
$route['admin/update_tag'] = 'AdminController/updateTag';


//manage page
$route['add_page'] = 'PageController/addPage';
$route['addPage'] = 'PageController/addPage';
$route['view_page'] = 'PageController/viewPage';
$route['edit_page/(:any)'] = 'PageController/editPage/$1';
$route['delete_page/(:any)'] = 'PageController/deletePage/$1';
$route['updatePage'] = 'PageController/updatePage';
$route['static-page/(:any)'] = 'AuthController/showStaticPage/$1';


//user site
$route['facebookLogin'] = 'User_Authentication/facebookLogin';
$route['googleLogin'] = 'User_Authentication/googleLogin';
$route['add_user'] = 'AuthController/signUp';
$route['sign_in'] = 'AuthController/signIn';
$route['sign_out'] = 'AuthController/signOut';
$route['home'] = 'AuthController';
$route['user/home'] = 'UserController';
$route['user/evaluation/(:any)'] = 'UserController/index/$1';
$route['user/evaluation'] = 'UserController';
$route['addNickname'] = 'UserController/addNickname';
$route['receiver'] = 'ApiLinkedin/receiver';


$route['add_social_user'] = 'FrontendController/userData';



$route['who_we_are'] = 'AuthController/showStaticPage';
$route['how_it_work'] = 'AuthController/showStaticPage';
$route['contact_us'] = 'AuthController/contactUs';

// forgot password

$route['forgot_password'] 			= 'AuthController/forgot_password';
$route['sendResetPassEmail'] 		= 'AuthController/sendResetPassEmail';
$route['passwordResetForm'] 		= 'AuthController/passwordResetForm';
$route['updateresetpassword'] 		= 'AuthController/updatePassword';
$route['passwordSuccess'] 		= 'AuthController/passwordSuccess';

/////////////
//user end //
/////////////
$route['auto_search'] = 'EvaluationController/autoSearch';
//evaluation
$route['evaluation/all'] = 'EvaluationController/allEvaluation';
//$route['evaluation/add'] = 'EvaluationController/addEvaluation';
$route['add_evaluation'] = 'EvaluationController/addEvaluation';
$route['evaluation/details/(:any)'] = 'UserController/viewEvaluation/$1';

$route['add_new_supplier/(:any)'] = 'AuthController/supplier_login/$1';
$route['add_supplier'] = 'AuthController/addSupplier';
$route['success'] = 'AuthController/success';
$route['supplier_registration_info/(:any)'] = 'AuthController/supplierInfo/$1';
$route['suplier_info'] = 'AuthController/addSupplierInfo';
$route['profile/update/(:any)'] = 'UserController/updateProfile/$1';

//skill
$route['search_skill'] = 'FrontendController/searchSkill';


//supplier
$route['supplier/home'] = 'SupplierController/supplierIndex';
$route['supplier/search'] = 'FrontendController/searchSupplier';
$route['supplier/details/(:any)'] = 'SupplierController/supplierInfo/$1';
$route['user_evaluation/details/(:any)'] = 'SupplierController/evaluationDetails/$1';
$route['supplier/complete/registration']    = 'AuthController/supplier_login';
$route['submit_answer'] = 'SupplierController/addAnswer';
$route['submit_requests'] = 'SupplierController/addRequest';
$route['message_to_contact'] = 'SupplierController/messageToContact';
$route['submit_request'] = 'SupplierController/messageToContact';



//apis callback
$route['callback/linkedin'] = 'SessionFree/linkedinCallback';
