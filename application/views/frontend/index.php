<!-- ==== ss top supplier  ==== -->
<section class="top_supllier">
  <div class="container">
    <div class="page_title">
      <h3> <span> Top </span>  Suppliers</h3>
    </div>
    <div class="ss_top_supp">

     <?php foreach($suppliers as $data):?>
      <div class="top_s_item">
        <div class="top_s_img"> <a href="supplier/details/<?php echo $data['user_id'].'#inner_main';?>"> <img src="<?php if($data['logo'])echo $data['logo'];else echo "assets/frontend/img/c1.png";?>" class="img-fluid"/> </a> </div>
        
        <h4> <a href="supplier/details/<?php echo $data['user_id'].'#inner_main';;?>"> <?php echo $data['company_name'];?> </a> </h4>
        <h3> <a href="supplier/details/<?php echo $data['user_id'].'#inner_main';;?>"><?php echo $data['industryName'];?> </a> </h3> 
        <div class="top_s_rat">
          <!-- <span class="float-left not-rate">
                 <span class="rateyo" data-rateyo-rating="<?php echo $data['rating']; ?>" 
                   data-rateyo-num-stars="5" data-rateyo-score="<?php echo $data['rating']; ?>"></span>
                 </span> -->     
                 <input id="input-id" name="input-name" type="number" class="star-rating" data-size="xs" data-rtl="false" readonly="true" value="<?php echo $data['rating']; ?>">
               </div>
               <!-- <p class="text-justify"><?php echo $data['industryName'];?></p> -->
               <a href="supplier/details/<?php echo $data['user_id'].'#inner_main';;?>" class="ss_r_btn">More info </a>
             </div>
           <?php endforeach;?>



         </div>
       </section>
       <!-- ==== ss end top supplier  ==== -->

       <!-- ==== ss who we are   ==== -->

       <section class="who_we_are">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="who_heading">
                <h3>Who we are</h3>
                <p>Lorem Ipsum is simply dummy </p>
              </div>


              <p>Pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <div class="col-md-6">
              <div class="who_w_ing">
                <img src="assets/frontend/img/w_i.png" class="img-fluid"/>
              </div>

            </div>
          </div>
        </div>
      </section>

      <!-- ==== ss end who we are   ==== -->

      <!-- ==== ss How it work   ==== -->
      <section class="how_it_work">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="h_iw_img">
                <img src="assets/frontend/img/h_i_w.png" class="img-fluid"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="padding80">
                <div class="how_it_heading">
                  <h3>How it work</h3>
                  <p>Lorem Ipsum is simply dummy </p>
                </div>
                <p>
                  Pesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- ==== ss end How it work   ==== -->



      
      <!-- ==== ss end client  ==== -->


      <script type="text/javascript">

        $(document).ready(function(){

          $(function () {
            $(".rateyo").rateYo({ readOnly: true,spacing: "35px"}).on("rateyo.change", function (e, data) {

            });


          });

        });

      </script>