<?php $this->load->view('frontend/session_msg');?>
<div id="inner_main">
  <section class="evulation_details">
    <div class="container">

      <div class="evulation_detail_bottom">
        <h3>Request to Delete</h3>
        <div class="evulation_review_details">

          <form method="post" action="submit_request" >


            <div class="form-group"> 
              <div class="dynamic-element form-delete" style="display: none;">
                <div class="row">
                  <div class="col-md-11">
                    <div class="form-group">                                  
                      <input type="email" name="email_list[]" class="form-control" placeholder="Email">
                    </div>
                  </div>
                  <div class="col-md-1">
                    <p style="cursor: pointer;" class="delete">x</p>
                  </div>
                </div>
              </div>
              <div class="dynamic-anchor">
                <!-- Dynamic element will be cloned here -->
                <!-- You can call clone function once if you want it to show it a first element-->
              </div>

              <div class="col-md-12">
                <p class="add-one btn btn-success">+ Add Email Field</p>
              </div>
            </div> 

            <div class="form-group"> 
              <textarea name="reason" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
            </div>
            <button type="submit" class="btn btn-success float-right"  >Submit Request</button>
          </form>
        </div>
      </div>


    </div> 
  </section>
  <!-- === end search result=== -->
  <!-- ==== ss top supplier  ==== -->


  <!-- ==== ss end supplier  ==== -->
</div>