<!-- Modal login -->
<div class="modal fade " id="user_login" tabindex="-1" role="dialog" aria-labelledby="user_loginlLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >User Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="sign_in" method="POST" id="singIn">
        <div class="social_login">
          <div><a href="<?php echo getFacebookUrl();?>"><img src="assets/frontend/img/btn_face_book.png"/></a></div>
          
          <div><a href="<?php echo getGoogleUrl();?>"><img src="assets/frontend/img/btn_google.png"/></a></div>
        </div>

        <input type="hidden" name="role" value="2">
        
      </form>
    </div>
    <div class="modal-footer">
      <p>New User <a href="#" id="modal_open" data-toggle="modal" data-target="#user_register"> Create an Account </a> </p>
    </div>
  </div>
</div>
</div>
<!-- Modal login -->
<div class="modal fade" id="invite_supplier" tabindex="-1" role="dialog" aria-labelledby="user_loginlLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >New Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form action="sign_in" method="POST" id="confEmailForm">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" required class="form-control" id="confEmail" placeholder="Enter email">
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <?php if ($this->session->role==2) {?>
          <div  class="form-group"><br>
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
           <input type="submit" name="submit" class="btn btn-primary" value="Submit">
         </div>
       <?php } else { ?>
        <div  class="form-group"><br>
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
         <a class="btn btn-primary" id="inviteSupplier"> Submit</a>
       </div>
     <?php }?>
   </form>
 </div>

</div>
</div>
</div>

<div class="modal fade" id="user_register" tabindex="-1" role="dialog" aria-labelledby="user_registerLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="add_social_user" method="POST" id="target">
          <div class="social_login">

            <div class="form-group"> 
              <input type="text" placeholder="User Name" required name="nick_name" id="nick_name" class="form-control">
            </div>
            <div class="form-group row">
              <div class="col-sm-12 text-justify">Completing the registration I declare I read the Privacy information notice. Here add the privacy link</div>

            </div>
            <div class="form-group row">
              <div class="col-sm-9 text-justify">Issue of information or promotional messages on the services offered by look4supplier  and statistical studies and automated contact methods (email, text message, multimedia message and instant messaging systems)</div>
              <div class="col-sm-3">
                <select class="form-control" name="privacy1">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-9 text-justify">I consent to the use of my browsing data to receive proposals and services in line with my preferences and my interests"
              for the privacy 3 change the label with "I consent to the use of my data to receive updates and news from third parties (information)</div>
              <div class="col-sm-3">
                <select class="form-control" name="privacy2">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-9 text-justify">I consent to the use of my data to receive updates and news from third parties (information)</div>
              <div class="col-sm-3">
                <select class="form-control" name="privacy3">
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 text-justify">Withdraw your consent to the processing of your personal data at any time sending an email to: info@look4supplier.com</div>

            </div>

            <div class="form-group row">
              <div class="col-sm-6">
                <button type="submit" style="cursor: pointer;" class="submitForm" url="<?php echo getFacebookUrl();?>">
                  <img  src="assets/frontend/img/btn_face_book.png"/>
                </button>
              </div>
              <div class="col-sm-6">
                <button type="submit" style="cursor: pointer;" class="submitForm" url="<?php echo getGoogleUrl();?>">
                  <img  src="assets/frontend/img/btn_google.png"/>
                </button>
              </div>
            </div>

            <div>
            </div>

          </div>


        </form>
      </div>
      <div class="modal-footer">
        <p>Alrady have an account  <a href="#" data-toggle="modal" data-target="#user_login"> Login </a> </p>
      </div>
    </div>
  </div>
</div>


<!--  -->
<div class="modal fade" id="supplier_login" tabindex="-1" role="dialog" aria-labelledby="user_loginlLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Supplier Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="sign_in" method="POST" id="supplier_singin">
          <input type="hidden" name="role" value="3">
          <p id="supplier_error" class="text-center text-warning"></p>
          <div class="login_form">
            <div class="form-group"> 
              <input type="email" placeholder="User Name" required name="email" id="email" class="form-control">
            </div>
            <div class="form-group"> 
              <input type="password" placeholder="Password" name="password" id="password" class="form-control">
            </div>
            <div class="form-group">
              <label for="remember-me" class="text-info"><span><input id="remember-me1" name="remember-me" type="checkbox"></span><span>Remember me</span> </label> <a href="forgot_password" class="text-info float-right">Forgot Password?</a>

            </div>
            <div  class="form-group">
              <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
            </div>

          </div>
        </form>
      </div>
      <div class="modal-footer">
        <p>New Supplier <a href="#" data-toggle="modal" data-target="#supplier_register"> Create an Account </a> </p>
      </div>
    </div>
  </div>
</div>
<!-- modal register -->


<!-- supplier -->
<div class="modal fade" id="supplier_register" tabindex="-1" role="dialog" aria-labelledby="user_registerLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="add_user" method="POST" id="supplier">
          <input type="hidden" name="role" value="3">
          <p id="res_error" class="text-center text-warning"></p>
          <div class="login_form">
            <div class="form-group"> 
              <input type="text" placeholder="User Name" required name="nick_name" id="nick_name" class="form-control">
            </div>
            <div class="form-group"> 
              <input type="email" placeholder="Email" required name="user_email" id="user_email" class="form-control">
            </div>
            <div class="form-group"> 
              <input type="password" placeholder="Password" required name="password" id="password2" class="form-control">
            </div>

            <div class="form-group row">
              <div class="col-sm-12 text-justify">Completing the registration I declare I read the Privacy information notice. Here add the privacy link</div>

            </div>
            <div class="form-group row">
              <div class="col-sm-9 text-justify">Issue of information or promotional messages on the services offered by look4supplier  and statistical studies and automated contact methods (email, text message, multimedia message and instant messaging systems)</div>
              <div class="col-sm-3">
                <select class="form-control" name="privacy1">
                  <option value="0">No</option>
                  <option value="1">Yes</option>
                  
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-9 text-justify">I consent to the use of my browsing data to receive proposals and services in line with my preferences and my interests"
              for the privacy 3 change the label with "I consent to the use of my data to receive updates and news from third parties (information)</div>
              <div class="col-sm-3">
                <select class="form-control" name="privacy2">
                  <option value="0">No</option>
                  <option value="1">Yes</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-9 text-justify">I consent to the use of my data to receive updates and news from third parties (information)</div>
              <div class="col-sm-3">
                <select class="form-control" name="privacy3">
                  <option value="0">No</option>
                  <option value="1">Yes</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-12 text-justify">Withdraw your consent to the processing of your personal data at any time sending an email to: info@look4supplier.com</div>

            </div>

            <div class="form-group">
              <label for="remember-me"><span><input required id="remember-me" name="remember_me" type="checkbox"></span><span> I accept the </span> </label> <a href="#" class="text-info">Terms & Conditions</a>

            </div>
            <div  class="form-group">
              <input type="submit" name="submit" class="btn btn-info btn-md" value="Sign Up">
            </div>


          </div>
        </form>
      </div>
      <div class="modal-footer">
        <p>Alrady have an account  <a href="#" data-toggle="modal" data-target="#user_login"> Login </a> </p>
      </div>
    </div>
  </div>
</div>

<!--  -->
<!-- modal new_evaluation -->
<div class="modal fade" id="new_evaluation" tabindex="-1" role="dialog" aria-labelledby="new_evaluationLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Add new evaluation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php include('evaluation_template.php') ?>

      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<?php include('disclaimer.php') ?>



