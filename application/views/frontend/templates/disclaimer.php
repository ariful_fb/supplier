<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="user_registerLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Disclaimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="add_user" method="POST" id="supplier">

          <p id="res_error" class="text-center text-warning"></p>


          <div class="form-group">
            <p class="text-justify">Look4supplier will do it’s best to try to check the evaluations but look4supplier can’t check all the evaluation. Saving this evaluation you confirm that 1) you are not working for the supplier you are evaluating 2) you don’t have any personal relationship with the supplier 3) you didn’t receive any payment to do the evaluation 4) you certify that the evaluation is only based on your own experience and you know that a fake evaluations are forbidden.
              Look4supplier doesn’t have any responsability on the evaluation added by the users. The supplier could add an answer to the evaluation and could ask to delete the evaluation if the supplier demostrates it is a fake one
            In addition to being a violation of our terms of service and an unethical practice, committing fraud on reviews is also prohibited by the law and regulations in many jurisdictions</p>

          </div>
          <div  class="form-group">
            <p  id="check_yes"  class="btn btn-info btn-md check_yes">Yes</p>
            <p  id="check_no"   class="btn btn-info btn-md check_no">No</p>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>