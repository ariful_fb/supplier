<?php include "header.php"?>

<!-- ==== ss end header ==== -->
<!-- ==== ss banner ==== -->

<section class="banner" >
  <!-- <img src="assets/frontend/img/banne_bg.png" class="img-fluid"/> -->
  <div class="banner_img">
    <img src="assets/frontend/img/footer_bg.png" class="img-fluid" style="background-color: #000"/>
  </div>
  <div class="banner_content">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-8">
          <h3 class="text-center">Find the available suppliers</h3>
          <form id="suppSearchForm">
            <div class="input-group mb-3">
              <div class="input-group-prepend">

                <input type="text" class="form-control" name="city"  placeholder="City">
                <input type="text" class="form-control" name="state"  placeholder="State">
                <select class="form-control" id="id_label_single" name="skill">
                  <option value="">--skill--</option>
                  <?php $skills=allSkill(); ?>
                  <?php foreach ($skills as $skill) : ?>
                    <option value="<?php echo $skill['id'] ?>"><?php echo $skill['name']; ?></option>
                  <?php endforeach; ?>
                </select>

              </div>
              <input type="text" class="form-control"  placeholder="By Category, Company or Brand" name="keyword">
              <button type="submit" class="btn btn-primary" >Search</button>
            </div>
          </form>
          <p><?php if (isset($totalConnect)) {
            echo $totalConnect;
          }?> + supplier and supplier’s evaluation are resister here.</p>
          <?php if($this->session->role!=3){?>
            <div class="banner_botom">
             <a href="#" data-toggle="modal" data-target="#new_evaluation" class="btn btnwhitebr">New  Evaluation</a>
             <a href="#" class="btn btnwhite" data-toggle="modal" data-target="#supplier_register">New Supplier</a>
           </div>
         <?php }?>
       </div>
     </div>
   </div>
 </div>
</section>




<?php $this->load->view('frontend/templates/session_msg');?>
<!-- ==== ss end banner ==== -->
<div id="mainContent">
  <?php  echo $content;?>
</div>
<?php include "footer.php"; ?>
