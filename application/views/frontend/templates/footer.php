<footer>
  <div class="container">

    <div class="row">
      <!-- <div class="col-sm-3">
      
        <div class="footer_contact">
          <div><img src="assets/frontend/f_logo.png" class="img-fluid"/></div>
          <p>Ipsum is simply dummy text of the love printing and typesetting industry. Lorem  has been the industry's standard dummy text e since the 1500s, when an unknown been </p>
          <ul class="footer_social">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
          </ul>
        </div>         
      </div> -->
      <div class="col-md-6">
        <h4> Contact us</h4>
        <div class="contact_info">
          <ul>

            <li><span><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
            info@look4supplier.com</li>

          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <h4> Our Link </h4>
        <ul> 
         <li><a href="https://www.iubenda.com/privacy-policy/20652261" class="iubenda-white iubenda-embed" title="Privacy Policy ">Privacy Policy</a></li>
         <li><a href="#" data-toggle="modal" data-target="#myModal"  title="Privacy Policy ">Privacy Disclaimer</a></li>
         <?php $page=getPages();
         foreach($page as $data):?>
          <li >
            <a href="static-page/<?php echo $data['page_title']?>"><?php echo $data['menu_title']?></a>
          </li>
        <?php endforeach;?>


      </ul> 
    </div>



  </div>


</div>


<div class="copyright"> 
  <div class="container">
    <p  class="text-center"> © <span>look 4 supplier</span>  2018. All Rights Reserved. Designed by Therssoftware </p>
  </div>

</div>
</footer>
<?php include('modal.php');?>
<!--  -->



<style type="text/css">
.modal { overflow: auto !important; }
#supplier_login .modal-header {
  background: #2bb559;
  color: #fff;
  /*padding: 5px 15px;*/
  text-align: center;
}
#supplier_login .modal-footer {
  background: #2bb559;
  color: #fff;
  text-align: center;
  display: block;
}
#supplier_register .modal-header {
  background: #2bb559;
  color: #fff;
  /*padding: 5px 15px;*/
  text-align: center;
}
#supplier_register .modal-footer {
  background: #2bb559;
  color: #fff;
  text-align: center;
  display: block;
}
</style>
<!-- star rating -->
<!-- <script src="plugins/rateyo/js/jquery.js"></script>
  <script src="plugins/rateyo/js/jquery.rateyo.js"></script> -->

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
  <!-- Optional JavaScript -->
  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 

  <script src="assets/js/ai_custom.js"></script>
  <script src="assets/plugins/autocomplete/jquery.autocomplete.min.js"></script>
  <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="assets/frontend/js/slick.min.js"></script>
  <script class="source" type="text/javascript">

   
    $("#modal_open").click(function(){
      $('#user_login').modal('hide');
      $('#user_register').modal('show');
    });

    $("#modal").click(function(){
      $('#new_evaluation').modal('hide');
      $('#user_login').modal('show');

    });
    
    $("#inviteSupplier").click(function(){
      $('#invite_supplier').modal('hide');
      $('#user_login').modal('show');

    });

    $(window).bind('scroll', function () {
      if ($(window).scrollTop() > 80) {
        $('#ss_menu').addClass('fixed');
      } else {
        $('#ss_menu').removeClass('fixed');
      }
    });
    $('.ss_top_supp').slick({
      dots: true, 
      prevArrow: false,
      nextArrow: false,
      autoplay: true,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4, 
      responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3, 
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      ]
    });

    $('.ss_testmonial').slick({
      dots: true,
      centerMode: true,

      centerPadding: '60px',
      slidesToShow: 2,
      arrows: false,
      autoplay: true,
      responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
      ]
    });

  //select2
  // $(document).ready(function(){
  //   $('.select2').select2();
  // })

</script> 


<script>

  $(document).on('submit','#suppSearchForm', function (e) {
    e.preventDefault();
    $.ajax({
      url:'supplier/search',
      //url:'SupplierController/search2',
      method:'POST',
      data: $(this).serialize(),
      success : function(data){
        console.log(data);
        $('#mainContent').html(data);
      }
    });
  });
  $("#search").click(function(){
    var city="<?php echo $this->session->userdata('city');?>";
    var state="<?php echo $this->session->userdata('state');?>";
    var skill="<?php echo $this->session->userdata('skill');?>";
    var keyword="<?php echo $this->session->userdata('keyword');?>";
    //alert(city);
    $.ajax({
      url:'supplier/search',
      //url:'SupplierController/search2',
      method:'POST',
      data: {city:city,state:state,skill:skill,keyword:keyword,},
      success : function(data){
        console.log(data);
        $('#mainContent').html(data);
      }

    });
  });

  jQuery(document).ready(function() {
    $(".star-rating").rating({
      min:0,
      max:5, 
      step:0.1, 
      size:'sm', 
      stars:5,
      showClear:false,
      showCaption:false,
    });
  });
  
</script>

<script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>

<script type="text/javascript" id="cookieinfo"
src="//cookieinfoscript.com/js/cookieinfo.min.js">
</script>


</body>
</html>
