<div class="container">
  <div class="row mt-2">     
    <div class="col-md-2">
    </div>           
    <div class="col-md-8"> 
      <?php if ($this->session->userdata('error_msg')) { ?>
        <div class="alert alert-danger">                    
          <span class="text-center"><?=$this->session->userdata('error_msg');?></span>
          <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
        </div>
      <?php } $this->session->unset_userdata('error_msg');?>
      <?php if ($this->session->userdata('success_msg')) { ?>
        <div class="alert alert-success">                    
          <span class="text-center"><?=$this->session->userdata('success_msg');?></span>
          <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
        </div>
      <?php } $this->session->unset_userdata('success_msg'); ?>
    </div> 
    <div class="col-md-2">
    </div>               
  </div>
</div>
