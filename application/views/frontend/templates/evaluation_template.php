<style>
svg{
  width: 20px;
  height: 20px;
}
.input-width{
  width:0px;border: 1px solid white;
  margin-top: 6px;
}
</style>
<script type="text/javascript">
  $("#addNewEvaluation").on('click',function(){
    var name = "<?php if (isset($supplier['company_name'])) {echo $supplier['company_name'];}?>";
   // alert(name);
   $('#new_evaluation').modal('show');
   $("#add_company_name").attr("disabled",true);
   $("#add_company_name").val(name);
 });
</script>

<form method="post" action="add_evaluation" id="add_evaluation">
  <div class="login_form">
    <div class="form-group row">
      <div class="col-md-11" > 
        <?php if (isset($supplier['company_name'])) {?>
          <input type="hidden" name="username" value="<?php if (isset($supplier['company_name'])) {
            echo $supplier['company_name'];
          }?>">
        <?php } ?>
        <input id="add_company_name" type="text" placeholder="Suppliers  Name" value="" name="username" class="form-control typeahead form-control inputDisabled">
      </div>
      <div class="col-md-1">
        <span class="text-danger h4">*</span>
      </div>
    </div>

    <div class="form-group row">
      <div class="col-sm-3">Disclaimer<span class="text-danger h4">*</span></div>
      <div class="col-sm-5">
        <input type="checkbox" name="check" id="check">
      </div>
    </div>
    <div class="form-group row"> 
     <div class="col-md-11">  
      <textarea type="text" placeholder="Brief Induction of the Activity managed by the supplier" name="induction"  class="form-control" rows="5" style="resize: none;"></textarea>
    </div>
    <div class="col-md-1"> 
      <span class="text-danger h4">*</span>
    </div>
  </div>
  <div class="form-group ">

    <select name="activity_type" id="activity_type"  class="form-control">
     <option value="2" onlcick="activityType(2);">Private</option>
     <option value="1" onlcick="activityType(1);">Business</option>

   </select>


 </div>
 <div class="form-group"> 
  <select name="activity_duration" id="activity_duration" class="form-control">
    <option value="">Activity Duration</option>
  </select>
</div>
<div class="form-group"> 
  <select name="budget" class="form-control" >
    <option value="">Budget</option>
    <option value="1">0-50.000€</option>
    <option value="2">50.000€-100.000€</option>
    <option value="3">100.000€-500.000€</option>
    <option value="4">500.000€-1.000.000€</option>
    <option value="5">More then 1.000.000€</option>

  </select>
</div>
<div class="form-group remove_style" style="display:none;"> 
  <input type="text" placeholder="Name of the customer company" name="customer_company" id="customer_company" class="form-control">
</div>
<div class="form-group remove_style" style="display:none;" > 

  <select name="budget" class="form-control" >
    <option value="">Dimension of user’s company</option>
    <option value="1">Less than 100 employees</option>
    <option value="2">Between 100-500 employees</option>
    <option value="3">Greater than 500 employees</option>
    <option value="4">private</option>

  </select>

</div>

<div class="form-group" > 
  <label for="">Assign some skills to the supplier</label>
  <select class="form-control select3" id=""  multiple="multiple" name="skill[]" style="width:100%!important;">
    <?php
    $skills = $this->FrontEndModel->get('skills');
    foreach ($skills as $data) :?>
      <option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
    <?php endforeach;?>
  </select>
</div> 

<div class="row">
  <div class="col-md-12">
    <label for="">Evaluation</label>
  </div>
</div>
<div class="row">
  <div class="col-sm-4 mt-2">
    <div class="rateYo" id="1" inputId="reliability"></div>
    <div class="counter"></div>
  </div>

  <div class="col-sm-8 ">
    <input type="text" id="reliability"  name="reliability" class="input-width">
    <span>Reliability<span  class="text-danger h4">*</span></span>

  </div>
</div><div class="row">
  <div class="col-sm-4 mt-2">
    <div class="rateYo" id="1" inputId="flexibility"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <input type="text" id="flexibility"  name="flexibility" class="input-width">
    <span>Flexibility<span  class="text-danger h4">*</span></span>
  </div>
</div><div class="row">
  <div class="col-sm-4 mt-2">
    <div class="rateYo" id="1" inputId="cost"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <input type="text" id="cost"  name="cost" class="input-width">
    <span>Cost/quality<span  class="text-danger h4">*</span></span>
  </div>
</div><div class="row">
  <div class="col-sm-4 mt-2">
    <div class="rateYo" id="1" inputId="global_quality"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <input type="text" id="global_quality"  name="global_quality" class="input-width">
    <span>Global quality of the work<span  class="text-danger h4">*</span></span>
  </div>
</div><div class="row">
  <div class="col-sm-4 mt-2">
    <div class="rateYo" id="1" inputId="people_skills"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <input type="text" id="people_skills"  name="people_skills" class="input-width">
    <span>Supplier’s people skills<span  class="text-danger h4">*</span></span>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="rateYo" id="1" inputId="supplier_people"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <p>Opened to talk with all the supplier’s people</p>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="rateYo" id="1" inputId="adapt"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <span>Open to changes/to adapt to the changes without burocratize</span>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="rateYo" id="1" inputId="engagement"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <span>Supplier’s people engagement</span>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="rateYo" id="1" inputId="knowledge"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <span>Supplier’s people business knowledge</span>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="rateYo" id="1" inputId="propositivity"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <span>Propositivity</span>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <div class="rateYo" id="1" inputId="proactivity"></div>
    <div class="counter"></div>
  </div>
  <div class="col-sm-8">
    <span>Proactivity</span>
  </div>
</div>
<input type="hidden" id="supplier_people" value="0" name="supplier_people">
<input type="hidden" id="adapt" value="0" name="adapt">
<input type="hidden" id="engagement" value="0" name="engagement">
<input type="hidden" id="knowledge" value="0" name="knowledge">
<input type="hidden" id="propositivity" value="0" name="propositivity">
<input type="hidden" id="proactivity" value="0" name="proactivity">
<input type="hidden" id="role" value="<?php echo $this->session->role;?>" name="role">

<div  class="form-group"><br>
  <input type="submit" name="submit" class="btn btn-success btn-md" value="Submit">
</div>


</div>
</form>


<script type="text/javascript">

  $('.select3').select2({
    tags: true,
    tokenSeparators: [","],

  })
  $('#select2').select2();

</script>
