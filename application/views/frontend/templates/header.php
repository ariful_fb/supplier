<!DOCTYPE html>
<html lang="en">
<head>
  <base href="<?php echo base_url();?>">
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?php echo metaValue(3);?>">
  <meta name="keywords" content="<?php echo metaValue(1);?>">

  <link rel="shortcut icon" type="image/png" href="assets/frontend/logo.jpg"/>
  <!-- Bootstrap CSS -->

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="assets/frontend/css/slick.css"/>
  <link rel="stylesheet" href="assets/frontend/css/style.css"/>
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Select2 -->
  <link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
  <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
  <link href="assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

  <script src="assets/js/jquery.min.js"></script>
  
  <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
  <script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
  <script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
  <script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
  <!--  -->
  
  
  <!-- sweet alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <!-- chart -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131492277-1"></script>

  <!-- production version, optimized for size and speed -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
  <!-- dev version -->
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

  <!-- krajee rating -->
  <link href="assets/plugins/bootstrap-star-rating-master/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />
  <script src="assets/plugins/bootstrap-star-rating-master/js/star-rating.js" type="text/javascript"></script>
  <script src="assets/plugins/bootstrap-star-rating-master/themes/krajee-svg/theme.js"></script>
  <link href="assets/plugins/bootstrap-star-rating-master/themes/krajee-svg/theme.css" media="all" rel="stylesheet" type="text/css" />
  <script src="assets/plugins/bootstrap-star-rating-master/themes/krajee-fa/theme.min.js"></script>
  <link href="assets/plugins/bootstrap-star-rating-master/themes/krajee-fa/theme.min.css" media="all" rel="stylesheet" type="text/css" />

  <!-- datatable -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-131492277-1');
  </script>
  
  <title><?php echo metaValue(2);?></title>
  
</head>

<body> 
  <!-- ===== ss header ===== -->
  <header>
    <div id="ss_menu">
      <div class="container">            
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand" href="<?php echo base_url();?>">
            <img style="height:50px; width:50px" src="assets/frontend/logo.jpg"/>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav  ml-auto"> 
              <?php $page=getPages();
              foreach($page as $data):?>
                <li class="nav-item">
                  <a class="nav-link" href="static-page/<?php echo $data['page_title']?>"><?php echo $data['menu_title']?></a>
                </li>
              <?php endforeach;?>

              <li class="nav-item">
                <a class="nav-link" href="contact_us">Contact us</a>
              </li>
              <?php if (isset($_SESSION['role'])) : ?>
                <li class="nav-item">
                  <a class="nav-link" href="" id="sendDelReq">Delete Data</a>
                </li>
              <?php endif; ?> 
              <?php if (isset($_SESSION['role'])&&($_SESSION['role']==2)) : ?>
              <li class="nav-item">
                <a class="nav-link btn btn-outline-success ss_b" href="user/home" >My Evaluations</a>
              </li>
            <?php endif; ?> 



            <?php  if (!$this->session->user_id) {?>
              <li class="nav-item">
                <a class="nav-link btn btn-outline-success ss_b" href="#" data-toggle="modal" data-target="#user_login"> User login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link btn btn-outline-success ss_bg" href="#" data-toggle="modal" data-target="#supplier_login">Supplier login</a>
              </li>
            <?php }?>

            <?php if (isset($_SESSION['role'])) : ?>
              <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="">

                  <span class="pro-user-name ml-1">
                    <?php if (isset($_SESSION['user_email'])) {
                      echo $_SESSION['user_email'];
                    } ?> <i class="mdi mdi-chevron-down"></i> 
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(10px, 50px, 0px);">


                  <?php if (isset($this->session->role) && $this->session->role==3) : ?>
                    <a href="<?php echo base_url().'supplier/home';?>" class="dropdown-item notify-item">
                      <i class="fa fa-align-justify mr-2"></i>
                      <span >My Evaluations</span>
                    </a>
                    <a href="<?php echo 'profile/update/'.$_SESSION['user_id'] ?>" class="dropdown-item notify-item">
                      <i class="fa fa-user mr-2"></i>
                      <span >My Account</span>
                    </a>
                    <a href="message_to_contact" class="dropdown-item notify-item">
                      <i class="fa fa-envelope"></i>
                      <span>Invite Customers</span>
                    </a>
                  <?php endif; ?>

                  <!--  <div class="dropdown-divider"></div> -->
                  <a href="sign_out" class="dropdown-item notify-item">
                    <i class="fa fa-sign-out"></i>
                    <span>Logout</span>
                  </a>

                </div>
              </li>
            <?php endif; ?>
          </ul>
        </div>
      </nav>        
    </div>
  </div>
</header>

<script type="text/javascript">
  $("#sendDelReq").on('click',function(e){
    e.preventDefault();

    swal('', "User id along with other related infos will permanently destroy, you really want to delete?", 'warning', {
      buttons: {
        yes: "Yes",
        no : 'No',
      },
    })
    .then((value) => {
      switch (value) {

        case "yes":
        $.ajax({
          url:'UserController/requestDeleteGDPR',
          success: function(data){
            console.log(data);
            swal('','Admin will get noified for this delete request. After deletion all associated data with your account will be erased.','success');
          }
        })
        break;

        case "no":
        swal("", "Request not sent", "error");
        break;

        default:
        swal("Nothing just happened!");
      }
    });
  });
</script>
