<div id="inner_main">

	<h3 class="text-center">Supplier Info</h3>
	<div class="row mt-2">     
		<div class="col-md-2">
		</div>           
		<div class="col-md-8"> 
			<?php if ($this->session->userdata('error_msg')) { ?>
				<div class="alert alert-danger">                    
					<span class="text-center"><?=$this->session->userdata('error_msg');?></span>
					<a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
				</div>
			<?php } $this->session->unset_userdata('error_msg');?>
			<?php if ($this->session->userdata('success_msg')) { ?>
				<div class="alert alert-success">                    
					<span class="text-center"><?=$this->session->userdata('success_msg');?></span>
					<a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
				</div>
			<?php } $this->session->unset_userdata('success_msg'); ?>
		</div> 
		<div class="col-md-2">
		</div>               
	</div>
	<section class="evulation_details">
		<div class="container">
			<div class="">

				<div class="row">
					<div class="col-md-12">
						<form  action="suplier_info" method="POST" id="suplier_info" enctype="multipart/form-data">
							<input type="hidden" name="supplier_id" value="<?php if(isset($supplierId))echo $supplierId;?>">

							<div class="form-group row">
								<label for="main City " class="col-lg-4 control-label">Company Name<span class="text-danger h4">*</span></label>
								<div class="col-lg-8">
									<input type="text" class="form-control" id="company_name " name="company_name" placeholder="">
								</div>
							</div>

							<div class="form-group row">
								<label for="industry" class="col-lg-4 control-label">Industry<span class="text-danger h4">*</span></label>

								<div class="col-md-8">
									<select class="form-control"  name="industry" >
										<option value="">--Select Industry--</option>
										<?php foreach($industry as $data):?>
											<option value="<?php echo $data['id'];?>"><?php echo $data['industryName'];?></option>
										<?php endforeach;?>
									</select>

								</div>
							</div>

							<div class="form-group row">
								<label for="email" class="col-lg-4 control-label">Contact Email<span class="text-danger h4">*</span></label>
								<div class="col-lg-8">
									<input type="email" class="form-control" id="email " name="email" >
								</div>
							</div>

							<div class="form-group row">
								<label for="Contact Name" class="col-lg-4 control-label">Contact Name</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" id="contact_name" name="contact_name" >
								</div>
							</div>

							<div class="form-group row">
								<label for="main country" class="col-lg-4 control-label">Main Country </label>
								<div class="col-lg-8">
									<select class="form-control"  name="main_country" >
										<?php foreach($countries as $data):?>
											<option value="<?php echo $data['country_name'];?>"><?php echo $data['country_name'];?></option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="main City " class="col-lg-4 control-label">Main City</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" id="main_city " name="main_city" >
								</div>
							</div>
							

							
							<div class="form-group row">
								<label for="other countries " class="col-lg-4 control-label">Other Countries</label>
								<div class="col-lg-8">
									<select class="form-control select2" id=""  multiple name="other_countries[]" style="width:100%;">
										<?php
										$countries = $this->FrontEndModel->get('countries');   										
										foreach($countries as $data):?>
											<option value="<?php echo $data['id'];?>"><?php echo $data['country_name'];?></option>
										<?php endforeach;?>
									</select>
									
								</div>
							</div>
							<div class="form-group row">
								<label for="size" class="col-lg-4 control-label">Size</label>
								<div class="col-lg-8">
									<select class="form-control" name="size">
										<option value="1">Less then 50 employees</option>
										<option value="2">Between 50-200 employees</option>
										<option value="3">Greater then 200 employees</option>
									</select>

								</div>
							</div>
							<div class="form-group row">
								<label for="description" class="col-lg-4 control-label">Company Description/presentation</label>
								<div class="col-lg-8">
									<textarea type="text" class="form-control" id="company_description" name="company_description"></textarea>
									
								</div>
							</div>
							<div class="form-group row">
								<label for="Core" class="col-lg-4 control-label">Company Core Business</label>
								<div class="col-lg-8">
									<textarea type="text" class="form-control" id="company_core_business" name="company_core_business"></textarea>
									
								</div>
							</div>
							<div class="form-group row">
								<label for="Core" class="col-lg-4 control-label">Main Activities Managed</label>
								<div class="col-lg-8">
									<textarea type="text" class="form-control" id="main_activities" name="main_activities"></textarea>
									
								</div>
							</div>
							<div class="form-group row">
								<label for="References" class="col-lg-4 control-label">References</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" id="reference" name="reference" >
								</div>
							</div>
							<div class="form-group row">
								<label for="Contact phone" class="col-lg-4 control-label">Contact Phone</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" id="contact_phone" name="contact_phone" >
								</div>
							</div>
							
							<div class="form-group row">
								<label for="Contact Name" class="col-lg-4 control-label">Select Skill</label>
								<div class="col-md-8">
									<select class="form-control select2" id=""  multiple name="add_skill[]" style="width:100%;">
										<?php
										$skills=$this->FrontEndModel->get('skills');                  
										foreach($skills as $data):?>
											<option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
							
							<!-- Text input-->
							<div class="form-group row">
								<label for="Contact Name" class="col-lg-4 control-label">New Skill</label>
								<div class="col-lg-8">
									<select class="form-control select4" id=""  multiple name="skill[]" style="width:100%;">
										<?php
										$skills=$this->FrontEndModel->get('skills');                  
										foreach($skills as $data):?>
											<option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="Contact Name" class="col-lg-4 control-label">Upload Logo</label>
								<div class="col-lg-8">
									<input type="file" name="userfile" size="20" />
								</div>
							</div>


							<div class="form-group row">
								<label for="Contact Name" class="col-lg-4 control-label"></label>
								<div class="col-lg-8">

									<button type="Submit" class="btn btn-success" >Submit</button>
								</div>
							</div>

						</form>

					</div>
				</div>

			</div> 
		</div>
	</section>

</div>


<script type="text/javascript">
	$(document).ready(function(){
		$("#suplier_info").validate({
			rules:{
				company_name:{
					required:true,
				},
				name:{
					required:true,
				},
				email:{
					required:true,
				},
				industry:{
					required:true,
				},
			},
		});

		$('.select2').select2();
		$('.select4').select2({
			tags: true,
			tokenSeparators: [","],
		})
	});
</script>
