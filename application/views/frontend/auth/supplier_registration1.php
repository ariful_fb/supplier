<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-9">
	<base href="<?php echo base_url();?>">
	<title>Supplier Evaluation</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131392277-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-131392277-1');
	</script>

	
	<!-- <script src="assets/dist/fastselect.js"></script> --> 
	<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>
	<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/additional-methods.js"></script>
	<style type="text/css">
	#personal_information,
	#company_information{
		display:none;
	}

	

</style>
<!-- Select2 -->
<link href="assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

</head>
<body>
	<div class="container">
		<div class="col-md-10">
			<form class="form-horizontal" action="add_supplier" method="POST" id="myform">

				<fieldset id="account_information" class="">
					<input type="hidden" name="authCode" value="<?php echo $authCode;?>">
					<h1 class="text-center">Supplier Registration</h1><hr>
					<div class="form-group">
						<label for="nick_name" class="col-md-3 ">Username<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="nick_name" name="nick_name" >
						</div>
					</div>
					<div class="form-group">
						<label for="user_email" class="col-md-3 ">Email<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="email" class="form-control" id="user_email" name="user_email"  value="">
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 ">Password<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="password" class="form-control" id="password" name="password" >
						</div>
					</div><div class="form-group">
						<label for="password" class="col-md-3 ">Privacy 1:</label>
						<div class="col-md-9">
							<select class="form-control" name="privacy1">
								<option>Yes</option>
								<option>No</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 ">Privacy 2:</label>
						<div class="col-md-9">
							<select class="form-control" name="privacy2">
								<option>Yes</option>
								<option>No</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 ">Privacy 3:</label>
						<div class="col-md-9">
							<select class="form-control" name="privacy3">
								<option>Yes</option>
								<option>No</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 ">Disclaimer<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="checkbox" class="custom-control-input" name="check" id="check">
						</div>

					</div>

					<div class="form-group">
						<label for="password" class="col-md-3 "></label>
						<label for="remember-me"><span><input  required id="remember-me" name="remember_me" type="checkbox"></span><span> I accept the </span> </label> <a href="#" class="text-info">Terms & Conditions</a>
					</div>
					<div class="form-group">
						<label for="password" class="col-md-3 "></label>
						<p><a class="btn btn-primary next">Next</a></p>
					</div>
				</fieldset>


				<fieldset id="company_information" class="">
					<h1 class="text-center">Supplier’s structure</h1><hr>

					<div class="form-group">
						<label for="name" class="col-md-3 ">Name<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="name" name="name"  value="name">
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-md-3 ">Email<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="email" name="email"  value="dfd@gmail.com">
						</div>
					</div>
					<div class="form-group">
						<label for="main country " class="col-md-3 ">Main Country </label>
						<div class="col-md-9">
							<select class="form-control "  name="main_country">
								<?php						
								foreach($countries as $data):?>
									<option value="<?php echo $data['id'];?>"><?php echo $data['country_name'];?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="main City " class="col-md-3 ">Main City</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="main_city " name="main_city" >
						</div>
					</div>
					<div class="form-group">
						<label for="main City " class="col-md-3 ">Company Name<span class="text-danger">*</span></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="company_name " name="company_name" >
						</div>
					</div>

					<div class="form-group">
						<label for="industry" class="col-md-3 ">Industry</label>

						<div class="col-md-9">
							<select class="form-control"  name="industry" >
								<option value="">--Select Industry--</option>
								<?php foreach($industry as $data):?>
									<option value="<?php echo $data['id'];?>"><?php echo $data['industryName'];?></option>
								<?php endforeach;?>
							</select>

						</div>
					</div>
					<div class="form-group">
						<label for="other countries " class="col-md-3 ">Other Countries</label>
						<div class="col-md-9">
							<select class="form-control select2" id=""  multiple name="other_countries[]" style="width:100%;">
								<?php						
								foreach($countries as $data):?>
									<option value="<?php echo $data['id'];?>"><?php echo $data['country_name'];?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="size" class="col-md-3 ">Size</label>
						<div class="col-md-9">
							<select class="form-control" name="size">
								<option value="1">Less then 50 employees</option>
								<option value="2">Between 50-200 employees</option>
								<option value="3">Greater then 200 employees</option>
							</select>

						</div>
					</div>
					<div class="form-group">
						<label for="description" class="col-md-3 ">Company Description/presentation</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="company_description" name="company_description" >
						</div>
					</div>
					<div class="form-group">
						<label for="Core" class="col-md-3 ">Company Core Business</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="company_core_business" name="company_core_business" >
						</div>
					</div>
					<div class="form-group">
						<label for="Core" class="col-md-3 ">Main Activities Managed</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="main_activities" name="main_activities" >
						</div>
					</div>
					<div class="form-group">
						<label for="References" class="col-md-3 ">References</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="reference" name="reference" >
						</div>
					</div>
					<div class="form-group">
						<label for="Contact phone" class="col-md-3 ">Contact Phone</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="contact_phone" name="contact_phone" >
						</div>
					</div>
					<div class="form-group">
						<label for="Contact Name" class="col-md-3 ">Contact Name</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="contact_name" name="contact_name" >
						</div>
					</div>
					<div class="form-group">
						<label for="Contact Name" class="col-md-3 "></label>
						<a class="btn btn-primary" id="previous" >Previous</a>
						<a class="btn btn-primary next">Next</a>
					</div>
				</fieldset>

				<fieldset id="personal_information" class="">
					<h1 class="text-center">Skill</h1><hr>
					<div class="form-group">
						<label for="Contact Name" class="col-md-3 ">Select Skill</label>
						<div class="col-md-9">
							<select class="form-control select2" id=""  multiple name="add_skill[]" style="width:209px;">
								<?php
								$skills=$this->FrontEndModel->get('skills');									
								foreach($skills as $data):?>
									<option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>

					<!-- Text input-->
					<!-- <div id="items">
						<div  class="form-group">
							<label class="col-md-3 " for="textinput">Add Skill</label>
							<div class="col-md-3  row" >
								<input id="textinput" name="skill[]" type="text" placeholder="Enter name of referral" class="form-control input-md">
							</div>
						</div>
					</div>

					<button id="add" class="btn add-more button-yellow uppercase" type="button">+ Add another skill</button> -->
					<div class="form-group row">
						<label for="Contact Name" class="col-md-3 ">New Skill</label>
						<div class="col-md-9">
							<input type="text" class="form-control skills" id="contact_name" name="skill[]" >
						</div>
					</div>
					
					<div class="form-group">
						<label for="Contact Name" class="col-md-3 "></label>
						<a class="btn btn-primary" id="previous" >Previous</a>
						<input class="btn btn-success" type="submit" value="submit">
					</div>
				</fieldset>

			</form>
		</div>  
	</div>

	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="user_registerLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" >Disclaimer</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form  action="add_user" method="POST" id="supplier">

						<p id="res_error" class="text-center text-warning"></p>


						<div class="form-group">
							<p class="text-justify">Look3supplier will do it’s best to try to check the evaluations but look3supplier can’t check all the evaluation. Saving this evaluation you confirm that 1) you are not working for the supplier you are evaluating 2) you don’t have any personal relationship with the supplier 3) you didn’t receive any payment to do the evaluation 3) you certify that the evaluation is only based on your own experience and you know that a fake evaluations are forbidden.
								Look3supplier doesn’t have any responsability on the evaluation added by the users. The supplier could add an answer to the evaluation and could ask to delete the evaluation if the supplier demostrates it is a fake one
							In addition to being a violation of our terms of service and an unethical practice, committing fraud on reviews is also prohibited by the law and regulations in many jurisdictions</p>

						</div>
						<div  class="form-group">
							<p  id="check_yes"  class="btn btn-info btn-md">Yes</p>
							<p  id="check_no"  class="btn btn-info btn-md">No</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>




</body>
</html>

<script>

	$(document).ready(function() {
		$(".delete").hide();
  //when the Add Field button is clicked
  $("#add").click(function(e) {
  	$(".delete").fadeIn("1500");
    //Append a new row of code to the "#items" div
    $("#items").append(
    	'<div  class="form-group"><label class="col-md-3 " for="textinput">Skill</label><div class="col-md-3  row" ><input id="textinput" name="skill[]" type="text" placeholder="Enter name of referral" class="form-control input-md"></div></div>'
    	);
});
  $("body").on("click", ".delete", function(e) {
  	$(".next-referral").last().remove();
  });
});

	$(document).ready(function(){
		$(".select2").select2();
	});
	$(document).ready(function(){


	// Custom method to validate username
	$.validator.addMethod("usernameRegex", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
	}, "Username must contain only letters, numbers");

	$(".next").click(function(){
		var form = $("#myform");
		form.validate({
			errorElement: 'span',
			errorClass: 'help-block',
			highlight: function(element, errorClass, validClass) {
				$(element).closest('.form-group').addClass("has-error");
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).closest('.form-group').removeClass("has-error");
			},
			rules: {
				user_email: {
					required: true,
					remote:{
						url:"AdminController/emailCheck",
						type:"POST",
						data: {
							'request':'add',
						}

					}

				},
				nick_name: {
					required: true,

				},
				password : {
					required: true,
				},
				check : {
					required: true,
				},
				conf_password : {
					required: true,
					equalTo: '#password',
				},
				company_name:{
					required: true,
				},
				url:{
					required: true,
				},
				name: {
					required: true,
				},
				email: {
					required: true,
				},
				industry: {
					required: true,
				},

			},
			messages: {
				user_email:{
					remote:"This email already used!!"
				},
				nick_name: {
					required: "Nick name required",
				},
				password : {
					required: "Password required",
				},
				conf_password : {
					required: "Password required",
					equalTo: "Password don't match",
				},
				name: {
					required: "Name required",
				},
				email: {
					required: "Email required",
				},
			}
		});
		if (form.valid() === true){
			if ($('#account_information').is(":visible")){
				current_fs = $('#account_information');
				next_fs = $('#company_information');
			}else if($('#company_information').is(":visible")){
				current_fs = $('#company_information');
				next_fs = $('#personal_information');
			}

			next_fs.show(); 
			current_fs.hide();
		}
	});

	$('#previous').click(function(){
		if($('#company_information').is(":visible")){
			current_fs = $('#company_information');
			next_fs = $('#account_information');
		}else if ($('#personal_information').is(":visible")){
			current_fs = $('#personal_information');
			next_fs = $('#company_information');
		}
		next_fs.show(); 
		current_fs.hide();
	});

	$('#check').change(function(){

		if ($(this).prop('checked')) {
			$( "#check" ).prop( "checked", false );
			$("#myModal").modal('show');
		}

	});
	$('#check_yes').click(function(){
		$( "#check" ).prop( "checked", true );
		$("#myModal").modal('hide');
	});
	$('#check_no').click(function(){
		$("#myModal").modal('hide');
	});

	$('input.skills').typeahead({

		source:  function (query, process) {
				//alert(55);
				return $.get('search_skill', { query: query }, function (data) {
					data = $.parseJSON(data);
					console.log(data);
					return process(data);
				});
			}
		});


});
</script>
</script>
