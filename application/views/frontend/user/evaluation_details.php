
<div id="inner_main">
  <section class="evulation_details">
    <div class="container">
      <div class="evulation_details_top">
        <div class="row">
          <div class="col-sm-6">
            <div class="company_working">
             <div><img src="assets/frontend/img/w1.png" alt=""/> </div>
           </div>
         </div>
         <div class="col-sm-6">
          <div class="evulation_details_cont">
            <div class="evulation_details_cont_top">
              <h4>Rating </h4>
              <div id="avg_rating">

              </div>
            </div>

            <ul>
              <li class="text-justify"><?php echo $evaluation['inductions'];?></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="evulation_detail_mid">
      <h3>User Evulation</h3>
      <div class="review_details">
        <div class="table-responsive-sm">
          <table class="table ">

            <tbody>
              <tr>
                <th scope="row">
                  <div id="reliability"></div>  
                </th>
                <td>Reliability</td>


              </tr>
              <tr>
                <th scope="row">
                  <div id="flexibility"></div>     
                </th>
                <td>Flexibility</td>


              </tr> 
              <tr>
                <th scope="row">
                  <div id="cost"></div>    
                </th>
                <td>Cost/quality</td>
              </tr> 

              <tr>
                <th scope="row">
                  <div id="global_quality"></div>      
                </th>
                <td>Global quality of the work</td>


              </tr> 
              <tr>
                <th scope="row">
                  <div id="people_skills"></div>    
                </th>
                <td>Supplier’s people skills </td>


              </tr>  
              <tr>
                <th scope="row">
                  <div id="supplier_people"></div>  
                </th>
                <td>Opened to talk with all the supplier’s people</td>


              </tr>
              <tr>
                <th scope="row">
                  <div id="adapt"></div> 	   
                </th>
                <td>Open to changes/to adapt to the changes without burocratize</td>


              </tr> 
              <tr>
                <th scope="row">
                  <div id="engagement"></div>    
                </th>
                <td>Supplier’s people engagement</td>


              </tr> 
              <tr>
                <th scope="row">
                  <div id="knowledge"></div> 	   
                </th>
                <td>Supplier’s people business knowledge</td>


              </tr> 
              <tr>
                <th scope="row">
                  <div id="propositivity"></div>    
                </th>
                <td>Propositivity</td>


              </tr> 
              <tr>
                <th scope="row">
                  <div id="proactivity"></div>    
                </th>
                <td>Proactivity</td>


              </tr> 
            </tbody>
          </table>
        </div>
      </div>
    </div>


    <div class="evulation_detail_bottom">
      <h3>User Evulation Details</h3>
      <div class="evulation_review_details">
        <div class="evulation_review_profiless">
          <div class="evulation_review_profiless_img">
            <img src="assets/frontend/img/ps1.png" alt=""/>
          </div>
          <h3>  D. C Smith</h3>

          <span class="not-rate  review_details_r">
            <i class="fa fa-star active" aria-hidden="true"></i>
            <i class="fa fa-star active" aria-hidden="true"></i>
            <i class="fa fa-star active" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            0.0
          </span>    
        </div>
        <p>I like Wired. It's a great magazine. One problem though, when I ordered, it explicitly said I would receive the digital version along with the physical magazine. (The latter is what I ordered.) I inquired about this, and they said, "No, it's only the physical magazine; no digital version." So something is a bit amiss in how they describe this on Amazon's page. In any event. I got it cheap, and may get the digital version on its own as well. I just don't like that the description has me feeling deceived as to what I'd be getting.</p>
        <h5><a href="#"  data-toggle="modal" data-target="#new_evaluation">Suppliers Answers</a></h5>
        <?php if(isset($this->session->role) && $this->session->role==3 && $evaluation['answer_creator_id']==null){?>
          <form method="post" action="submit_answer" >
            <input type="hidden" name="answer_creator_id" value="<?php if(isset($this->session->user_id))echo $this->session->user_id;?>">
            <input type="hidden" name="evaluationId" value="<?php if(isset($evaluation['evaluationId']))echo $evaluation['evaluationId'];?>">
            <div class="form-group"> 
              <textarea name="answer" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
            </div>
            <button type="submit" class="btn btn-success float-right"  >Submit Answer</button>
          </form>
        <?php }?>
      </div>
    </div>


    <div class="evulation_detail_bottom">
      <h3>Request to Delete</h3>
      <div class="evulation_review_details">

        <?php if(isset($this->session->role) && $this->session->role==3){?>
          <form method="post" action="submit_request" >
            <input type="hidden" name="creator_id" value="<?php if(isset($this->session->user_id))echo $this->session->user_id;?>">
            <input type="hidden" name="evaluation_id" value="<?php if(isset($evaluation['evaluationId']))echo $evaluation['evaluationId'];?>">
            <div class="form-group"> 
              <textarea name="reason" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
            </div>
            <button type="submit" class="btn btn-success float-right"  >Submit Request</button>
          </form>
        <?php }?>
      </div>
    </div>


  </div> 
</section>
<!-- === end search result=== -->
<!-- ==== ss top supplier  ==== -->


<!-- ==== ss end supplier  ==== -->
</div>

<!-- modal new_evaluation -->
<div class="modal fade" id="new_evaluation" tabindex="-1" role="dialog" aria-labelledby="new_evaluationLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Add new evaluation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>

          <div class="login_form">
            <div class="form-group"> 
              <input type="text" placeholder="Suppliers  Name" name="username" id="username" class="form-control">
            </div>
            <div class="form-group row">
              <div class="col-sm-3">Disclaimer</div>
              <div class="col-sm-3">
                <select class="form-control">
                  <option>Yes</option>
                  <option>No</option>
                </select>
              </div>
            </div>
            <div class="form-group"> 
              <input type="email" placeholder="Email" name="Email" id="Email" class="form-control">
            </div>
            <div class="form-group"> 
              <input type="tel" placeholder="Phone" name="Phone" id="Phone" class="form-control">
            </div>
            <div class="form-group"> 
              <input type="text" placeholder="Address" name="Address" id="Address" class="form-control">
            </div>
            <div class="form-group">
              <span class="not-rate  review_details_r">
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>

              </span>   
              Lorem Ipsum is  
            </div>
            <div class="form-group">
              <span class="not-rate  review_details_r">
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i> 


              </span>    
              simply dummy 
            </div>
            <div class="form-group">
              <span class="not-rate  review_details_r">
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>


              </span>  
              text of the printing   
            </div>
            <div class="form-group">
              <span class="not-rate  review_details_r">
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>


              </span>   
              and typesetting  
            </div>
            <div class="form-group">
              <span class="not-rate  review_details_r">
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star active" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>

              </span>
              industry.    
            </div>

            <div  class="form-group">
              <input type="submit" name="submit" class="btn btn-success btn-md" value="Ok">
            </div>


          </div>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(function () {

      $("#reliability").rateYo({
        rating: <?php echo $evaluation['reliability'];?>,
        spacing: "20px",
        readOnly: true
      });
      $("#flexibility").rateYo({
        rating: <?php echo $evaluation['flexibility'];?>,
        spacing: "20px",
        readOnly: true
      }); 
      $("#cost").rateYo({
        rating: <?php echo $evaluation['cost'];?>,
        spacing: "20px",
        readOnly: true
      });
      $("#global_quality").rateYo({
        rating: <?php echo $evaluation['global_quality'];?>,
        spacing: "20px",
        readOnly: true
      }); 
      $("#people_skills").rateYo({
        rating: <?php echo $evaluation['people_skills'];?>,
        spacing: "20px",
        readOnly: true
      });
      $("#supplier_people").rateYo({
        rating: <?php echo $evaluation['supplier_people'];?>,
        spacing: "20px",
        readOnly: true
      }); 
      $("#adapt").rateYo({
        rating: <?php echo $evaluation['adapt'];?>,
        spacing: "20px",
        readOnly: true
      });
      $("#engagement").rateYo({
        rating: <?php echo $evaluation['engagement'];?>,
        spacing: "20px",
        readOnly: true
      }); 
      $("#knowledge").rateYo({
        rating: <?php echo $evaluation['knowledge'];?>,
        spacing: "20px",
        readOnly: true
      });
      $("#propositivity").rateYo({
        rating: <?php echo $evaluation['propositivity'];?>,
        spacing: "20px",
        readOnly: true
      });
      $("#proactivity").rateYo({
        rating: <?php echo $evaluation['proactivity'];?>,
        spacing: "20px",
        readOnly: true
      }); 
      $("#avg_rating").rateYo({
        rating: <?php echo $evaluation['avg_rating'];?>,
        spacing: "20px",
        readOnly: true
      });

    });
  });
</script>