<div id="inner_main">
  <section class="search_result">
    <div class="container">
      <div class="row">


        <?php foreach ($evaluation as $data) : ?>
          <div class="col-sm-3">
            <div class="top_s_item">
              <div class="top_s_img"> 
                <a href="user_evaluation/details/<?php echo  $data['id']; ?>"> 
                  <img src="assets/frontend/img/c4.png"/> 
                </a> 
              </div>
              <h3> <a href="#"> <?php echo $data['company_name']; ?> </a> </h3>
              <input id="input-id" name="input-name" type="number" class="rating" data-size="xs" data-rtl="false" readonly="true" value="<?php echo $data['avg_rating']; ?>">
              <ul>
                <span><?php echo substr($data['customer_company'], 0, 100) ?></span>
              </ul>
              <a href="user_evaluation/details/<?php echo  $data['id']; ?>" class=" ss_r_btn">More info</a>
            </div>
          </div>
        <?php endforeach; ?> 
      </div>
      <div class="float-right">
        <nav aria-label="Page navigation example ">
          <?php echo $links; ?>
        </nav>
      </div>

    </div>
    <div class="p-3">
    </div>
  </section>

  <!-- === end search result=== -->


</div>


<div class="modal fade" id="modalNickname" tabindex="-1" role="dialog" aria-labelledby="user_loginlLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Nick Name</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="addNickname" method="POST" id="addNickname">
          <input type="hidden" name="user_id" value="<?php echo $user_id?>">
          <div class="form-group"> 
            <input type="text" placeholder="Nick Name" name="nick_name" id="" class="form-control">
          </div>

          <div  class="form-group">

            <button type="submit" class="btn btn-info btn-md">Save</button>
          </div>
        </div>
      </form>
    </div>

  </div>
</div>


<script type="text/javascript">
  $(function(){
    var nick_name=<?php if ($nick_name) {
      echo $nick_name;
    } else {
      echo "null";
    }?>;
    if(nick_name==null){
      $("#modalNickname").modal('show');
    }
  });
  $(document).ready(function(){
    $( "#addNickname" ).validate({
      rules:{
        nick_name:{
          required:true,
          remote:{
            url:"AdminController/nameCheck",
            type:"POST",
            data: {
              'request':'add',
            }
          }
        }
      },
      messages: {
        nick_name:{
          remote:"Name already used!!"
        }
      }

    });

    $(function () {
      $(".rateyo").rateYo({ readOnly: true}).on("rateyo.change", function (e, data) {

      });
      $("#rateYoFrontPage").rateYo({
        rating: 2.6,
        spacing: "20px"
      });

    });

  });

</script>
