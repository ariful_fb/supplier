<div id="inner_main">

	<div class="container">
		<button onclick="goBack()" class="btn btn-success">Go Back</button>

		<script>
			function goBack() {
				window.history.back();
			}
		</script>
		<div class="row">
			<div class="col-sm-12">
				<div class="evulation_details_cont">
					<div class="evulation_details_cont_top">
						<h3 class="text-center"><b>Activity Description Parameters</b></h3><br>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<ul>
								<li><b>Inductions :</b> <?php if ($evaluation['inductions']) {
									echo $evaluation['inductions'];
								} else {
									echo "Not present";
								}?></li>
								<li><b>Activity Type:</b> <?php if ($evaluation['activity_type']==1) {
									echo "Business";
								} else {
									echo "Private";
								}?>
							</li>

							<li>
								<b>Activity Duration :</b> <?php
								if ($evaluation['activity_duration']==1) {
									echo "More than 1 year service";
								} elseif ($evaluation['activity_duration']==2) {
									echo "Less than 1 year service";
								} elseif ($evaluation['activity_duration']==3) {
									echo "project";
								} elseif ($evaluation['activity_duration']==4) {
									echo "On demand activity";
								} elseif ($evaluation['activity_duration']==5) {
									echo "Other Value";
								} else {
									echo "Not present";
								}
								?>
							</li>



						</ul>
					</div>
					<div class="col-sm-6">
						<ul>
							<li>
								<b>Budget :</b> <?php
								if ($evaluation['budget']==1) {
									echo "0-50.000€";
								} elseif ($evaluation['budget']==2) {
									echo "50.000€-100.000€";
								} elseif ($evaluation['budget']==3) {
									echo "100.000€-500.000€";
								} elseif ($evaluation['budget']==4) {
									echo "500.000€-1.000.000€";
								} elseif ($evaluation['budget']==5) {
									echo "More then 1.000.000€";
								} else {
									echo "Not present";
								}
								?>
							</li>
							<li><b>Customer Company :</b> <?php if ($evaluation['customer_company']) {
								echo $evaluation['customer_company'];
							} else {
								echo "Not present";
							}?></li>

							<li>
								<b>User Company:</b> <?php
								if ($evaluation['user_company']==1) {
									echo "Less than 100 employees";
								} elseif ($evaluation['user_company']==2) {
									echo "Between 100-500 employees";
								} elseif ($evaluation['user_company']==3) {
									echo "Greater than 500 employees";
								} elseif ($evaluation['user_company']==4) {
									echo "private";
								} else {
									echo "Not present";
								}
								?></li>
								
								<li>
									<b>Skills: </b><?php echo strlen($skills)>2 ? $skills : 'Not Present'; ?>
								</li>

							</ul>

						</div>
					</div>



				</div>

			</div>
		</div>

		<div class="evulation_detail_mid">
			<h3 class="text-center">Evaluation parameters </h3>

			<div class="review_details1">
				<div class="table table-striped table-bordered">
					<table class="table ">

						<tbody>
							<tr>
								<th scope="row">
									<!-- <div id="reliability"></div> -->
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['reliability'];?>">  
								</th>
								<td>Reliability</td>


							</tr>
							<tr>
								<th scope="row">
									<!-- <div id="flexibility"></div> -->
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['flexibility'];?>">
								</th>
								<td>Flexibility</td>


							</tr> 
							<tr>
								<th scope="row">
									<!-- <div id="cost"></div>     -->
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['cost'];?>">
								</th>
								<td>Cost/quality</td>
							</tr> 

							<tr>
								<th scope="row">
									<!-- <div id="global_quality"></div> -->      
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['global_quality'];?>">
								</th>
								<td>Global quality of the work</td>


							</tr> 
							<tr>
								<th scope="row">
									<!-- <div id="people_skills"></div> -->    
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['people_skills'];?>">
								</th>
								<td>Supplier’s people skills </td>


							</tr>  
							<tr>
								<th scope="row">
									<!-- <div id="supplier_people"></div>   -->
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['supplier_people'];?>">
								</th>
								<td>Opened to talk with all the supplier’s people</td>


							</tr>
							<tr>
								<th scope="row">
									<!-- <div id="adapt"></div> -->     
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['adapt'];?>">
								</th>
								<td>Open to changes/to adapt to the changes without burocratize</td>


							</tr> 
							<tr>
								<th scope="row">
									<!-- <div id="engagement"></div> -->
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['engagement'];?>">
								</th>
								<td>Supplier’s people engagement</td>


							</tr> 
							<tr>
								<th scope="row">
									<!-- <div id="knowledge"></div> -->
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['knowledge'];?>">     
								</th>
								<td>Supplier’s people business knowledge</td>


							</tr> 
							<tr>
								<th scope="row">
									<!-- <div id="propositivity"></div> -->    
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['propositivity'];?>">

								</th>
								<td>Propositivity</td>


							</tr> 
							<tr>
								<th scope="row">
									<!-- <div id="proactivity"></div> -->    
									<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $evaluation['proactivity'];?>">
								</th>
								<td>Proactivity</td>


							</tr> 
						</tbody>
					</table>
				</div>
			</div>
		</div>


		<div class="evulation_detail_bottom">
			
			<div class="evulation_review_details">
				
				<?php if ($evaluation['answer_creator_id']) {?>
					<h3>Supplier Answer </h3>
					<div class="evulation_review_profiless">
						<div class="evulation_review_profiless_img">
							<img src="assets/frontend/img/ps1.png" alt=""/>
						</div>
						<h3><?php if ($evaluation['answer_creator_id']) {
							echo creatorName($evaluation['answer_creator_id']);
						}?></h3>
					</div>
					<p class="text-justify"><?php if ($evaluation['answer_creator_id']) {
						echo $evaluation['answer'];
					}?></p>
				<?php } else if($this->session->role == 3 && $evaluation['supplier_id']==$this->session->user_id){?>
					<h3>Supplier Answer </h3>
					<form method="post" action="submit_answer" >
						<input type="hidden" name="answer_creator_id" value="<?php if (isset($this->session->user_id)) {
							echo $this->session->user_id;
						}?>">
						<input type="hidden" name="evaluationId" value="<?php if (isset($evaluation['evaluationId'])) {
							echo $evaluation['evaluationId'];
						}?>">
						<div class="form-group"> 
							<textarea name="answer" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
						</div>
						
						<button type="submit" class="btn btn-success float-right"  id="checkAnswer">Submit Answer</button>
						
					</form>
				<?php }?>

			</div>
		</div>

		<div class="evulation_detail_bottom">
			<?php if (isset($this->session->role) && $this->session->role==3 && $evaluation['supplier_id'] == $this->session->user_id){?>
				<h3>Request to Delete</h3>
			<?php }?>
			<div class="evulation_review_details">

				<?php if (isset($this->session->role) && $this->session->role==3 && $evaluation['supplier_id'] == $this->session->user_id){?>
					<form method="post" action="submit_requests" >
						<input type="hidden" name="creator_id" value="<?php if (isset($this->session->user_id)) {
							echo $this->session->user_id;
						}?>">
						<input type="hidden" name="evaluation_id" value="<?php if (isset($evaluation['evaluationId'])) {
							echo $evaluation['evaluationId'];
						}?>">
						<div class="form-group"> 
							<textarea name="reason" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
						</div>
						<button type="submit" class="btn btn-success float-right"  >Submit Request</button>
					</form>
				<?php }?>
			</div>
		</div>





	</section> 


</div>

<!-- modal new_evaluation -->


<script type="text/javascript">
	/*$(document).ready(function(){

		$(function () {

			$("#reliability").rateYo({
				rating: <?php echo $evaluation['reliability'];?>,
				spacing: "35px",
				readOnly: true
			});
			$("#flexibility").rateYo({
				rating: <?php echo $evaluation['flexibility'];?>,
				spacing: "35px",
				readOnly: true
			}); 
			$("#cost").rateYo({
				rating: <?php echo $evaluation['cost'];?>,
				spacing: "35px",
				readOnly: true
			});
			$("#global_quality").rateYo({
				rating: <?php echo $evaluation['global_quality'];?>,
				spacing: "35px",
				readOnly: true
			}); 
			$("#people_skills").rateYo({
				rating: <?php echo $evaluation['people_skills'];?>,
				spacing: "35px",
				readOnly: true
			});
			$("#supplier_people").rateYo({
				rating: <?php echo $evaluation['supplier_people'];?>,
				spacing: "35px",
				readOnly: true
			}); 
			$("#adapt").rateYo({
				rating: <?php echo $evaluation['adapt'];?>,
				spacing: "35px",
				readOnly: true
			});
			$("#engagement").rateYo({
				rating: <?php echo $evaluation['engagement'];?>,
				spacing: "35px",
				readOnly: true
			}); 
			$("#knowledge").rateYo({
				rating: <?php echo $evaluation['knowledge'];?>,
				spacing: "35px",
				readOnly: true
			});
			$("#propositivity").rateYo({
				rating: <?php echo $evaluation['propositivity'];?>,
				spacing: "35px",
				readOnly: true
			});
			$("#proactivity").rateYo({
				rating: <?php echo $evaluation['proactivity'];?>,
				spacing: "35px",
				readOnly: true
			}); 
			$("#avg_rating").rateYo({
				rating: <?php echo $evaluation['avg_rating'];?>,
				spacing: "35px",
				readOnly: true
			});

		});
	});*/
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#checkAnswer").click(function(){
			var checkAnswer=<?php if(isset($evaluation['answer_creator_id']))echo $evaluation['answer_creator_id'];?>;
			if(checkAnswer){
				swal({
					text: "Answer already submited",
					icon: "success",
				});
			}else
			return true;
			return false;

		});


	});

</script>
