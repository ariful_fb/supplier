 <!-- success and error msg showing area -->
 <br><br><div class="row"> 
  <div class="col-md-1"> 
  </div>               
  <div class="col-md-10"> 
    <?php if($this->session->userdata('error_msg')){ ?>
      <div class="alert alert-danger">                    
        <span><?=$this->session->userdata('error_msg');?></span>
        <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
      </div>
    <?php } $this->session->unset_userdata('error_msg');?>
    <?php if($this->session->userdata('success_msg')){ ?>
      <div class="alert alert-success">                    
        <span><?=$this->session->userdata('success_msg');?></span>
        <a style="float: right;" href="#" class="close" data-dismiss="alert">&times;</a>
      </div>
    <?php } $this->session->unset_userdata('success_msg'); ?>
  </div>  
  <div class="col-md-1"> 
  </div>              
</div>
          <!-- success and error msg showing area end-->