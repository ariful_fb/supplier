<div class="content-page">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-12">
          <div class="row">
            <div class="col xl-6">
             <div class="card">
              <div class="card-body">
                <h4 class="header-title">Input Types</h4>

                <div class="row">
                  <div class="col-lg-6">
                    <form action="EvaluationController/addEvaluation" method="POST">
                      <div class="form-group mb-3">
                        <label for="example-textarea">Text area</label>
                        <textarea class="form-control" id="example-textarea" rows="5" name="induction"></textarea>
                      </div>
                       <div class="form-group mb-3">
                        <label for="example-select">Rating</label>
                        <select class="form-control" id="example-select" name="rating">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      </div> 
                      <input type="submit" class="btn btn-default" value="submit">

                    </form>
                  </div> <!-- end col -->

                </div>
                <!-- end row-->

              </div> <!-- end card-body -->
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
