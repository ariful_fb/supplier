<div id="inner_main">
  <section >
    <!-- class="evulation_details" -->
    <div class="container">
      <div class="row">
        <div class="col-md-10">

          <form action="profile/update/<?php echo $_SESSION['user_id'] ?>" method="POST" id="suplier_info" enctype="multipart/form-data">
           <div class="form-group row">
            <label for="main City " class="col-lg-4 control-label">Company Name<span class="text-danger h4">*</span></label>
            <div class="col-lg-8">
              <input type="text" value="<?php if(isset($userInfo['company_name']))echo $userInfo['company_name'];?>" class="form-control" id="company_name " name="company_name" placeholder="">
            </div>
          </div>

          <div class="form-group row">
            <label for="industry" class="col-lg-4 control-label">Industry<span class="text-danger h4">*</span></label>

            <div class="col-md-8">
              <select class="form-control"  name="industry" >
                <option value="">--Select Industry--</option>
                <?php foreach($industry as $data):?>
                  <option value="<?php echo $data['id'];?>" <?php if($userInfo['industry']==$data['id'])echo "selected";?>><?php echo $data['industryName'];?></option>
                <?php endforeach;?>
              </select>

            </div>
          </div>

          <div class="form-group row">
            <label for="email" class="col-lg-4 control-label">Contact Email<span class="text-danger h4">*</span></label>
            <div class="col-lg-8">
              <input type="email" value="<?php if(isset($userInfo['email']))echo $userInfo['email'];?>" class="form-control" id="email " name="email" >
            </div>
          </div>

          <div class="form-group row">
            <label for="Contact Name" class="col-lg-4 control-label">Contact Name</label>
            <div class="col-lg-8">
              <input type="text" value="<?php if(isset($userInfo['contact_name']))echo $userInfo['contact_name'];?>" class="form-control" id="contact_name" name="contact_name" >
            </div>
          </div>

          <div class="form-group row">
            <label for="main country" class="col-lg-4 control-label"><?php echo $userInfo['main_country'];?>Main Country </label>
            <div class="col-lg-8">
              <select class="form-control"  name="main_country" >
                <?php foreach($countries as $data):?>
                  <option value="<?php echo $data['country_name'];?> <?php if(trim($userInfo['main_country'])==trim($data['country_name']))echo "selected";?>"><?php echo $data['country_name'];?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="main City " class="col-lg-4 control-label">Main City</label>
            <div class="col-lg-8">
              <input type="text" value="<?php if(isset($userInfo['main_city']))echo $userInfo['main_city'];?>" class="form-control" id="main_city " name="main_city" >
            </div>
          </div>



          <div class="form-group row">
            <label for="other countries " class="col-lg-4 control-label">Other Countries</label>
            <div class="col-lg-8">
              <select class="form-control select2" id=""  multiple name="other_countries[]" style="width:100%;">
                <?php
                $countries = $this->FrontEndModel->get('countries');                      
                foreach($countries as $data):
                  if(empty($userInfo['other_countries'])){?>
                    <option value="<?php echo $data['id'];?>"><?php echo $data['country_name'];?></option>
                  <?php }else{$i=0;$tot=count($userInfo['other_countries']);
                  while($tot--){?>
                    <option value="<?php echo $data['id'];?>" <?php if($data['id']==$userInfo['other_countries'][$i++])echo "selected";?>><?php echo $data['country_name'];?></option>
                  <?php }}?>
                <?php endforeach;?>
              </select>

            </div>
          </div>
          <div class="form-group row">
            <label for="size" class="col-lg-4 control-label">Size</label>
            <div class="col-lg-8">
              <select class="form-control" name="size">
                <option value="1" <?php if($userInfo['size']==1)echo "selected";?>>Less then 50 employees</option>
                <option value="2" <?php if($userInfo['size']==2)echo "selected";?>>Between 50-200 employees</option>
                <option value="3" <?php if($userInfo['size']==3)echo "selected";?>>Greater then 200 employees</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="description" class="col-lg-4 control-label">Company Description/presentation</label>
            <div class="col-lg-8">
              <textarea type="text" value="" class="form-control" id="company_description" name="company_description"><?php if(isset($userInfo['company_description']))echo $userInfo['company_description'];?></textarea>

            </div>
          </div>
          <div class="form-group row">
            <label for="Core" class="col-lg-4 control-label">Company Core Business</label>
            <div class="col-lg-8">
              <textarea type="text" class="form-control" id="company_core_business" name="company_core_business"><?php if(isset($userInfo['company_core_business']))echo $userInfo['company_core_business'];?></textarea>

            </div>
          </div>
          <div class="form-group row">
            <label for="Core" class="col-lg-4 control-label">Main Activities Managed</label>
            <div class="col-lg-8">
              <textarea type="text" class="form-control" id="main_activities" name="main_activities">
                <?php if(isset($userInfo['main_activities']))echo $userInfo['main_activities'];?>
              </textarea>

            </div>
          </div>
          <div class="form-group row">
            <label for="References" class="col-lg-4 control-label">References</label>
            <div class="col-lg-8">
              <input type="text" value="<?php if(isset($userInfo['reference']))echo $userInfo['reference'];?>" class="form-control" id="reference" name="reference" >
            </div>
          </div>
          <div class="form-group row">
            <label for="Contact phone" class="col-lg-4 control-label">Contact Phone</label>
            <div class="col-lg-8">
              <input type="text" value="<?php if(isset($userInfo['contact_phone']))echo $userInfo['contact_phone'];?>" class="form-control" id="contact_phone" name="contact_phone" >
            </div>
          </div>

          <div class="form-group row">
            <label for="Contact Name" class="col-lg-4 control-label">Select Skill</label>
            <div class="col-md-8">
              <select class="form-control select2" id=""  multiple name="add_skill[]" style="width:100%;">
                <?php               
                foreach($skills as $data):
                  if(empty($s_skill)){?>
                    <option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
                  <?php }else{$i=0;$tot=count($s_skill);
                    while($tot--){?>
                      <option value="<?php echo $data['id'];?>" <?php if($data['id']==$s_skill[$tot]['skill_id'])echo "selected";?>><?php echo $data['name'];?></option>
                    <?php }}?>
                  <?php endforeach;?>
                </select>
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group row">
              <label for="Contact Name" class="col-lg-4 control-label">New Skill</label>
              <div class="col-lg-8">
                <select class="form-control select4" id=""  multiple name="skill[]" style="width:100%;">
                  <?php                  
                  foreach($skills as $data):?>
                    <option value="<?php echo $data['id'];?>"><?php echo $data['name'];?></option>
                  <?php endforeach;?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="Contact Name" class="col-lg-4 control-label">Upload Logo</label>
              <div class="col-lg-">
               <img height="200" width="200" src="<?php if(isset($userInfo['logo'])
               && !empty($userInfo['logo']))echo $userInfo['logo'];else echo"assets/images/users/default_avatar.png"?>?>">
             </div> 
             <div class="col-lg-4">
              <input type="file" name="userfile" size="20" />
            </div>
          </div>


          <div class="form-group row">
            <label for="Contact Name" class="col-lg-4 control-label"></label>
            <div class="col-lg-8">

              <button type="Submit" class="btn btn-success" >Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>



</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#suplier_info").validate({
      rules:{
        company_name:{
          required:true,
        },
        name:{
          required:true,
        },
        email:{
          required:true,
        },
        industry:{
          required:true,
        },
      },
    });

    $('.select2').select2();
    $('.select4').select2({
      tags: true,
      tokenSeparators: [","],
    })
  });
</script>