<div id="inner_main">
	<section >
		<!-- class="evulation_details" -->
		<div class="container">
			<div class="evulation_details_top">
				<button id="search" class="btn btn-success">Back</button>
				<div class="row">
					<div class="col-sm-12">
						<div class="evulation_details_cont">
							<div class="evulation_details_cont_top">
								<h3 class="text-center"><b>
									<?php if ($supplier['company_name']) {
										echo $supplier['company_name'];
									} else {
										"Not present";
									}?></b>
								</h3><br>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<ul>
										<li>
											<b>Company Description :</b> <?php echo ($supplier['company_description']) ? $supplier['company_description']:"Not present";?>
										</li>
										<li>
											<b>Company Core Business :</b> <?php echo ($supplier['company_core_business']) ? $supplier['company_core_business']:"Not present";?>
										</li>
										<li>
											<b>Main Activities :</b> <?php echo ($supplier['main_activities']) ? $supplier['main_activities']:"Not present";?>
										</li>
										<li>
											<b>Supplier Skills :</b> <?php echo ($skill_data) ? $skill_data:"Not present";?>
										</li>
										<li>
											<b>Reference :</b> <?php echo ($supplier['reference']) ? $supplier['reference']:"Not present";?>
										</li>
										<li>
											<b>Industry :</b> <?php echo ($supplier['industryName']) ? $supplier['industryName']:"Not present";?>
										</li>

										<li>
											<?php $sizArr = ['','<50 employees', '50-200 employees', '>200 employees']; ?>
											<b>Size :</b> <?php echo ($supplier['size']) ? $sizArr[$supplier['size']]:"Not present";?>
										</li> 


									</ul>
								</div>
								<div class="col-sm-6">
									<ul>
										<li>
											<b>Contact Name :</b> <?php echo ($supplier['contact_name']) ? $supplier['contact_name']:"Not present";?>
										</li>
										<li>
											<b>Email :</b> <?php echo ($supplier['user_email']) ? $supplier['user_email']:"Not present";?>
										</li>
										<li>
											<b>Main Country :</b> <?php echo ($supplier['main_country']) ? $supplier['main_country']:"Not present";?>
										</li>

										<li>
											<b>City :</b> <?php echo ($supplier['main_city']) ? $supplier['main_city']:"Not present";?>
										</li>
										<li>
											<b>State :</b> <?php echo ($supplier['state']) ? $supplier['state']:"Not present";?>
										</li>

										<li>
											<b>Other Countries :</b> <?php echo $otherCountries;?>
										</li>

										<li>
											<b>Contact Phone :</b> <?php echo ($supplier['contact_phone']) ? $supplier['contact_phone']:"Not present";?>
										</li>


									</ul>

								</div>
							</div>



						</div>

					</div>
				</div>

				<div class="row" style="margin-bottom: 5%">
					<div class="col-md-1"></div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-body">
								<canvas id="myChart" width="200" height="200"></canvas>   
							</div>
						</div>
					</div>
					<div class="col-md-2"></div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-body" >
								<h5 class='text-center'><?php echo 'Average '. round($supplier['rating'], 1)." out of 5" ?></h5>
								<table class="table-borderless" style="margin: 0 auto;">
									<tbody>
										<tr>
											<td>
												<input id="reliability" name="input-name" type="number" class="star-rating" data-size="xs" data-rtl="false" readonly="true" value="5">
											</td>
											<td><?php echo $recapData[5] ?></td>

										</tr> 
										<tr>
											<td>
												<input id="flexibility" name="input-name" type="number" class="star-rating" data-size="xs" data-rtl="false" readonly="true" value="4">
											</td>
											<td><?php echo $recapData[4] ?></td>
										</tr><tr>
											<td>
												<input id="cost" name="input-name" type="number" class="star-rating" data-size="xs" data-rtl="false" readonly="true" value="3">
											</td>
											<td><?php echo $recapData[3] ?></td>
										</tr><tr>
											<td>
												<input id="global_quality" name="input-name" type="number" class="star-rating" data-size="xs" data-rtl="false" readonly="true" value="2">
											</td>
											<td> <?php echo $recapData[2] ?> </td>
										</tr><tr>
											<td>
												<input id="people_skills" name="input-name" type="number" class="star-rating" data-size="xs" data-rtl="false" readonly="true" value="1">
											</td>
											<td><?php echo $recapData[1] ?></td>
										</tr>
									</tbody>
								</table>;
							</div>
						</div>
					</div>
				</div>


				<div class="evulation_detail_mid">
					<h3 class="text-center">User Evaluation</h3>
					<button class="btn btn success mb-5" data-toggle="modal" data-target="#new_evaluation">New  Evaluation</button>
					<div class="review_details1">
						<div class="table-responsive-sm">
							<?php if (empty($user_evaluation)) {
								echo "<p class=' text-center'>Evaluation not present, add the first one</p>";
							} else {?>
								<table class="table">
									<thead>
										<tr>
											<th><b>Nick Name</b></th>
											<th><b>Rating</b></th>
											<th><b>Brief Induction</b></th>
											<th><b>Skills</b></th>
											<th><b>Actions</b></th>
										</tr>
									</thead>

									<tbody>
										<?php foreach ($user_evaluation as $data) : ?>
											<tr id="<?php echo $data['id']; ?>" rating="<?php echo $data['avg_rating']; ?>">
												<td><?php echo $data['nick_name']; ?></td>
												<td class='evalRecapPop' data-placement="bottom"  data-trigger="hover" data-toggle="popover">
													<input id="input-id" name="input-name" type="number" class="star-rating" data-size="sm" data-rtl="false" readonly="true" value="<?php echo $data['avg_rating']; ?>">
												</td>

												<td><?php echo substr($data['inductions'], 0, 30); ?></td>
												<td><?php echo substr($data['skill'], 0, 30); ?></td>
												<td><strong><a href="user_evaluation/details/<?php echo $data['id']?>">Details</a></strong></td>

											</tr>
										<?php endforeach; ?>  
									</tbody>

								</table>
							<?php }?>  
						</div>
					</div>
				</div>

			</div> 
		</section>
		<!-- === end search result=== -->
		<!-- ==== ss top supplier  ==== -->
		

		<!-- ==== ss end supplier  ==== -->
	</div>

	<div id="radarData" style="display:none"><?php echo $radarData; ?></div>

	<script type="text/javascript">
		$(document).ready(function(){
			$(".rateyo_user").rateYo({readOnly: true,spacing: "35px",}).on("rateyo.change", function (e, data) {

			});
		});
		$(document).ready(function(){
			$(".rateyo_ev").rateYo({readOnly: true}).on("rateyo.change", function (e, data) {

			});
		});


    //radar diagram
    var radarData = $('#radarData').html();
    radarData = JSON.parse(radarData);
    var ratingValues = [];
    var ratingUnit = [];
    $.each(radarData,  function(key, item){
    	ratingValues.push(item);
    	ratingUnit.push(key);
      /*var id = '#'+key;
      console.log(id);
      $(id).val(item);*/
  })  

    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
    	type: 'radar',
    	data: {
        //labels: ["Terrible", "Poor", "Average", "Very Good", "Excellent"],
        labels: ratingUnit,//["1", "2", "3", "4", "5","4", "5"],
        datasets: [{
        	label: 'Average',
          data: ratingValues,//[12, 13, 9, 6, 7],
          borderWidth: 1,
          backgroundColor:['rgba(0, 230, 0, 0.5)']
      }]
  },

});

    $('.table').DataTable({
    	searching: false,
    });
</script>
