<div id="inner_main">
	<section class="evulation_details">
		<div class="container">
			<div class="evulation_details_top">

				<div class="row">
					<div class="col-sm-12">
						<div class="evulation_details_cont">
							<div class="evulation_details_cont_top">
								<h3 class="text-center"><b>Supplier Info</b></h3><br>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<ul>
										<li>
											<b>Name :</b> <?php if ($supplier['nick_name']) {
												echo $supplier['nick_name'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Email :</b> <?php if ($supplier['user_email']) {
												echo $supplier['user_email'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Main Country :</b> <?php if ($supplier['main_country']) {
												echo $supplier['main_country'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Other Countries :</b> <?php if ($supplier['other_countries']) {
												echo $supplier['other_countries'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>City :</b> <?php if ($supplier['main_city']) {
												echo $supplier['main_city'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>State :</b> <?php if (isset($supplier['state'])) {
												echo $supplier['state'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Industry :</b> <?php if ($supplier['industryName']) {
												echo $supplier['industryName'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Size :</b> <?php if ($supplier['size']) {
												echo $supplier['size'];
											} else {
												"Not present";
											}?>
										</li> 
										<li>
											<b>Company Name :</b> <?php if ($supplier['company_name']) {
												echo $supplier['company_name'];
											} else {
												"Not present";
											}?>
										</li>
									</ul>
								</div>
								<div class="col-sm-6">
									<ul>
										<li>
											<b>Company Description :</b> <?php if ($supplier['company_description']) {
												echo $supplier['company_description'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Company Core Business :</b> <?php if ($supplier['company_core_business']) {
												echo $supplier['company_core_business'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Reference :</b> <?php if ($supplier['reference']) {
												echo $supplier['reference'];
											} else {
												echo "Not present";
											}?>
										</li>

										<li>
											<b>Main Activities :</b> <?php if ($supplier['main_activities']) {
												echo $supplier['main_activities'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Contact Phone :</b> <?php if ($supplier['contact_phone']) {
												echo $supplier['contact_phone'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Contact Name :</b> <?php if ($supplier['contact_name']) {
												echo $supplier['contact_name'];
											} else {
												echo "Not present";
											}?>
										</li>
										<li>
											<b>Supplier Skills :</b> <?php if ($skill_data) {
												echo $skill_data;
											} else {
												echo "Not present";
											}?>
										</li>
									</ul>

								</div>
							</div>



						</div>

					</div>
				</div>

				<div class="row" style="margin-bottom: 10px">
					<div class="col-md-4"></div>
					<div class="col-md-5">
						<div class="card">
							<div class="card-body">
								<canvas id="myChart" width="200" height="200"></canvas>   
							</div>
						</div>
					</div>
				</div>


				<div class="evulation_detail_mid">
					<h3 class="text-center">User Evaluation</h3>
					<div class="review_details">
						<div class="table-responsive-sm">
							<?php if (empty($user_evaluation)) {
								echo "<p class=' text-center'>Evaluation not present, add the first one</p>";
							} else {?>
								<table class="table ">
									<thead>
										<tr>
											<th><b>Nick Name</b></th>
											<th><b>Rating</b></th>
											<th><b>Brief Induction</b></th>
											<th><b>Skills</b></th>
											<th><b>Actions</b></th>
										</tr>
									</thead>

									<tbody>
										<?php foreach ($user_evaluation as $data) : ?>
											<tr>
												<td><?php echo $data['nick_name']; ?></td>
												<th scope="row">
													<div class="rateyo" data-rateyo-rating="<?php echo $data['avg_rating']; ?>" 
														data-rateyo-num-stars="5" data-rateyo-score="<?php echo $data['avg_rating']; ?>"></div>  
													</th>
													<td><?php echo substr($data['inductions'], 0, 30); ?></td>
													<td><?php echo substr($data['skill'], 0, 30); ?></td>
													<td><a href="user_evaluation/details/<?php echo $data['id']?>">Details</a></td>

												</tr>
											<?php endforeach; ?>  
										</tbody>

									</table>
								<?php }?>  
							</div>
						</div>
					</div>

				</div> 
			</section>
			<!-- === end search result=== -->
			<!-- ==== ss top supplier  ==== -->
			

			<!-- ==== ss end supplier  ==== -->
		</div>
		<div id="radarData" style="display:none"><?php echo $radarData; ?></div>

		<script type="text/javascript">
			$(document).ready(function(){
				$(".rateyo").rateYo({readOnly: true}).on("rateyo.change", function (e, data) {

				});
			});


    //radar diagram
    var radarData = $('#radarData').html();
    radarData = JSON.parse(radarData);
    var ratingValues = [];
    var ratingUnit = [];
    $.each(radarData,  function(key, item){
    	ratingValues.push(item);
    	ratingUnit.push(key);
    })  

    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
    	type: 'radar',
    	data: {
        //labels: ["Terrible", "Poor", "Average", "Very Good", "Excellent"],
        labels: ratingUnit,//["1", "2", "3", "4", "5","4", "5"],
        datasets: [{
        	label: 'Number Of Evaluation',
          data: ratingValues,//[12, 13, 9, 6, 7],
          borderWidth: 1,
          backgroundColor:['rgba(0, 230, 0, 0.5)']
      }]
  },

});

</script>
