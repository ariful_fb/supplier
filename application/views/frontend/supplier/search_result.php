<div id="inner_main">
  <section class="search_result">
    <div class="container">
      <div class="row justify-content-md-center">

        <?php if (!count($suppliers)) : ?>
          <p>No data found</p>
        <?php else : ?>
            <?php foreach ($suppliers as $supplier) : ?>
              <div class="col-sm-3" style="margin:5px 60px 5px 5px !important">
                <div class="card text-center top_s_item" style="width: 20rem;">
                  <?php $supplierLogo = isset($supplier['logo']) && file_exists(FCPATH.$supplier['logo'])?$supplier['logo']:'assets/frontend/img/c1.png' ?>

                  <div class="top_s_img"><a href="supplier/details/<?php echo  $supplier['user_id']; ?>"><img  src="<?php echo $supplierLogo; ?>" alt="..."></a>

                  </div>
                  <div class="card-body">
                    <h5 class="card-title"><a href="supplier/details/<?php echo  $supplier['user_id']; ?>"><?php  echo isset($supplier['company_name'])?$supplier['company_name']:'Conpany Name Not Found';?></a></h5>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item">
                        <div class="rateyoo" data-rateyo-rating="<?php echo $supplier['rating']; ?>" 
                          data-rateyo-num-stars="5" data-rateyo-score="<?php echo $supplier['rating']; ?>">
                          
                        </div>
                        <!-- <input  name="input-name" type="number" class="star-rating" data-size="md" data-rtl="false" readonly="true" value="4.5"> -->
                      </li>
                      <li class="list-group-item"><strong>Search Matched: </strong>
                        <?php

                        $res = (!$paramCount) ? 1 : (float)($supplier['occurance']/$paramCount);

                        ?>
                        <?php echo isset($supplier['occurance']) ? round($res*100, 2) : 0 ?>%</li>

                        <li class="list-group-item"><a href="supplier/details/<?php echo  $supplier['user_id']; ?>" class="btn btn-primary">Details</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
            <?php endforeach; ?> 
        <?php endif; ?>

          </div>
        </div>
        <div class="container">
        </div>
      </section>
      <!-- === end search result=== -->

    </div>
    <script type="text/javascript">

      $(document).ready(function(){


        $(function () {
          $(".rateyoo").rateYo({ readOnly: true}).on("rateyo.change", function (e, data) {

          });

        }); 
        


      });

    </script>
