<div id="inner_main">
  <div class="row" id="vueInstance">
    <div class="col-md-4"></div>
    <form id="contact_form" v-on:submit="formSubmit">
      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="name">Your Name</label>
          <input type="text" class="form-control" id="contact_name" placeholder="Name" name="name" required>
        </div>
        <div class="form-group col-md-6">
          <label for="email">Your Email</label>
          <input type="email" class="form-control" id="contact_email" placeholder="Email" name="email" required>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="inputCity">Phone</label>
          <input type="phone" class="form-control" id="phone">
        </div>
        <div class="form-group col-md-4">
          <label for="inputState">Contact Method</label>
          <select id="inputState" class="form-control" name="contact_method">
            <option value="email" selected>Email</option>
            <option value="phone" >Phone</option>
            <option value="text" >Text</option>
          </select>
        </div>

      </div>
      <div class="form-row">
        <div class="form-group col-md-12">
          <div class="form-group">
            <label for="message">Your Message</label>
            <textarea class="form-control" id="message" rows="3" cols="12" v-model="message" v-on:input="textareaInput" name="message" required></textarea>
          </div>
        </div>
      </div>
     <!--  <div class="form-row" style="background-color: #f2f2f2" v-if="showIf==1">
        <div class="form-group col-md-12">
          <div class="form-group">
            <p style="white-space: pre-line;">{{ message }}</p>
          </div>
        </div>
      </div> -->
      <div class="form-row">
        <div class="form-group col-md-12">
          <br>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
      
    </form>
  </form>                    
</div>
</div>
</div>


<script>

  new Vue({
    el: '#vueInstance',
    data:{
      message:'',
      showIf:0,
    },
    methods: {
      formSubmit: function(e){
        e.preventDefault();
        var data = $('#contact_form').serialize();
        $.ajax({
          url:'AuthController/contactUs',
          method:'POST',
          data:data,
          success:function(response){
            if(response=='1'){
              swal('You\'ll get contacted soon. Thanks!');
            } else {
              swal('Somthing is wrong, Please come back later. Thanks!');
            }
            $('#contact_form')[0].reset();
            this.showIf = 0;  
          }
        })
      },
      textareaInput: function(){
        
       /* if(this.val==""){
          console.log('empty');
          this.showIf = 0;
        } else {
          this.showIf = 1;
        }*/
      }
    }
  })
</script>