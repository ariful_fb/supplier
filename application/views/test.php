<!DOCTYPE html>
<html lang="en">
<head>
  <base href="<?php echo base_url(); ?>">
  <meta charset="UTF-8">
  <title>test</title>
  <!-- bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- faicon -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- krajee -->

<link href="assets/plugins/bootstrap-star-rating-master/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />
<script src="assets/plugins/bootstrap-star-rating-master/js/star-rating.js" type="text/javascript"></script>
<link href="assets/plugins/bootstrap-star-rating-master/themes/krajee-fa/theme.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="assets/plugins/bootstrap-star-rating-master/themes/krajee-fa/theme.min.js"></script>


 


</head>
<body>

  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      
      <input  name="input-name" type="number" class="star-rating" data-size="md" data-rtl="false" readonly="true" value="4.5">
    </div>
  </div>
  
</body>

<script>
  $("#input-id").rating({min:1, max:5, step:0.1, size:'sm', stars:5});
</script>
</html>
