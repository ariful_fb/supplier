<?php include('email_temp_header.php'); ?>
<div id="emailBody">
  <p>Welcome to the look4supplier</p>
  <p>we confirm you completed the registration to the look4supplier web site.</p>
  <p>See you soon on look4supplier to complete new evaluations!</p>
  <p>This is an automatic email, don’t answer to this email</p>
  <br>
  <p>If you want to delete your data you can write an email to info@look4supplier.com </p>
  <br>
  <p>Thanks</p>
  <p>Regards</p>
  <p>The “look4supplier” team</p>
</div>
<?php include('email_temp_footer.php');?>
