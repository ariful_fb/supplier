
<!DOCTYPE HTML>
<html>
<head>
    <base href="<?php echo base_url(); ?>">
    <title>404 | page not found</title>
    <meta name="keywords" content="404 page not found" />
    <link href="assets/css/404_style.css" rel="stylesheet" type="text/css"  media="all" />
    
</head>
<body>
    <!--start-wrap--->
    <div class="wrap">
        <!---start-header---->
        <div class="header">
            <div class="logo">
                <h1><a href="<?php echo base_url(); ?>">Ohh</a></h1>
            </div>
        </div>
        <!---End-header---->
        <!--start-content------>
        <div class="content">
            <img src="assets/images/error-img.png" title="error" />
            <p><span><label>O</label>hh.....</span>You Requested the page that is no longer There.</p>
            <a href="<?php echo base_url(); ?>">Back To Home</a>
            <div class="copy-right">
                <p>&copy; <?php echo date('Y'); ?> Look 4 Supplier. All Rights Reserved </p>
            </div>
        </div>
        <!--End-Cotent------>
    </div>
    <!--End-wrap--->
</body>
</html>

