<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?php echo get_phrase('customer_subscription_&_payment_report');?> </b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="card-box">


                                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><?php echo get_phrase('serial_no');?></th>
                                                    <th><?php echo get_phrase('date');?></th>
                                                    <th><?php echo get_phrase('purchased_package');?></th>
                                                    <th><?php echo get_phrase('paid_amount');?></th>
                                                    <th><?php echo get_phrase('payment_email');?></th>
                                                    <th><?php echo get_phrase('user');?></th>

                                                </tr>
                                            </thead>


                                            <tbody>
                                                <?php $sl=0; foreach($payments as $payment): $sl++; ?>

                                                <tr>
                                                    <td><?php echo $sl; ?></td>
                                                    <td style="vertical-align: middle;"><?php echo date($payment['created_at']); ?></td>
                                                    <td><?php echo getPackageNameByID($payment['product_id']); ?></td>
                                                    <td><?php echo $payment['payment_gross']; ?></td>
                                                    <td><?php echo $payment['payer_email']; ?></td>
                                                 
                                                    <td><?php echo getUserNameByID($payment['user_id']); ?></td>


                                                </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                    </div>









                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
    </div>
</div>
</div>


<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
    
    $(document).ready(function() {
        $('form').parsley();
    });
</script>



