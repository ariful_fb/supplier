<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Manage Users</h4>
                            <div class="p-20">
                                <?php $this->load->view('backend/session_msg'); ?>
                                <div class="row">

                                    <div class="col-sm-12">

                                        <div class="card-box">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="m-b-30">
                                                        <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#exampleModal">Add Users
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="col-sm-2">
                                                    <div class="m-b-30">

                                                    </div>
                                                </div>

                                            </div>

                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Nick Name</th>
                                                        <th>Email</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $sl=0; foreach($all_users as $data): $sl++; ?>

                                                    <tr id="<?php echo $data['user_id']; ?>">
                                                        <td><?php echo $sl; ?></td>
                                                        <td id="nick_name"><?php echo $data['nick_name']; ?></td>
                                                        <td id="user_email"><?php echo $data['user_email']; ?></td>
                                                        <td class="modalOpen">

                                                           <!--  <a  href="admin/reject_user/<?php echo $data['user_id'];?>" ><button class="btn btn-danger">Reject</button>
                                                            </a>
                                                            <a  href="admin/approve_supplier/<?php echo $data['user_id'];?>" ><button class="btn btn-success">Approved</button> -->
                                                            </a>
                                                            <a  href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal2"  class="btn btn-success" >Edit
                                                            </a>
                                                            <a  onclick="return confirm('Are you sure,You want to delete?')" href="admin/delete_user/<?php echo $data['user_id'];?>" class="btn btn-success" >Delete
                                                            </a>
                                                            <a class="btn btn-success" id="sendRegLinkIcon" userEmail="<?php echo $data['user_email']; ?>">
                                                                <i class="fa fa-paper-plane"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- end: page -->

                            </div> <!-- end Panel -->
                        </div>

                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>

<!-- Modal Start -->                
<div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title header-title mt-0">Add User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <form  role="form" action="admin/add_user" method="POST" id="add_user" >                                         
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Nick Name<span class="text-danger">*</span></label>
                                    <div class="col-7">                            
                                        <input type="text" required name="nick_name" class="form-control" value="sdf">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                                    <div class="col-7">                            
                                        <input type="text" required name="user_email" class="form-control" value="sdfs@gmail.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                                    <div class="col-7">                            
                                        <input type="password" disabled="disabled" required name="password" class="form-control" value="123456">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-8 offset-4">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 offset-4">
                                        <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                            Save
                                        </button>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>



                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Edit Modal Start -->                
<div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title header-title mt-0">Edit User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">


                            <form  role="form" action="<?=base_url('admin/update_user')?>" method="POST" id="edit_user">

                                <input type="hidden" id="e_id"  name="user_id" >
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Nick Name<span class="text-danger">*</span></label>
                                    <div class="col-7">                            
                                        <input type="text" required name="nick_name" class="form-control" id="e_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                                    <div class="col-7">                            
                                        <input type="text" required name="user_email" class="form-control" id="e_email">
                                    </div>
                                </div>
                               <!--  <div class="form-group row">
                                    <label for="password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                                    <div class="col-7">                            

                                        <input type="password"  name="password" class="form-control" placeholder="">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <div class="col-12 offset-4">
                                        <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                            Update
                                        </button>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>



                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(document).ready(function(){
        $("#add_user").validate({
            rules:{
                user_email:{
                    email:true,
                    remote:{
                        url:"AdminController/emailCheck",
                        type:"POST",
                        data: {
                            'request_type':'add',
                        }

                    }
                },
                nick_name:{
                    remote:{
                        url:"AdminController/nameCheck",
                        type:"POST",
                        data: {
                            'request_type':'add',
                        }

                    }
                }
            },
            messages: {
                user_email:{
                    remote:"This email already used!!"
                },
                nick_name:{
                    remote:"This nick name already used!!"
                }
            }           
        });

        $("#edit_user").validate({

            rules:{
                password:{

                    remote:{
                        url:"AdminController/passwordCheck",
                        type:"POST",
                        data: {

                        },

                    }
                }

            },
            messages: {
                password:{
                    remote:"Minmum length 6."
                }
            }           
        });

        /*$("#edit_user").validate({

            rules:{
                user_email:{
                    email:true,
                    remote:{
                        url:"AdminController/editEmailCheck",
                        type:"POST",
                        data: {
                            'user_id':$('#e_id').find('e_id').text(),
                        },

                    }
                },
                nick_name:{
                    remote:{
                        url:"AdminController/editNameCheck",
                        type:"POST",
                        data: {
                            'user_id':$("#e_id").val(),
                        }
                    }
                }
            },
            messages: {
                user_email:{
                    remote:"This email already used!!"
                },
                nick_name:{
                    remote:"This nick name already used!!"
                }
            }           
        });*/

    });
</script>

<script>
    $('.modalOpen').on('click', function(){
        var id= $(this).closest('tr').attr('id');
        var name= $(this).parent('tr').find('#nick_name').text();
        var email= $(this).parent('tr').find('#user_email').text();
        //alert(id);
        $("#e_name").val(name);
        $("#e_id").val(id);
        $("#e_email").val(email);
/*
        $.ajax({
            url:'AdminController/updateUser',
            method:'POST',
            data:{id:id},
            success:function(data){
                console.log(data);
            }
        })

        */
    });

    /*send registration link*/
    $('#sendRegLinkIcon').on('click', function(){
        var email = $(this).attr('userEmail');
        swal("An email will be sent to confirm registration, really want to proceed?", {
            buttons: {
                cancel: "NO",
                catch: {
                    text: "YES",
                },
            },
        })
        .then((value) => {
            switch (value) {

                case "cancel":
                swal("Pikachu fainted! You gained 500 XP!");
                break;

                case "catch":
                $.ajax({
                    url:"<?php echo base_url() ?>supplier/mail/confirmation/?email="+email,
                    method:'GET',
                    success:function(){
                        swal('Email Sent');
                    }
                })
                break;

                default:
                swal("Email not sent!");
            }
        });
    })

</script>

