<div class="content-page">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card-box">
            <h4 class="m-t-0 header-title"><b>DELETE GDPR</h4>
              <div class="p-20">
                <?php $this->load->view('backend/session_msg'); ?>
                <div class="row">

                  <div class="col-sm-12">

                    <div class="card-box">
                      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Nick Name</th>
                            <th>Email</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($requests as $data) : ?>
                            <tr id="<?php echo $data['user_id']; ?>">
                              <td><?php echo $data['user_id']; ?></td>
                              <td><?php echo $data['nick_name']; ?></td>
                              <td><?php echo $data['user_email']; ?></td>
                              <td><i class="fa fa-times del_gdpr" style="color:red" data-toggle="tooltip" data-placement="top" title="Delete GDPR"></i></td>
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- end: page -->

                </div> <!-- end Panel -->
              </div>

            </div> <!-- end card-box -->
          </div> <!-- end col -->
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Start -->                
  <div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title header-title mt-0">Add User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box">

                <form  role="form" action="admin/add_user" method="POST" id="add_user" >                                         
                  <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Nick Name<span class="text-danger">*</span></label>
                    <div class="col-7">                            
                      <input type="text" required name="nick_name" class="form-control" value="sdf">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                    <div class="col-7">                            
                      <input type="text" required name="user_email" class="form-control" value="sdfs@gmail.com">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                    <div class="col-7">                            
                      <input type="password" disabled="disabled" required name="password" class="form-control" value="123456">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-8 offset-4">

                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-12 offset-4">
                      <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                        Save
                      </button>

                    </div>
                  </div>
                </form>

              </div>
            </div>



          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- Edit Modal Start -->                
  <div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title header-title mt-0">Edit User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box">


                <form  role="form" action="<?=base_url('admin/update_user')?>" method="POST" id="edit_user">

                  <input type="hidden" id="e_id"  name="user_id" >
                  <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Nick Name<span class="text-danger">*</span></label>
                    <div class="col-7">                            
                      <input type="text" required name="nick_name" class="form-control" id="e_name">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="name" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                    <div class="col-7">                            
                      <input type="text" required name="user_email" class="form-control" id="e_email">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-12 offset-4">
                      <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                        Update
                      </button>

                    </div>
                  </div>
                </form>

              </div>
            </div>



          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <script type="text/javascript">

    $(document).on('click', '.del_gdpr', function () {
      var uId = $(this).closest('tr').attr('id');
      var item = $(this).closest('tr');
      swal("User id along with other related infos will permanently destroy, you really want to delete?", {
        buttons: {
          yes: "Yes",
          no : 'No',
        },
      })
      .then((value) => {
        switch (value) {

          case "yes":
          $.ajax({
            url:'AdminController/deleteGDPR/'+uId,
            success: function(){
              swal("", "Account deleted successfully", "success");
              item.fadeOut(5000);
            }
          })
          break;

          case "no":
          swal("", "Account not deleted", "error");
          break;

          default:
          swal("Nothing just happened!");
        }
      });
    })
  </script>
