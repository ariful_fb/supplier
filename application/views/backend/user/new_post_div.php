<?php   
$edit_data      =   $this->db->get_where('post_challenge' , array('id' => $param2) )->result_array();
foreach ( $edit_data as $challenge):
  ?>

  <div class="form-group row" id="id">
    <label class="col-4 col-form-label"><?php echo get_phrase('name');?>:</label>
    <div class="col-7">
      <label class="col-7 col-form-label"><?php echo User_helper::getChallengeNameByID($challenge['challenge_id']) ;?></label>
    </div>
  </div>


  <div class="form-group row">
    <label class="col-4 col-form-label"><?php echo get_phrase('description');?>:</label>
    <div class="col-7">
      <label class="col-7 col-form-label"><?php echo $challenge['description'] ?></label>
    </div>
  </div>

  <div class="form-group row">
    <label class="col-4 col-form-label"><?php echo get_phrase('type');?>:</label>
    <div class="col-7">
      <label class="col-7 col-form-label"><?php echo $challenge['footage_type'] ?></label>
    </div>
  </div>



  <?php if($challenge['footage_type']=='photo'){ ?>
    <div class="form-group row">
      <label class="col-4 col-form-label"><?php echo get_phrase('footage');?>:</label>
      <div class="col-7">
        <img src="<?php echo $challenge['footage'] ?>" style="height:280px; width:360px;">
      </div>
    </div>
  <?php } ?>

  <?php if($challenge['footage_type']=='video'){ ?>
    <div class="form-group row">
      <label class="col-4 col-form-label"><?php echo get_phrase('footage');?>:</label>
      <div class="col-7">
        <video controls="controls" preload="metadata" width="360" height="280"><source src="<?php echo $challenge['footage'] ?>" type="video/mp4"></video>
        </div>
      </div>
    <?php } ?>

    <div class="form-group row">
      <div class="col-12 offset-4">
        <div id="hideDiv">

          <div class="row">

            <div class="col-md-4">
              <a href="approve_challenge_post/<?php echo $challenge['id']; ?>"><i style="margin-right:10px; font-size: 16px;" class="fa fa-check btn btn-default btn-rounded waves-effect waves-light" ><?php echo get_phrase('approve');?>
            </i></a>
          </div>
          <div class="col-md-4">
            <a href="reject_challenge_post/<?php echo $challenge['id']; ?>"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Reject" class="fa fa-times btn btn-danger btn-rounded waves-effect waves-light"><?php echo get_phrase('reject');?></i></a>
          </div>
        </div>
      </div>

    </div>

  </div>
<?php endforeach; ?>
<!-- view post -->

<script type="text/javascript">
  var filePath=<?php echo $challenge['footage'];?>;
  function downloadFile(filePath) {
    var link = document.createElement('a');
    link.href = filePath;
    link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
    link.click();
  }
</script>



<script>
  var status=<?php echo $challenge['status'];?>;
  function load(status) {

    if(status!=1){
      document.getElementById("hideDiv").style.display = "block";
    }
    else{
      document.getElementById("hideDiv").style.display = "none";
    }

  }
  window.onload=load(status);

</script>

