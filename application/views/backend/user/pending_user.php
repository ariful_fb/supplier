
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?php echo get_phrase('pending_users');?></b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="card-box">


                                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><?php echo get_phrase('serial_no');?></th>
                                                    <th><?php echo get_phrase('name');?></th>
                                                    <th><?php echo get_phrase('father_name');?></th>
                                                    <th><?php echo get_phrase('email');?></th>
                                                    <th><?php echo get_phrase('phone');?></th>
                                                    <th><?php echo get_phrase('nid');?></th>
                                                    <th><?php echo get_phrase('status');?></th>
                                                    <th><?php echo get_phrase('action');?></th>

                                                </tr>
                                            </thead>


                                            <tbody>
                                               <?php $sl=0; foreach ($pending_user as $user) { $sl++;?>
                                                   <tr id="<?php echo $user['id']; ?>">
                                                    <td><?php echo $sl; ?></td>

                                                    <input id="id" type="hidden" value="<?php echo $user['id'] ?>">
                                                    <td id="name"><?php echo $user['first_name']. '&nbsp'.$user['last_name']; ?></td>
                                                    <td id="father_name"><?php echo $user['father_name']; ?></td>

                                                    <td id="email"><?php echo $user['email']; ?></td>
                                                    <td id="phone"><?php echo $user['phone']; ?></td>
                                                    <td id="nid"><?php echo $user['nid']; ?></td>
                                                    <td style="padding:none"><?php echo User_helper::getUserStatus($user['status']); ?></td> 



                                                    <td class="modalOpen"> 
                                                        <?php $page_title= "User Profile";?>
                                                        <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/user_profile/<?php echo $user['id'];?>/<?php $page_title;?>');" 
                                                            >
                                                            <i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="View" class="fa fa-eye"></i>

                                                        </a>


                                                        <a href="approve_user/<?php echo $user['id']; ?>"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="<?php echo get_phrase('approve');?>" class="fa fa-check"></i></a>

                                                        <a href="reject_user/<?php echo $user['id']; ?>"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="<?php echo get_phrase('rejact');?>" class="fa fa-times"></i></a>


                                                    </td>
                                                </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                    </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
    </div>
</div>
</div>


<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
    $(document).ready(function() {
        $('form').parsley();
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: true,
            lengthMenu: [25,50,100,150,200],          
        });       
    } );

</script>

<script>
    $('.modalOpen').on('click', function(){

    });
</script>





