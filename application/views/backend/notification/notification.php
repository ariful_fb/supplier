<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Notification</h4>
                            <div class="p-20">
                                <?php $this->load->view('backend/session_msg'); ?>
                                <div class="row">

                                    <div class="col-sm-12">

                                        <div class="card-box">

                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Reason</th>
                                                        <th>Supplier Company</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $sl=0; foreach($notification as $data): $sl++; ?>
                                                    <tr id="<?php echo $data['id'];?>">
                                                        <td><?php echo $sl; ?></td>
                                                        <td ><?php echo substr($data['reason'],0,30);if(strlen($data['reason'])>30)echo '...'; ?></td>
                                                        <td ><?php echo $data['company_name']; ?></td>
                                                        <td class="modalOpen">  
                                                            <a  onclick="return confirm('Are you sure,You want to delete notification.?')" href="delete/notification/<?php echo $data['id'];?>"  ><i class="fa fa-times ml-1 text-danger" title="Delete Notification"></i>
                                                            </a>

                                                            <a  onclick="return confirm('Are you sure,You want to delete evaluation id=<?php echo $data['evaluation_id'];?>.?')" href="delete/evaluation/<?php echo $data['evaluation_id'];?>"  ><i class="fa fa-trash ml-1 text-danger" title="Delete Evaluation"></i>
                                                            </a>

                                                            <a href="" data-toggle="modal" data-target=".bd-example-modal-lg"  ><i class="fa fa-eye showUserInfoIcon ml-1" title="View Reason"></i>
                                                            </a> 
                                                            <a href="admin/view_evaluation/<?php echo $data['evaluation_id'];?>"  ><i class="fa fa-eye-slash ml-1 text-danger" title="View Evaluation"></i>
                                                            </a>

                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- end: page -->

                            </div> <!-- end Panel -->
                        </div>

                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="list-group" id="supDetModBody">
                <li class="list-group-item list-group-item-action active">Supplier Info</li>
                <li class="list-group-item list-group-item-light userEmail"></li>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click','.showUserInfoIcon', function(){
        var notificationId = $(this).closest('tr').attr('id'); 
    //console.log('userId:'+userId);
    $.ajax({
      url:'AdminController/notificationDetails/'+notificationId,
      method:'POST',
      data:{},
      success: function(data){
        data = JSON.parse(data);
        var html = `
        <li class="list-group-item list-group-item-action active">Supplier Info</li>
        <li class="list-group-item list-group-item-light"> Email: `+data.reason+`</li>
        `;
        $('#supDetModBody').html(html);
    }
})
})
</script>