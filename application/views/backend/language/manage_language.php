<!-- ============================================================== -->
<div class="content-page">
  <base href="<?php echo base_url();?>">
  <link href="assets/css/neon-theme.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/language_style.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
  <!-- Start content -->
  <div class="content">




    <div class="container-fluid">


      <style>

      
    </style>

    <div class="row">
      <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
          <?php if(isset($edit_profile)):?>
           <li class="active">
             <a href="#edit" data-toggle="tab"><i class="icon-wrench"></i> 
               <?php echo get_phrase('edit_phrase');?>
             </a></li>
           <?php endif;?>
           <li class="<?php if(!isset($edit_profile))echo 'active';?>">
             <a href="LanguageController/manage_language" ><i class="entypo-menu"></i> 
               <?php echo get_phrase('language_list');?>
             </a></li>
             <li>

               <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/add_phrase/');" 
                 >
                 <i class="entypo-plus-circled"></i>
                 <?php echo get_phrase('add_phrase');?>
               </a> 

             </li>
             <li class="">
               <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/add_language/');" 
                 >
                 <i class="entypo-plus-circled"></i>
                 <?php echo get_phrase('add_language');?>
               </a> 

             </li>
           </ul>
           <!------CONTROL TABS END------>


           <div class="tab-content">
             <!----PHRASE EDITING TAB STARTS-->
             <?php if (isset($edit_profile)):?>
               <div class="tab-pane active" id="edit" style="padding: 5px">
                 <div class="">
                   <?php 
                   $current_editing_language  =   $edit_profile;
                   echo form_open(base_url() . 'LanguageController/manage_language/update_phrase/'.$current_editing_language  , array('id' => 'phrase_form'));

                   ?> 


                   <div class="row">

                     <?php

                     $count = 1;
                     $language_phrases  =   $this->db->query("SELECT `phrase_id` , `phrase` , `$current_editing_language` FROM `language`")->result_array();
                     foreach($language_phrases as $row)
                     {
                       $count++;
                                                     $phrase_id           =   $row['phrase_id'];                  //id number of phrase
                                                     $phrase              =   $row['phrase'];                     //basic phrase text
                                                     $phrase_language =   $row[$current_editing_language];    //phrase of current editing language
                                                     ?>
                                                     <!----phrase box starts-->
                                                     <div class="col-sm-3">
                                                       <div class="tile-stats tile-gray">
                                                         <div class="icon"><i class="entypo-mail"></i></div>


                                                         <h3><?php echo $row['phrase'];?></h3>
                                                         <p>
                                                           <input type="text" name="phrase<?php echo $row['phrase_id'];?>"  
                                                           value="<?php echo $phrase_language;?>" class="form-control"/>
                                                         </p>
                                                       </div>

                                                     </div>
                                                     <!----phrase box ends-->
                                                     <?php 
                                                   }
                                                   ?>
                                                 </div>
                                                 <input type="hidden" name="total_phrase" value="<?php echo $count;?>" />
                                                 <input type="submit" value="<?php echo get_phrase('update_phrase');?>" onClick="document.getElementById('phrase_form').submit();" class="btn btn-blue"/> 
                                                 <?php
                                                 echo form_close();
                                                 ?>

                                               </div>                
                                             </div>
                                           <?php endif;?>
                                           <!----PHRASE EDITING TAB ENDS-->
                                           <!----TABLE LISTING STARTS-->
                                           <div class="tab-pane <?php if(!isset($edit_profile))echo 'active';?>" id="list">


                                             <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
                                               <thead>
                                                 <tr>
                                                   <th><?php echo get_phrase('language');?></th>
                                                   <th><?php echo get_phrase('option');?></th>
                                                 </tr>
                                               </thead>
                                               <tbody>
                                                 <?php
                                                 $fields = $this->db->list_fields('language');
                                                 foreach($fields as $field)
                                                 {
                                                  if($field == 'phrase_id' || $field == 'phrase')continue;
                                                  ?>
                                                  <tr>
                                                    <td><?php echo ucwords($field);?></td>
                                                    <td>
                                                      <a href="<?php echo base_url();?>LanguageController/manage_language/edit_phrase/<?php echo $field;?>"
                                                        class="btn btn-info">
                                                        <?php echo get_phrase('edit_phrase');?>
                                                      </a>
                                                      <a href="<?php echo base_url();?>LanguageController/manage_language/delete_language/<?php echo $field;?>"
                                                        rel="tooltip" data-placement="top" data-original-title="<?php echo get_phrase('delete_language');?>" class="btn btn-gray" onclick="return confirm('Delete Language ?');">
                                                        <i class="fa fa-trash btn_delete_skill" data-toggle="tooltip" title="Delete" style="margin-right:10px; font-size: 20px;">  </i>
                                                      </a>
                                                    </td>
                                                  </tr>
                                                  <?php
                                                }
                                                ?>
                                              </tbody>
                                            </table>
                                          </div>
                                          <!----TABLE LISTING ENDS--->



                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

