<div class="content-page">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Manage Evaluation</h4>
              <div class="p-20">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="card-box">
                      <div class="row">

                        <div class="col-sm-2">
                          <div class="m-b-30">

                          </div>
                        </div>

                      </div>

                      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Serial</th>
                            <th>Reviewer</th>
                            <th>Supplier Company</th>
                            <th>Supplier Name</th>
                            <th>Rating Given</th>
                            <th>Action</th>

                          </tr>
                        </thead>
                        <tbody>
                          <?php $sl=1; ?>
                            <?php foreach ($evaluation as $eval) : ?>
                            <tr id="<?php echo $eval['id'];?>">
                              <td><?php echo $sl++; ?></td>
                              <td><?php echo $eval['creator']; ?></td>
                              <td><?php echo $eval['company_name']; ?></td>
                              <td><?php echo $eval['given_to']; ?></td>
                              <td><?php echo $eval['avg_rating']; ?></td>
                              <td>
                                <a class="delEvalIcon">
                                  <i class="fa fa-times" style="color:red"></i>
                                </a>
                                <a href="admin/view_evaluation/<?php echo $eval['id'];?>">
                                  <i class="fa fa-eye" style="color:green"></i>
                                </a>
                              </td>
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- end: page -->

                </div> <!-- end Panel -->
              </div>

            </div> <!-- end card-box -->
          </div> <!-- end col -->
        </div>
      </div>
    </div>
  </div>

  <!-- supplier details -->
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="list-group" id="supDetModBody">
          <li class="list-group-item list-group-item-action active">Supplier Info</li>
          <li class="list-group-item list-group-item-light userEmail"></li>
          <li class="list-group-item list-group-item-light userNick"></li>
          <li class="list-group-item list-group-item-light userCountry"></li>
          <li class="list-group-item list-group-item-light userCity"></li>
          <li class="list-group-item list-group-item-light userIndustry"></li>
          <li class="list-group-item list-group-item-light userComDesc"></li>
          <li class="list-group-item list-group-item-light comCoreDesc"></li>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).on('click','.showUserInfoIcon', function(){
      var userId = $(this).closest('tr').attr('id'); 
      console.log('userId:'+userId);
      $.ajax({
        url:'AdminController/evaluationDetails/'+userId,
        method:'POST',
        data:{},
        success: function(data){
          console.log(data);
          data = JSON.parse(data);
          var html = `
          <li class="list-group-item list-group-item-action active">Supplier Info</li>
          <li class="list-group-item list-group-item-light">User email: `+data.user_email+`</li>
          <li class="list-group-item list-group-item-light">User nick name : `+data.user_email+`</li>
          <li class="list-group-item list-group-item-light">User Main country: `+data.main_country+`</li>
          <li class="list-group-item list-group-item-light">User industry: `+data.industry+`</li>
          <li class="list-group-item list-group-item-light">Company description: `+data.company_description+`</li>
          <li class="list-group-item list-group-item-light">company_core_business: `+data.company_core_business+`</li>
          <li class="list-group-item list-group-item-light">Contact phone: `+data.contact_phone+`</li> 
          `;
          console.log(data);
          $('#supDetModBody').html(html);
        }
      })
    })

  //reject or delete evaluation
  $('.delEvalIcon').on('click', function(e){
   e.preventDefault();
   var evalId = $(this).closest('tr').attr('id');
   var item = $(this).closest('tr');

   swal("Evaluation data will permanently destroy, you really want to delete?", {
    buttons: {
      yes: "Yes",
      no : 'No',
    },
  })
   .then((value) => {
    switch (value) {

      case "yes":

      $.ajax({
        url: 'AdminController/rejectEvaluation/'+evalId,
        success: function(data){
          console.log(data);
          swal("", "Evaluation deleted successfully", "success");
          item.fadeOut(5000);
        }
      })
      break;

      case "no":
      swal("", "Evaluation not deleted", "error");
      break;

      default:
      swal("Nothing just happened!");
    }
  });



 })

</script>

