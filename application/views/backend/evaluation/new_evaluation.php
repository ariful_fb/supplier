<div class="content-page">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Manage Evaluation</h4>
              <div class="p-20">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="card-box">
                      <div class="row">

                        <div class="col-sm-2">
                          <div class="m-b-30">

                          </div>
                        </div>

                      </div>

                      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Serial</th>
                            <th>Reviewer</th>
                            <th>Supplier Company</th>
                            <th>Supplier Name</th>
                            <th>Rating Given</th>
                            <th>Created Date</th>
                            <th>Action</th>

                          </tr>
                        </thead>
                        <tbody>

                          <?php $sl=1; ?>
                          <?php foreach ($evaluation as $eval) : ?>
                            <tr id="<?php echo $eval['id'];?>">
                              <td <?php if (!$eval['sup_approved']) {
                                echo 'class="bg-danger"';
                              }?>><?php echo $sl++; ?></td>
                              <td><?php echo $eval['creator']; ?></td>
                              <td><?php echo $eval['company_name']; ?></td>
                              <td><?php echo $eval['given_to']; ?></td>
                              <td><?php echo $eval['avg_rating']; ?></td>
                              <td><?php echo date("d M Y", strtotime($eval['created_at'])); ?></td>
                              <td>
                                <?php if ($eval['is_approved']) : ?>
                                  <a style="color:red;"  href="admin/reject_evaluation/<?php echo $eval['id'];?>" ><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Reject Evaluation"></i>
                                  </a>
                                  <?php else : ?>
                                    <a style="color:green;"   href="admin/approve_evaluation/<?php echo $eval['id'];?>" ><i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="Approve Evaluation"></i>
                                    </a>
                                  <?php endif; ?>
                                  <a class="delEvalIcon">
                                    <i class="fa fa-times" style="color:red"></i>
                                  </a>
                                  <a style="color:blue;"  href="admin/view_evaluation/<?php echo $eval['id'];?>" ><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="Evaluation Details"></i>
                                  </a>
                                </td>
                              </tr>
                            <?php endforeach; ?>

                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!-- end: page -->

                  </div> <!-- end Panel -->
                </div>

              </div> <!-- end card-box -->
            </div> <!-- end col -->
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $('.delEvalIcon').on('click', function(e){
       e.preventDefault();
       var evalId = $(this).closest('tr').attr('id');
       var item = $(this).closest('tr');

       swal("Evaluation data will permanently destroy, you really want to delete?", {
        buttons: {
          yes: "Yes",
          no : 'No',
        },
      })
       .then((value) => {
        switch (value) {

          case "yes":

          $.ajax({
            url: 'AdminController/deleteEvaluation/'+evalId,
            success: function(data){
              console.log(data);
              swal("", "Evaluation deleted successfully", "success");
              item.fadeOut(5000);
            }
          })
          break;

          case "no":
          swal("", "Evaluation not deleted", "error");
          break;

          default:
          swal("Nothing just happened!");
        }
      });



     })
   </script>