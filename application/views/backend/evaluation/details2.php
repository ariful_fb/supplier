

<script>
	function goBack() {
		window.history.back();
	}
</script>
<div id="inner_main">
	<section class="evulation_details">
		<div class="container">
			<div class="evulation_details_top">
				<button  onclick="goBack()" class="btn btn-success">Back</button>
				<div class="row">
					<div class="col-sm-12">
						<div class="evulation_details_cont">
							<div class="evulation_details_cont_top">
								<h3 class="text-center"><b>Activity Description Parameters</b></h3><br>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<ul>
										<li><b>Inductions :</b> <?php if($evaluation['inductions'])echo $evaluation['inductions'];else
										echo "Not present";?></li>
										<li><b>Activity Type:</b> <?php if($evaluation['activity_type']==1)echo "Business";else echo "lPrivate";?>
									</li>

									<li>
										<b>Activity Duration :</b> <?php 
										if($evaluation['activity_duration']==1)
											echo "More than 1 year service";
										else if($evaluation['activity_duration']==2)
											echo "Less than 1 year service";
										else if($evaluation['activity_duration']==3)
											echo "project";
										else if($evaluation['activity_duration']==4)
											echo "On demand activity";
										else if($evaluation['activity_duration']==5)
											echo "Other Value";
										else 
											echo "Not present";
										?>
									</li>



								</ul>
							</div>
							<div class="col-sm-6">
								<ul>
									<li>
										<b>Budget :</b> <?php 
										if($evaluation['budget']==1)
											echo "0-50.000€";
										else if($evaluation['budget']==2)
											echo "50.000€-100.000€";
										else if($evaluation['budget']==3)
											echo "100.000€-500.000€";
										else if($evaluation['budget']==4)
											echo "500.000€-1.000.000€";
										else if($evaluation['budget']==5)
											echo "More then 1.000.000€";
										else 
											echo "Not present";
										?>
									</li>
									<li><b>Customer Company :</b> <?php if($evaluation['customer_company'])echo $evaluation['customer_company'];else
									echo "Not present";?></li>

									<li>
										<b>User Company:</b> <?php 
										if($evaluation['user_company']==1)
											echo "Less than 100 employees";
										else if($evaluation['user_company']==2)
											echo "Between 100-500 employees";
										else if($evaluation['user_company']==3)
											echo "Greater than 500 employees";
										else if($evaluation['user_company']==4)
											echo "private";
										else 
											echo "Not present";
										?></li>

									</ul>

								</div>
							</div>



						</div>

					</div>
				</div>
			</div>
			<div class="evulation_detail_mid">
				<h3 class="text-center">Evaluation parameters </h3>
				<div class="review_details">
					<div class="table-responsive-sm">
						<table class="table ">

							<tbody>
								<tr>
									<th scope="row">
										<div id="reliability"></div>  
									</th>
									<td>Reliability</td>


								</tr>
								<tr>
									<th scope="row">
										<div id="flexibility"></div>     
									</th>
									<td>Flexibility</td>


								</tr> 
								<tr>
									<th scope="row">
										<div id="cost"></div>    
									</th>
									<td>Cost/quality</td>
								</tr> 

								<tr>
									<th scope="row">
										<div id="global_quality"></div>      
									</th>
									<td>Global quality of the work</td>


								</tr> 
								<tr>
									<th scope="row">
										<div id="people_skills"></div>    
									</th>
									<td>Supplier’s people skills </td>


								</tr>  
								<tr>
									<th scope="row">
										<div id="supplier_people"></div>  
									</th>
									<td>Opened to talk with all the supplier’s people</td>


								</tr>
								<tr>
									<th scope="row">
										<div id="adapt"></div> 	   
									</th>
									<td>Open to changes/to adapt to the changes without burocratize</td>


								</tr> 
								<tr>
									<th scope="row">
										<div id="engagement"></div>    
									</th>
									<td>Supplier’s people engagement</td>


								</tr> 
								<tr>
									<th scope="row">
										<div id="knowledge"></div> 	   
									</th>
									<td>Supplier’s people business knowledge</td>


								</tr> 
								<tr>
									<th scope="row">
										<div id="propositivity"></div>    
									</th>
									<td>Propositivity</td>


								</tr> 
								<tr>
									<th scope="row">
										<div id="proactivity"></div>    
									</th>
									<td>Proactivity</td>


								</tr> 
							</tbody>
						</table>
					</div>
				</div>
			</div>


			<div class="evulation_detail_bottom">
				<h3>Supplier Answer </h3>
				<div class="evulation_review_details">
					<?php if($evaluation['answer_creator_id']):?>
						<div class="evulation_review_profiless">
							<div class="evulation_review_profiless_img">
								<img src="assets/frontend/img/ps1.png" alt=""/>
							</div>
							<h3>  D. C Smith</h3>

						</div>
						<p class="text-justify"><?php if($evaluation['answer_creator_id'])echo $evaluation['answer'];?></p>
					<?php endif;?>


					<form method="post" action="submit_answer" >
						<input type="hidden" name="answer_creator_id" value="<?php if(isset($this->session->user_id))echo $this->session->user_id;?>">
						<input type="hidden" name="evaluationId" value="<?php if(isset($evaluation['evaluationId']))echo $evaluation['evaluationId'];?>">
						<div class="form-group"> 
							<textarea name="answer" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
						</div>
						<?php if(isset($this->session->role) && $this->session->role==3 ){?>
							<button type="submit" class="btn btn-success float-right"  id="checkAnswer">Submit Answer</button>
						<?php }else{?>
							<button type="submit" disabled class="btn btn-success float-right"  >Need to Log In As Supplier</button>
						<?php }?>
					</form>

				</div>
			</div>

			<div class="evulation_detail_bottom">
				<h3>Request to Delete</h3>
				<div class="evulation_review_details">

					<?php if(isset($this->session->role) && $this->session->role==3){?>
						<form method="post" action="submit_requests" >
							<input type="hidden" name="creator_id" value="<?php if(isset($this->session->user_id))echo $this->session->user_id;?>">
							<input type="hidden" name="evaluation_id" value="<?php if(isset($evaluation['evaluationId']))echo $evaluation['evaluationId'];?>">
							<div class="form-group"> 
								<textarea name="reason" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Talk about why you like this review, or ask a question."></textarea>
							</div>
							<button type="submit" class="btn btn-success float-right"  >Submit Request</button>
						</form>
					<?php }?>
				</div>
			</div>





		</div> 
	</section>
	<!-- === end search result=== -->
	<!-- ==== ss top supplier  ==== -->


	<!-- ==== ss end supplier  ==== -->
</div>

<!-- modal new_evaluation -->


<script type="text/javascript">
	$(document).ready(function(){

		$(function () {

			$("#reliability").rateYo({
				rating: <?php echo $evaluation['reliability'];?>,
				spacing: "20px",
				readOnly: true
			});
			$("#flexibility").rateYo({
				rating: <?php echo $evaluation['flexibility'];?>,
				spacing: "20px",
				readOnly: true
			}); 
			$("#cost").rateYo({
				rating: <?php echo $evaluation['cost'];?>,
				spacing: "20px",
				readOnly: true
			});
			$("#global_quality").rateYo({
				rating: <?php echo $evaluation['global_quality'];?>,
				spacing: "20px",
				readOnly: true
			}); 
			$("#people_skills").rateYo({
				rating: <?php echo $evaluation['people_skills'];?>,
				spacing: "20px",
				readOnly: true
			});
			$("#supplier_people").rateYo({
				rating: <?php echo $evaluation['supplier_people'];?>,
				spacing: "20px",
				readOnly: true
			}); 
			$("#adapt").rateYo({
				rating: <?php echo $evaluation['adapt'];?>,
				spacing: "20px",
				readOnly: true
			});
			$("#engagement").rateYo({
				rating: <?php echo $evaluation['engagement'];?>,
				spacing: "20px",
				readOnly: true
			}); 
			$("#knowledge").rateYo({
				rating: <?php echo $evaluation['knowledge'];?>,
				spacing: "20px",
				readOnly: true
			});
			$("#propositivity").rateYo({
				rating: <?php echo $evaluation['propositivity'];?>,
				spacing: "20px",
				readOnly: true
			});
			$("#proactivity").rateYo({
				rating: <?php echo $evaluation['proactivity'];?>,
				spacing: "20px",
				readOnly: true
			}); 
			$("#avg_rating").rateYo({
				rating: <?php echo $evaluation['avg_rating'];?>,
				spacing: "20px",
				readOnly: true
			});

		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#checkAnswer").click(function(){
			var checkAnswer=<?php echo $evaluation['answer_creator_id'];?>;
			if(checkAnswer){
				swal({
					text: "Answer already submited",
					icon: "success",
				});
			}else
			return true;
			return false;

		});

		
	});

</script>