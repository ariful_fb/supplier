<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b>Manage Evaluation</h4>
                            <div class="p-20">

                                <div class="row">

                                    <div class="col-sm-12">

                                        <div class="card-box">
                                            <div class="row">

                                                <div class="col-sm-2">
                                                    <div class="m-b-30">

                                                    </div>
                                                </div>

                                            </div>

                                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Rating</th>
                                                        <th>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $sl=0;
                                                    foreach ($evaluation as $data) :
                                                        $sl++; ?>

                                                    <tr >
                                                        <td><?php echo $sl; ?></td>
                                                        <td><?php echo $data['rating']; ?></td>
                                                        <td>
                                                            <?php if ($data['approved']) {?>
                                                                <a type="button" class="btn btn-danger" href="admin/reject_evaluation/<?php echo $data['id'];?>" >Reject
                                                                </a>
                                                            <?php } else {?> 
                                                                <a type="button" class="btn btn-success" href="admin/approve_evaluation/<?php echo $data['id'];?>" >Approved
                                                                </a>
                                                            <?php }?>

                                                            

                                                        </td>

                                                    </tr>
                                                    <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- end: page -->

                            </div> <!-- end Panel -->
                        </div>

                    </div> <!-- end card-box -->
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>

