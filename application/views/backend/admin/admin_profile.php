<div class="content-page">
  <div class="content">
    <div class="row">
      <base href="<?php echo base_url();?>">
      <div class="col-md-2"></div>
      <div class="col-md-8 ">
        <div class="panel panel-primary" >
          <?php $this->load->view('session_msg'); ?>
          <div class="panel-body">

            <h2> <?php echo get_phrase('admin_profile');?></h2>
            <?php foreach($admin_profile as $admin): ?>
              <form  role="form" action="<?=base_url('admmin_update')?>" method="POST"  enctype="multipart/form-data" >
                <div class="form-group">
                  <label  class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" 
                    value=" <?php echo $admin['name']?>">  
                  </div>
                </div>
                <input type="hidden" class="form-control" name="old_photo" 
                value=" <?php echo $admin['profile_photo']?>">




                <div class="form-group">
                  <label  class="col-sm-3 control-label"><?php echo get_phrase('email');?></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" value="<?php echo $admin['email']?>">
                  </div>
                </div>

                <div class="form-group">
                  <label  class="col-sm-3 control-label"><?php echo get_phrase('password');?></label>
                  <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" value="">
                  </div>
                </div>

                <div class="form-group">
                  <label  class="col-sm-3 control-label"><?php echo get_phrase('address');?></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="address" value="<?php echo $admin['address']?>">
                  </div>
                </div>



                <div class="form-group">
                  <label  class="col-sm-3 control-label"><?php echo get_phrase('country');?></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="country" 
                    value="<?php echo $admin['country']?>">
                  </div>
                </div>

                <div class="form-group">

                  <div class="panel panel-primary" style="margin-left:50px">
                    <div class="box">
                      <div class="box-header with-border">
                        <h3 class="box-title"><?php echo get_phrase('profile_photo');?> </h3>
                      </div>
                      <div class="box-body box-profile">


                        <div class="thumbnail upload_image" >                     
                          <?php 
                          $id = $this->session->userdata('id'); 
                          $profile_photo = $this->db->get_where('admin' , array('id' => $id))->row()->profile_photo; 

                          if(!empty($profile_photo)):
                            ?>
                            <img  style=" max-height: 200px; max-width:300px" src="<?php echo base_url().$profile_photo ;?>"alt="user"  >

                            <?php else: ?>
                              <img  style=" max-height: 200px; max-width:300px" src="<?php echo base_url() ;?>assets/images/default_avatar.png"alt="user"  >

                            <?php endif;?>


                          </div>


                          <div class="btn btn-default btn-file" style="margin-top:10px" >
                            <i class="fa fa-picture-o"></i>  
                            <input type="file" name="photo"  onchange="instantShowUploadImage(this, '.upload_image')">


                          </div>



                        </div>
                      </div>


                    </div>


                  <?php endforeach; ?>
                </div>



                <div class="form-group row">
                  <div class="col-12 offset-4">
                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                      <?php echo get_phrase('save');?>
                    </button>

                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  

  <script>

    function instantShowUploadImage(input, target) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $(target + ' img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
      }
      $(target).show();
    }
  </script>

