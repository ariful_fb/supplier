
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?php echo get_phrase('notice');?></b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="m-b-30">
                                                 <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#exampleModal"><b><?php echo get_phrase('add');?></a>
                                                 </button>
                                             </div>
                                         </div>
                                     </div>

                                     <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><?php echo get_phrase('serial_no');?></th>
                                                <th><?php echo get_phrase('notice');?></th>
                                                <th><?php echo get_phrase('action');?></th>

                                            </tr>
                                        </thead>


                                        <tbody>
                                            <?php $sl=0; foreach ($all_notice as $notice) { $sl++;?>
                                               <tr id="<?php echo $notice['id']; ?>">
                                                <td><?php echo $sl; ?></td>

                                                <td id="notice"><?php echo $notice['notice']; ?></td>
                                                <td class="modalOpen">                                                   


                                                   <!-- <a data-toggle="modal" data-target="#exampleModal2" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></a> -->


                                                   <a href="javascript:void(0)"><i data-id1="<?php echo $notice['id']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="<?php echo get_phrase('delete');?>" class="fa fa-trash btn_delete_notice"></i></a>               

                                               </td>
                                           </tr>
                                       <?php } ?> 

                                   </tbody>
                               </table>
                           </div>
                       </div>
                       <!-- end: page -->

                   </div> <!-- end Panel -->
               </div>



               <!-- Modal Start -->                
               <div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title header-title mt-0"><?php echo get_phrase('add_new_notice');?></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">


                                        <form  role="form" action="<?=base_url('notice')?>" method="POST" >                                          

                                            <div class="form-group row">
                                                <label for="name" class="col-4 col-form-label"><?php echo get_phrase('notice');?><span class="text-danger">*</span></label>
                                                <div class="col-7">                            
                                                    <textarea type="text" required name="notice" class="form-control"></textarea>
                                                </div>
                                            </div>




                                            <div class="form-group row">
                                                <div class="col-8 offset-4">

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12 offset-4">
                                                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                                        <?php echo get_phrase('save');?>
                                                    </button>

                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>



                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->





            <!-- Edit Modal Start -->                
            <div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title header-title mt-0">Edit Notice</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">


                                        <form  role="form" action="<?=base_url('updateNotice')?>" method="POST" >


                                            <div class="form-group row">
                                                <label for="name" class="col-4 col-form-label">Notice<span class="text-danger">*</span></label>
                                                <div class="col-7">

                                                    <textarea type="text" id="e_notice" required name="notice" class="form-control"></textarea>
                                                </div>
                                                <input type="hidden" id="e_id" required name="id" >
                                            </div>





                                            <div id="edit_div"></div>

                                            <div class="form-group row">
                                                <div class="col-8 offset-4">

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12 offset-4">
                                                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                                        Save
                                                    </button>

                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>



                            </div>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->







            <!-- permission modal -->





        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
</div>
</div>
</div>


<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
    $(document).ready(function() {
        $('form').parsley();
    });
</script>


<script type="text/javascript">
    $(document).ready(function() {
        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: true,
            lengthMenu: [25,50,100,150,200],          
        });       
    } );

</script>

<!-- delete admin -->
<script type="text/javascript">
    $(document).on('click', '.btn_delete_notice', function(){         
     var id=$(this).data("id1");

     if(confirm("Are you sure you want to delete this?"))  
     {  
        $.ajax({  
         url:"deleteNotice",  
         method:"post",  
         data:{id:id},
         dataType:"text",  
         success:function(data){  
          $("#"+id).fadeOut(); 
      }  
  });  
    }  
}); 
</script>



<!-- permission modal open -->
<script>
    $('.modalOpen').on('click', function(){
        var id= $(this).closest('tr').attr('id');
        var notice= $(this).parent('tr').find('#notice').text();
        $("#e_notice").val(notice);
        $("#e_id").val(id);        
    });
</script>













