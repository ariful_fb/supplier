<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?php echo get_phrase('add_admin');?></b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="m-b-30">
                                                   <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#exampleModal"><b><?php echo get_phrase('add');?></b>
                                                   </button>
                                               </div>
                                           </div>
                                       </div>

                                       <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><?php echo get_phrase('serial_no');?></th>
                                                <th><?php echo get_phrase('name');?></th>
                                                <th><?php echo get_phrase('email');?></th>
                                                <th><?php echo get_phrase('action');?></th>

                                            </tr>
                                        </thead>


                                        <tbody>
                                            <?php $sl=0; foreach ($all_admin as $admin) { $sl++;?>


                                                <tr id="<?php echo $admin['id']; ?>">
                                                    <td><?php echo $sl; ?></td>

                                                    <td id="name"><?php echo $admin['name']; ?></td>
                                                    <td id="email"><?php echo $admin['email']; ?></td>
                                                    
                                                    <input id="role" type="hidden" value="<?php echo $admin['role'] ?>">
                                                    <td class="modalOpen">                                                   

                                                        <a data-toggle="modal" data-target="#exampleModal2" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></a>

                                                        <a href="javascript:void(0)"><i data-id1="<?php echo $admin['id']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_admin"></i></a>



                                                    </td>
                                                </tr>
                                            <?php } ?> 

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                    </div>



                    <!-- Modal Start -->                
                    <div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title header-title mt-0"><?php echo get_phrase('add_new_admin');?></h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box">


                                                <form  role="form" action="<?=base_url('all_admin')?>" method="POST" >                                          

                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label"><?php echo get_phrase('name');?><span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <input type="text" required name="name" class="form-control"
                                                            placeholder="<?php echo get_phrase('enter_name');?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="email" class="col-4 col-form-label"><?php echo get_phrase('email');?><span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <input type="text" required name="email" class="form-control"
                                                            placeholder="<?php echo get_phrase('enter_email');?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="password" class="col-4 col-form-label"><?php echo get_phrase('password');?><span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <input type="password" required name="password" class="form-control"
                                                            placeholder="<?php echo get_phrase('enter_password');?>">
                                                        </div>
                                                    </div> 

                                                    <div class="form-group row">

                                                        <label class="col-4 col-form-label"><?php echo get_phrase('select_role');?><span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <select required name="role_id" class="form-control">
                                                                <option value="">Select Role</option>
                                                                <?php echo getRole(); ?>
                                                            </select>
                                                        </div>
                                                    </div> 




                                                    <div class="form-group row">
                                                        <div class="col-8 offset-4">

                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-12 offset-4">
                                                            <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                                                Save
                                                            </button>

                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->





                    <!-- Edit Modal Start -->                
                    <div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title header-title mt-0">Edit Admin</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-box">


                                                <form  role="form" action="<?=base_url('updateAdmin')?>" method="POST" >


                                                    <div class="form-group row">
                                                        <label for="name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <input type="text" id="e_name" required name="name" class="form-control"
                                                            placeholder="Enter Name">
                                                        </div>
                                                        <input type="hidden" id="e_id" required name="id" >
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="email" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <input type="text" id="e_email" required name="email" class="form-control"
                                                            placeholder="Enter Email">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="email" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <input type="text" id="e_password" name="password" class="form-control"
                                                            placeholder="******">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="email" class="col-4 col-form-label">Select Role<span class="text-danger">*</span></label>
                                                        <div class="col-7">
                                                            <select class=" form-control" name="role_id" tabindex="-1"  required="required" id="admin_edit_modal">                        
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <div id="edit_div"></div>

                                                    <div class="form-group row">
                                                        <div class="col-8 offset-4">

                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-12 offset-4">
                                                            <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                                                Save
                                                            </button>

                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->









                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
    </div>
</div>
</div>


<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
    $(document).ready(function() {
        $('form').parsley();
    });
</script>



<!-- delete admin -->
<script type="text/javascript">
    $(document).on('click', '.btn_delete_admin', function(){         
     var id=$(this).data("id1");

     if(confirm("Are you sure you want to delete this?"))  
     {  
        $.ajax({  
         url:"deleteAdmin",  
         method:"post",  
         data:{id:id},
         dataType:"text",  
         success:function(data){  
          $("#"+id).fadeOut(); 
      }  
  });  
    }  
}); 
</script>



<!-- permission modal open -->
<script>
    $('.modalOpen').on('click', function(){
        var id= $(this).closest('tr').attr('id');
        var name= $(this).parent('tr').find('#name').text();
        var email= $(this).parent('tr').find('#email').text();
        var role_id= $(this).parent('tr').find('#role').val();
       // alert(role_id);

        //alert(id);
        $("#e_name").val(name);
        $("#e_email").val(email);
        $("#e_id").val(id);

        $.ajax({
            url:'AdminController/render_admin',
            method:'POST',
            data:{id:role_id},
            success:function(data){
                $('#admin_edit_modal').html(data);
            }
        })

        
    });
</script>

