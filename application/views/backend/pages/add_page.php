<div class="content-page">
    <div class="content">
        <div class="container-fluid">


            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">


                    </div>
                </div>
            </div>
            <!-- end page title end breadcrumb -->      

            <!-- success and error msg showing area -->
            <div class="row">                
                <div class="col-md-12">                 


                    <?php if ($this->session->userdata('log_err')) { ?>
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <span><?= $this->session->userdata('log_err'); ?></span>
                        </div>
                    <?php } $this->session->unset_userdata('log_err'); ?>
                    <?php if ($this->session->userdata('log_scc')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <span><?= $this->session->userdata('log_scc'); ?></span>
                        </div>
                    <?php } $this->session->unset_userdata('log_scc'); ?>
                </div>

            </div>

            <!-- msg showing area end -->

            <div class="row">

                <div class="col-lg-12">
                    <div class="card-box">
                        <h4 class="page-title">Page Content</h4><hr>
                        <form action="addPage" method="POST" role="form">
                           <div class="col-md-12">
                            <label>Page Title</label>
                            <div class="form-group">
                                <input class="form-control" type="text" name="page_title" value="" required="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Menu Title</label>
                            <div class="form-group">
                                <input class="form-control" type="text" name="menu_title" value="" required="">
                            </div>
                        </div>
                        <!-- <div class="col-md-12">
                            <label>Meta Title</label>
                            <div class="form-group">
                                <input class="form-control" type="text" name="meta_title" value="">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Meta Description</label>
                            <div class="form-group">
                                <input class="form-control" type="text" name="meta_description" value="">
                            </div>
                        </div> -->

                        <div class="col-md-12">
                            <label>Page Content</label>
                            <div class="form-group">
                                <textarea id="editor" name="content"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 offset-4">
                                <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                                    Save
                                </button>

                            </div>
                        </div>
                    </form>

                </div>

            </div> 

        </div>
        <!-- end page title end breadcrumb -->

    </div> <!-- end container -->
</div>
</div>

<!-- parsely -->
<script type="text/javascript" src="assets/plugins/parsleyjs/parsley.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>


