<div class="content-page">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">

						
					</div>
				</div>
			</div>

			<!-- success and error msg showing area -->
			<div class="row">
				<div class="col-md-12">
					<?php if($this->session->userdata('log_err')){ ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<span><?=$this->session->userdata('log_err');?></span>
						</div>
					<?php } $this->session->unset_userdata('log_err');?>
					<?php if($this->session->userdata('log_scc')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<span><?=$this->session->userdata('log_scc');?></span>
						</div>
					<?php } $this->session->unset_userdata('log_scc'); ?>
				</div>

			</div>
			<div class="row">

				<div class="col-sm-12">

					<div class="card-box">
						<h4 class="page-title">VIEW PAGES</h4><hr>
						<div class="form-group row" >
							<div class="col-8 offset-4">
								<a href="add_page">
									<button  class="btn btn-default btn-rounded waves-effect waves-light" ><b>Add Page</b>
									</button>
								</a>

							</div><br>
						</div>


						<table class="table table-striped add-edit-table" id="datatable-buttons">
							<thead>
								<tr>
									<th>Page Title</th>
									<th>Menu Title</th>
									<th>Meta Title</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($page as $data){?>
									<tr class="gradeU">
										<td ><?php echo $data['page_title'];?></td>
										<td><?php echo $data['menu_title'];?></td>
										<td><?php echo $data['meta_title'];?></td>
										<td class="actions">
											<a href="delete_page/<?php echo $data['id'];?>"  onclick="return confirm('Are you sure you want to delete?');" ><i class="fa fa-trash-o"></i></a>&nbsp;
											<a href="edit_page/<?php echo $data['id'];?>" ><i class="fa fa-pencil"></i></a>										                     
										</td>
									</tr>
								<?php  } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- end: page -->

			</div> <!-- end Panel -->

		</div> <!-- end container -->
	</div>
</div>
<!-- end wrapper -->
<!-- Button trigger modal -->

