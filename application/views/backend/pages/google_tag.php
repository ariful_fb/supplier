<div class="content-page">
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">

						
					</div>
				</div>
			</div>

			<!-- success and error msg showing area -->
			<div class="row">
				<div class="col-md-12">
					<?php if($this->session->userdata('log_err')){ ?>
						<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<span><?=$this->session->userdata('log_err');?></span>
						</div>
					<?php } $this->session->unset_userdata('log_err');?>
					<?php if($this->session->userdata('log_scc')){ ?>
						<div class="alert alert-success">
							<a href="#" class="close" data-dismiss="alert">&times;</a>
							<span><?=$this->session->userdata('log_scc');?></span>
						</div>
					<?php } $this->session->unset_userdata('log_scc'); ?>
				</div>

			</div>
			<div class="row">

				<div class="col-sm-12">

					<div class="card-box">
						<h4 class="page-title">Google Tags</h4><hr>
						<div class="form-group row" >
							<div class="col-8 offset-4">
								
							</div><br>
						</div>


						<table class="table table-striped add-edit-table" id="datatable-buttons">
							<thead>
								<tr>
									<th>Tag Name</th>
									<th>Tag Description</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($tags as $data){?>
									<tr class="gradeU" id="<?php echo $data['id'];?>">
										<td ><?php echo $data['name'];?></td>
										<td id="tag"><?php echo $data['tag'];?></td>
										<td class="actions modalOpen">
											
											<a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal2" ><i class="fa fa-pencil"></i></a>										                     
										</td>
									</tr>
								<?php  } ?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- end: page -->

			</div> <!-- end Panel -->

		</div> <!-- end container -->
	</div>
</div>
<!-- end wrapper -->
<!-- Button trigger modal -->
<!-- Edit Modal Start -->                
<div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title header-title mt-0">Edit Tag</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">

							<form  role="form" action="<?=base_url('admin/update_tag')?>" method="POST" id="edit_user">

								<input type="hidden" id="e_id"  name="id" >
								<div class="form-group row">
									<label for="name" class="col-4 col-form-label">Tag Description<span class="text-danger">*</span></label>
									<div class="col-7">                            
										<input type="text" required name="tag" class="form-control" id="e_tag">
									</div>
								</div>
								

								<div class="form-group row">
									<div class="col-12 offset-4">
										<button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
											Update
										</button>

									</div>
								</div>
							</form>

						</div>
					</div>



				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$('.modalOpen').on('click', function(){
		var id= $(this).closest('tr').attr('id');
		var tag= $(this).parent('tr').find('#tag').text();
		$("#e_tag").val(tag);
		$("#e_id").val(id);


	});



</script>