<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                <li class="has_sub">
                    <a href="admin/notifications" class="waves-effect"><i class="fa fa-cog fa-fw"></i> <span>Delete Notifications</span> </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog fa-fw"></i> <span>Evaluation</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="evaluation/all">Approved Evaluation</a></li>
                        <li><a href="admin/new_evaluation">New Evaluation</a></li>
                    </ul>
                </li> 

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog fa-fw"></i> <span>Suppliers</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="admin/approved_suppliers">Approved Suppliers</a></li>
                        <li><a href="admin/new_suppliers">New Suppliers</a></li>
                        <li><a href="" data-toggle="modal" data-target="#conf_email_modal" id="sendConf">Send Confirmation</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog fa-fw"></i> <span>Users</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="admin/manage_users">Manage Users</a></li>
                        <li><a href="admin/delete/gdpr_request">Delete GDPR</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-cog fa-fw"></i> <span>Configuration</span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="view_page">Pages</a></li>
                        <li><a href="admin/tags">Tags</a></li>
                    </ul>
                </li> 



            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->


<!-- confirm email modal -->
<div class="modal fade" id="conf_email_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send Confirmation Mail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="confEmailForm">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="confEmail" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>


<script>
    $('#sendConf').on('click', function(e){
        e.preventDefault();
        /*swal({
            text: 'Enter email to send confirmation',
            content: "input",
            button: {
                catch: {
                    text: "Send",
                },
            },
        }).then((value) => {
            switch (value) {

                case "catch":
                $.ajax({
                    url:"<?php echo base_url() ?>supplier/mail/confirmation/?email="+email,
                    method:'GET',
                    success:function(){
                        swal('Email Sent');
                    }
                })
                break;

                default:
                swal("Email not sent!");
            }
        })
        .then(name=>{
            console.log(name);
        });*/
    })

    $('#confEmailForm').on('submit', function(e){
        e.preventDefault();
        var email = $('#confEmail').val();
        $.ajax({
            url:'supplier/mail/confirmation/?email='+email,
            method: 'GET',
            success: function(){
                $('#conf_email_modal').modal('toggle');
                swal('Email sent successfully');
            }
        })
    })
</script>
