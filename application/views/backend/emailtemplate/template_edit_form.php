
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?php echo get_phrase('email_template');?></b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="card-box">


                                        <form role="form" action="emailtemplate/update" method="post" >

                                            <input type="hidden" name="email_template_id" value="<?= $template->email_template_id; ?>">



                                            <div class="form-group row">
                                                <label for="email_template_subject"
                                                class="col-3 col-form-label"><?php echo get_phrase('subject');?><span
                                                class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    <input type="text" class="form-control"
                                                    name="email_template_subject" value="<?= $template->email_template_subject; ?>"
                                                    placeholder="Subject Text" required="required">                                   
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email_template"
                                                class="col-3 col-form-label"><?php echo get_phrase('email_template_text');?><span
                                                class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    <textarea type="text" id="email_template" class="form-control summernote" name="email_template"
                                                    rows="3" required="required"><?= $template->email_template; ?></textarea>                              
                                                </div>
                                            </div>

                                            <div class="form-group row">   
                                                <div class="col-8 offset-4">
                                                    <button class="btn btn-primary waves-effect waves-light" type="submit"><?php echo get_phrase('submit');?>
                                                </button>
                                            </div>
                                        </div>
                                    </form>



                                </div>
                            </div>
                            <!-- end: page -->

                        </div> <!-- end Panel -->
                    </div>

                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
    </div>
</div>
</div>

<!--summernote init-->
<script src="assets/plugins/summernote/summernote.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });



    //summernote on
    $(document).ready(function(){
        $('.summernote').summernote({
            height: 500,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                 // set focus to editable area after initializing summernote
        });
    });
</script>



