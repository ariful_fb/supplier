<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url();?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Job Recruitment">
	<meta name="author" content="partia socialiste">

	<link rel="shortcut icon" href="assets/frontend/favicon.png">
	<title>Login</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.css" rel="stylesheet" type="text/css" />

	<script src="assets/js/modernizr.min.js"></script>

</head>
<body>

	<div class="account-pages"></div>
	<div class="clearfix"></div>		
	<div class="wrapper-page">
		<div class="card-box">
			<div class="p-20 bg-dark text-center" >
				<div class="col-12" >
					<img src="<?php echo base_url() ;?>assets/frontend/f_logo.png"/>
				</div>					
			</div>

			<div class="panel-heading">
				<h4 class="text-center"></h4>
			</div>


			<div class="p-20">
				
				<?php $this->load->view('backend/auth/session_msg'); ?>

				<?php if($this->session->userdata('error_msg')){  } ?>

				<form action="check_login" method="post" id="loginForm">

					<div class="form-group ">
						<div class="col-12">
							<input class="form-control" name="email" type="email" required="" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<div class="col-12">
							<input class="form-control" name="password" type="password" required="" placeholder="Password" minlength="6">
						</div>
					</div>
					<div class="form-group text-center m-t-40">
						<div class="col-12">
							<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
								Login
							</button>
						</div>
					</div>

					<!-- <div class="form-group m-t-20 m-b-0">
						<div class="col-12">
							<a href="forgot_password" class="text-dark"><i class="fa fa-lock m-r-5"></i> forgot_password</a>
						</div>
					</div> -->


				</form>

			</div>
		</div>


	</div>

	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/detect.js"></script>
	<script src="assets/js/fastclick.js"></script>
	<script src="assets/js/jquery.slimscroll.js"></script>
	<script src="assets/js/jquery.blockUI.js"></script>
	<script src="assets/js/waves.js"></script>
	<script src="assets/js/wow.min.js"></script>
	<script src="assets/js/jquery.nicescroll.js"></script>
	<script src="assets/js/jquery.scrollTo.min.js"></script>

	<script src="assets/js/jquery.core.js"></script>
	<script src="assets/js/jquery.app.js"></script>		
	<script type="text/javascript" src="plugins/parsleyjs/parsley.min.js"></script>

</body>
</html>


<script type="text/javascript">
	/*form validation*/

	$(document).ready(function() {
		$('form').parsley();
	});
</script>