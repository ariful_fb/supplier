<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo base_url();?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Job Recruitment">
    <meta name="author" content="partia socialiste">

    <link rel="shortcut icon" href="assets/frontend/favicon.png">

    
    <title>Forgot Passowrd</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>
<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class=" card-box">
            <div class="panel-heading">
                <h4 class="text-center"><img src="assets/images/logo.jpg" height="80px" width="95px;"></h4>
                <h4 class="text-center"> Forgot Password </h4>
            </div>

            <div class="p-20">

                <div class="col-12 flash_msg">
                    <?php  if ($this->session->flashdata('error_message')) :?>
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <span><?php echo $this->session->flashdata('error_message'); ?></span>
                        </div>

                    <?php endif; ?>
                    <?php  if ($this->session->flashdata('success_message')) :?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <span><?php echo $this->session->flashdata('success_message'); ?></span>
                        </div>                        

                    <?php endif; ?>
                </div>


                <form method="post" action="sendResetPassEmail" role="form" class="text-center">
                    <div >
                        <p style="text-align: justify;">
                            Please enter your email address and we will send you an email with a link to set your password.
                        </p>
                    </div><br>
                    <div class="form-group m-b-0">
                        <div class="input-group">
                            <input type="email" class="form-control" name="email" placeholder="Enter Email" required="">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-pink w-sm waves-effect waves-light">
                                    Reset
                                </button> 
                            </span>
                        </div>
                    </div>

                </form>
            </div>
        </div>


    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/detect.js"></script>
    <script src="assets/js/fastclick.js"></script>
    <script src="assets/js/jquery.slimscroll.js"></script>
    <script src="assets/js/jquery.blockUI.js"></script>
    <script src="assets/js/waves.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>

    <script src="assets/js/jquery.core.js"></script>
    <script src="assets/js/jquery.app.js"></script>

    <script type="text/javascript" src="plugins/parsleyjs/parsley.min.js"></script>

</body>
</html>

    <!--
    <script type="text/javascript">
        /*form validation*/
        $(document).ready(function() {
            $('form').parsley();
        });
    </script>
    -->
