<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url();?>">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Rs Flex">

  <link rel="shortcut icon" href="assets/frontend/favicon.png">
  <title>Forgot Passowrd</title>

  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

  <script src="assets/js/modernizr.min.js"></script>

</head>
<body>

  <div class="account-pages"></div>
  <div class="clearfix"></div>
  <div class="wrapper-page">
    <div class="card-box">
     <div class="panel-heading">
      <h4 class="text-center"><img src="assets/images/logo.jpg" height="85" width="100%;"></h4>

    </div>


    <div class="p-20">
      <div class="col-12">

      </div>
      <div class="col-12 flash_msg">
        <?php  if($this->session->userdata('err_message_pass')):?>

          <div class="alert alert-danger">
           <a href="#" class="close" data-dismiss="alert">&times;</a>
           <span><?php echo $this->session->userdata('err_message_pass'); ?></span>
           <?php echo $this->session->unset_userdata('err_message_pass'); ?>
         </div>
       <?php endif; ?>
       <?php  if($this->session->userdata('success_msg')):?>

         <div class="alert alert-success">
           <a href="#" class="close" data-dismiss="alert">&times;</a>
           <span><?php echo $this->session->userdata('success_msg'); ?></span>
           <?php echo $this->session->unset_userdata('success_msg'); ?>
         </div>                        
       <?php endif; ?>
       <?php  if($this->session->userdata('error_msg')):?>

         <div class="alert alert-danger">
           <a href="#" class="close" data-dismiss="alert">&times;</a>
           <span><?php echo $this->session->userdata('error_msg'); ?></span>
           <?php echo $this->session->unset_userdata('error_msg'); ?>
         </div>

       <?php endif; ?>
     </div>

     <a class="btn btn-default text-right" href="<?php echo base_url();?>">Home</a>
   </div>
 </div>


</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javaScript">
  $("#passResetForm").validate({
    rules: {

      new_pass:{
        required: true,
        minlength: 6
      },
      conf_pass:{
        required: true,
        minlength: 6
      },
    }
  });
</script>

