<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo base_url();?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Rs Flex">

    <link rel="shortcut icon" href="assets/frontend/favicon.png">
    <title>Forgot Passowrd</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

    <script src="assets/js/modernizr.min.js"></script>

</head>
<body>

    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        <div class="card-box">
         <div class="panel-heading">
            <h4 class="text-center"><img src="assets/images/logo.jpg" height="80px" width="95px;"></h4>
            <h4 class="text-center">Reset Password</h4>
        </div>


        <div class="p-20">
            <div class="col-12">
                <p>Enter your password</p>
            </div>
            <div class="col-12 flash_msg">
                <?php  if ($this->session->flashdata('error_message')) :?>
                  <div class="alert alert-danger">
                   <a href="#" class="close" data-dismiss="alert">&times;</a>
                   <span><?php echo $this->session->flashdata('error_message'); ?></span>
               </div>


                <?php endif; ?>
            <?php  if ($this->session->flashdata('success_message')) :?>
               <div class="alert alert-success">
                   <a href="#" class="close" data-dismiss="alert">&times;</a>
                   <span><?php echo $this->session->flashdata('success_message'); ?></span>
               </div>                        
            <?php endif; ?>
            <?php  if ($this->session->flashdata('err_message_pass')) :?>
              <div class="alert alert-danger">
                 <a href="#" class="close" data-dismiss="alert">&times;</a>
                 <span><?php echo $this->session->flashdata('err_message_pass'); ?></span>
             </div>

            <?php endif; ?>
     </div>
     <form class="form-horizontal m-t-20" method="POST" action="updateresetpassword" id="passResetForm">

        <div class="form-group ">
            <div class="col-12">
                <input class="form-control" type="password"  placeholder="New password" name="new_pass" id="new_pass" min-length="6" required>
            </div>


        </div>

        <div class="form-group">
         <div class="col-12">
            <input class="form-control" type="password"  placeholder="Confirm password" name="conf_pass" id="conf_pass" min-length="6" required>
        </div>
    </div>


    <div class="form-group text-center m-t-40">
        <div class="col-12">
            <input type="hidden" name="email" value="<?php if (isset($email)) {
                echo $email;
                                                     }?>">
            <input type="hidden" name="authCode" value="<?php if (isset($authCode)) {
                echo $authCode;
                                                        }?>">
            <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light"
            type="submit">Continue</button>
        </div>
    </div>


</form>

</div>
</div>


</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javaScript">
    $("#passResetForm").validate({
        rules: {

            new_pass:{
                required: true,
                minlength: 6
            },
            conf_pass:{
                required: true,
                minlength: 6
            },
        }
    });
</script>

