<div class="content-page">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Manage Evaluation</h4>
              <div class="p-20">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box">
                      <div class="row">
                        <div class="col-sm-2">
                          <div class="m-b-30">
                          </div>
                        </div>
                      </div>
                      <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Id</th>
                            <th>Company Name</th>
                            <th>Nick Name</th>
                            <th>Email</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $sl=0;
                          foreach ($new_suppliers as $data) :?>

                            <tr userId="<?php echo $data['user_id']; ?>">
                              <td><?php echo ++$sl; ?></td>
                              <td><?php echo $data['company_name']; ?></td>
                              <td><?php echo $data['nick_name']; ?></td>
                              <td><?php echo $data['user_email']; ?></td>

                              <td>
                                <a href="" class="approveSupplier"><i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="Approve Supplier"></i>
                                </a>
                                <a style="color:red;"   ><i class="fa fa-times rejectSupplier" data-toggle="tooltip" data-placement="top" title="Reject Supplier"></i>
                                </a>
                                <a href=""  data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-eye showUserInfoIcon" data-toggle="tooltip" data-placement="top" title="Supplier Info"></i></a>
                                <div style="display:none" class="userdata">
                                  <?php echo  json_encode($data);?>
                                </div>
                              </td>
                            </tr>
                          <?php endforeach; ?>

                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- end: page -->

                </div> <!-- end Panel -->
              </div>

            </div> <!-- end card-box -->
          </div> <!-- end col -->
        </div>
      </div>
    </div>
  </div>


  <!-- supplier details -->
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="list-group" id="supDetModBody">
          <li class="list-group-item list-group-item-action active">Supplier Info</li>
          <li class="list-group-item list-group-item-light userEmail"></li>
          <li class="list-group-item list-group-item-light userNick"></li>
          <li class="list-group-item list-group-item-light userCountry"></li>
          <li class="list-group-item list-group-item-light userCity"></li>
          <li class="list-group-item list-group-item-light userIndustry"></li>
          <li class="list-group-item list-group-item-light userComDesc"></li>
          <li class="list-group-item list-group-item-light comCoreDesc"></li>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(document).on('click','.showUserInfoIcon', function(){
      var userId = $(this).closest('tr').attr('userId'); 
      $.ajax({
        url:'AdminController/supplierDetails/'+userId,
        method:'POST',
        data:{},
        success: function(data){
          data = JSON.parse(data);
          var html = `
          <li class="list-group-item list-group-item-action active">Supplier Info</li>
          <li class="list-group-item list-group-item-light"> Email: `+data.user_email+`</li>
          <li class="list-group-item list-group-item-light"> Nick name : `+data.nick_name+`</li>
          <li class="list-group-item list-group-item-light"> Contact name : `+data.contact_name+`</li>
          <li class="list-group-item list-group-item-light"> Main country: `+data.main_country+`</li>
          <li class="list-group-item list-group-item-light"> Main city: `+data.main_city+`</li>
          <li class="list-group-item list-group-item-light"> State: `+data.state+`</li>
          <li class="list-group-item list-group-item-light"> industry: `+data.industryName+`</li>
          <li class="list-group-item list-group-item-light">Company Name: `+data.company_name+`</li>
          <li class="list-group-item list-group-item-light">Company description: `+data.company_description+`</li>
          <li class="list-group-item list-group-item-light">Company Core Business: `+data.company_core_business+`</li>
          <li class="list-group-item list-group-item-light">Skills: `+data.skills+`</li>
          <li class="list-group-item list-group-item-light">Main activities: `+data.main_activities+`</li>
          <li class="list-group-item list-group-item-light">References: `+data.reference+`</li>
          <li class="list-group-item list-group-item-light">Contact phone: `+data.contact_phone+`</li> 
          `;
          $('#supDetModBody').html(html);
        }
      })
    })


    //approve supplier
    $('.approveSupplier').on('click', function(e){
      e.preventDefault();
      var userId = $(this).closest('tr').attr('userId');
      var parent = $(this).closest('tr');
      $.ajax({
        url:'supplier/change-status/'+userId+'/'+1,
        method:'POST',
        data:{},
        success: function(data){
          if(data=='1'){
            swal('Supplier approved!');
            parent.fadeOut(5000);
          } else {
            swal('Something is wrong!');
          }
        }
      })
    })

    //reject supplier
    $('.rejectSupplier').on('click', function(e){
      e.preventDefault();
      var userId = $(this).closest('tr').attr('userId');
      var parent = $(this).closest('tr');
      var elem   = $(this);
      $.ajax({
        url:'supplier/change-status/'+userId+'/'+2,
        method:'POST',
        data:{},
        success: function(data){
          if(data=='1'){
            swal('Supplier rejected!');
            elem.parent('a').css('color', 'black');
          } else {
            swal('Something is wrong!');
          }
        }
      })
    })
  </script>