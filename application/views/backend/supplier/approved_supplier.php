<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<div class="content-page">
  <div class="content">
    <div class="container-fluid">
     <?php $this->load->view('frontend/templates/session_msg');?>
     <div class="row">
      <div class="col-md-12">
        <div class="card-box">
          <h4 class="m-t-0 header-title"><b>Suppliers</h4> 
            <div class="p-20">

              <div class="row">


                <div class="col-sm-12">

                  <div class="card-box">
                    <div class="row">

                      <div class="col-sm-2">
                        <div class="m-b-30">

                        </div>
                      </div>

                    </div>

                    <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Company Name</th>
                          <th>Nick Name</th>
                          <th>Email</th>
                          
                          <th>Action</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php $sl=0;
                        foreach ($approved_suppliers as $data) :
                          $sl++; ?>

                          <tr id="<?php echo $data['user_id']; ?>">
                            <td><?php echo $sl; ?></td>
                            <td ><?php echo $data['company_name']; ?></td>
                            <td id="nick_name"><?php echo $data['nick_name']; ?></td>
                            <td id="user_email"><?php echo $data['user_email']; ?></td>
                            
                            <td class="modalOpen">


                              <a  href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal2" ><i class="fa fa-pencil"></i>
                              </a>
                              <a style="color:red;"  href="" ><i class="fa fa-times rejectSupplier" data-toggle="tooltip" data-placement="top" title="Delete Supplier"></i>
                              </a>
                              <a href=""  data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fa fa-eye showUserInfoIcon" data-toggle="tooltip" data-placement="top" title="Supplier Info"></i></a>

                            </td>

                          </tr>
                        <?php endforeach; ?>

                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end: page -->

              </div> <!-- end Panel -->
            </div>

          </div> <!-- end card-box -->
        </div> <!-- end col -->
      </div>
    </div>
  </div>
</div>


<!-- Modal Start -->                
<div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title header-title mt-0">Add User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">

              <form  role="form" action="admin/add_user" method="POST" id="add_user" >                                         
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Nick Name<span class="text-danger">*</span></label>
                  <div class="col-7">                            
                    <input type="text" required name="nick_name" class="form-control" value="sdf">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                  <div class="col-7">                            
                    <input type="text" required name="user_email" class="form-control" value="sdfs@gmail.com">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="password" class="col-4 col-form-label">Password<span class="text-danger">*</span></label>
                  <div class="col-7">                            
                    <input type="password" required name="password" class="form-control" value="123456">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-8 offset-4">

                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-12 offset-4">
                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                      Save
                    </button>

                  </div>
                </div>
              </form>

            </div>
          </div>



        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Edit Modal Start -->                
<div class="modal fade bs-example-modal-lg" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title header-title mt-0">Edit Supplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="javascript:void(0)">×</a></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">
              <form  role="form" action="<?=base_url('admin/update_supplier')?>" method="POST" id="edit_user">

                <input type="hidden" id="e_id"  name="user_id" >
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Nick Name<span class="text-danger">*</span></label>
                  <div class="col-7">                            
                    <input type="text" required name="nick_name" class="form-control" id="e_name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Email<span class="text-danger">*</span></label>
                  <div class="col-7">                            
                    <input type="text" required name="user_email" class="form-control" id="e_email">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Main Country<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="main_country" class="form-control" id="main_country">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Main City<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="main_city" class="form-control" id="main_city">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Company Name<span class="text-danger">*</span></label>
                  <div class="col-7">                            
                    <input type="text" required name="company_name" class="form-control" id="company_name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Other Countries<span class="text-danger"></span></label>
                  <div class="col-7">                            

                    <select class="select2_other_countries" name="countries[]" id="mySelect" multiple="multiple">

                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Size<span class="text-danger"></span>
                  </label>
                  <div class="col-7">                            
                    <select class="form-control" name="size" id="sizef">
                      <option value="1">Less then 50 employees</option>
                      <option value="2">Between 50-200 employees</option>
                      <option value="3">Greater then 200 employees</option>
                    </select>
                  </div>
                </div>


                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Industry<span class="text-danger"></span>
                  </label>
                  <div class="col-7">                            
                    <select class="form-control" name="industry" id="industry">
                      <?php foreach ($industry as $data) :?>
                        <option value="<?php echo $data['id'];?>"><?php echo $data['industryName'];?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Company Description/presentation<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="company_description" class="form-control" id="company_description">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Company Core Business<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="company_core_business" class="form-control" id="company_core_business">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Main Activities Managed<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="main_activities" class="form-control" id="main_activities">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">References<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="reference" class="form-control" id="reference">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Contact Phone<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="contact_phone" class="form-control" id="contact_phone">
                  </div>
                </div> 
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Contact Name<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <input type="text"  name="contact_name" class="form-control" id="contact_name">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="name" class="col-4 col-form-label">Skills<span class="text-danger"></span></label>
                  <div class="col-7">                            
                    <select class="js-example-basic-multiple" name="skill[]" id="mySelect" multiple="multiple">

                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="password" class="col-4 col-form-label">Password</label>
                  <div class="col-7">                            
                    <input type="password"  name="password" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-12 offset-4">
                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">
                      Update
                    </button>

                  </div>
                </div>
              </form>

            </div>
          </div>



        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- supplier details -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="list-group" id="supDetModBody">
        <li class="list-group-item list-group-item-action active">Supplier Info</li>
        <li class="list-group-item list-group-item-light userEmail"></li>
        <li class="list-group-item list-group-item-light userNick"></li>
        <li class="list-group-item list-group-item-light userCountry"></li>
        <li class="list-group-item list-group-item-light userCity"></li>
        <li class="list-group-item list-group-item-light userIndustry"></li>
        <li class="list-group-item list-group-item-light userComDesc"></li>
        <li class="list-group-item list-group-item-light comCoreDesc"></li>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
      </div>
    </div>
  </div>
</div>

<script>
  $('.modalOpen').on('click', function(){
    var id= $(this).closest('tr').attr('id');
    var name= $(this).parent('tr').find('#nick_name').text();
    var email= $(this).parent('tr').find('#user_email').text();
    $("#e_name").val(name);
    $("#e_email").val(email);
    $("#e_id").val(id);

    $.ajax({
      url:'AdminController/getSupplierInfo',
      method:'POST',
      data:{supplierId:id},
      dataType:'JSON',
      success:function(res){
        data = res[0];skill = res[1];var str='';var country='';
        $(".js-example-basic-multiple").empty();
        $(".select2_other_countries").empty();
        for (var i = res[3].length - 1; i >= 0; i--) {
          console.log(res[3][i].name);
          if(res[3][i].name!='null')
            country+='<option value="'+res[3][i].id+'" selected >'+res[3][i].name+'</option>';
        }
        for (var i = skill.length - 1; i >= 0; i--) {
          if(skill[i].name!='null')
            str+='<option value="'+skill[i].id+'" selected >'+skill[i].name+'</option>';
        }
        $(".js-example-basic-multiple").append(str);
        $(".select2_other_countries").append(country);
        $("#main_country").val(data.main_country);
        $("#main_city").val(data.main_city); 
        $("#company_name").val(data.company_name);
        $("#other_countries").val(data.other_countries);
        $("#sizef").val(data.size);
        $("#industry").val(data.industry);
        $("#company_description").val(data.company_description); 
        $("#company_core_business").val(data.company_core_business);
        $("#main_activities").val(data.main_activities);
        $("#reference").val(data.reference);
        $("#contact_phone").val(data.contact_phone);
        $("#contact_name").val(data.contact_name);
        $('.js-example-basic-multiple').select2({
          ajax:{
            url:'AdminController/getskills',
            dataType:'JSON',
            data:function(res){

            }
          }
        });
        $('.select2_other_countries').select2({
          ajax:{
            url:'AdminController/getCountries',
            dataType:'JSON',
            data:function(res){

            }
          }
        });

      }
    });

  });

  $(document).on('click','.showUserInfoIcon', function(){
    var userId = $(this).closest('tr').attr('id'); 
  //console.log('userId:'+userId);
  $.ajax({
    url:'AdminController/supplierDetails/'+userId,
    method:'POST',
    data:{},
    success: function(data){
      data = JSON.parse(data);
      var html = `
      <li class="list-group-item list-group-item-action active">Supplier Info</li>
      <li class="list-group-item list-group-item-light"> Email: `+data.user_email+`</li>
      <li class="list-group-item list-group-item-light"> Nick name : `+data.nick_name+`</li>
      <li class="list-group-item list-group-item-light"> Contact name : `+data.contact_name+`</li>
      <li class="list-group-item list-group-item-light"> Main country: `+data.main_country+`</li>
      <li class="list-group-item list-group-item-light"> Main city: `+data.main_city+`</li>
      <li class="list-group-item list-group-item-light"> State: `+data.state+`</li>
      <li class="list-group-item list-group-item-light"> industry: `+data.industryName+`</li>
      <li class="list-group-item list-group-item-light">Company Name: `+data.company_name+`</li>
      <li class="list-group-item list-group-item-light">Company description: `+data.company_description+`</li>
      <li class="list-group-item list-group-item-light">Company Core Business: `+data.company_core_business+`</li>
      <li class="list-group-item list-group-item-light">Skills: `+data.skills+`</li>
      <li class="list-group-item list-group-item-light">Main activities: `+data.main_activities+`</li>
      <li class="list-group-item list-group-item-light">References: `+data.reference+`</li>
      <li class="list-group-item list-group-item-light">Contact phone: `+data.contact_phone+`</li> 
      `;
      $('#supDetModBody').html(html);
    }
  })
})


  /*user delete*/
  $(document).on('click', '.rejectSupplier', function(e){
    e.preventDefault();
    var uId = $(this).closest('tr').attr('id');
    var item = $(this).closest('tr');
    swal("Supplier id along with other related infos will permanently destroy, you really want to delete?", {
      buttons: {
        yes: "Yes",
        no : 'No',
      },
    })
    .then((value) => {
      switch (value) {

        case "yes":

        $.ajax({
          url:'AdminController/deleteSupplier/'+uId+'/'+1,
          method:'',
          success: function(){
            swal("", "Account deleted successfully", "success");
            item.fadeOut(5000);
          }
        })
        break;

        case "no":
        swal("", "Account not deleted", "error");
        break;

        default:
        swal("Nothing just happened!");
      }
    });
  })
</script>     

