<base href="<?php echo base_url();?>" >


<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">


	<!-- Start content -->
	<div class="content">

		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-12">
					<div class="page-title-box">
						
						<?php  if($this->session->flashdata('scc_msg')):?>

							<div class="alert alert-success">
								<a href="#" class="close" data-dismiss="alert">&times;</a>
								<span><?php echo $this->session->flashdata('scc_msg'); ?></span>
							</div>           
						<?php endif; ?>
						<?php  if($this->session->userdata('access_denied')):?>

							<div class="alert alert-danger">
								<a href="#" class="close" data-dismiss="alert">&times;</a>
								<span><?php echo $this->session->userdata('access_denied'); ?></span>
								<?php echo $this->session->unset_userdata('access_denied'); ?>
							</div>
						<?php endif; ?>

					</div>
				</div>
			</div>



			<!--  PAGE TITLE -->
			<div class="page-title">
				<i class="icon-custom-right"></i>
				<h4 class="page-title">Home - Summary</h4>
			</div>
			<!-- BEGIN PlACE PAGE CONTENT HERE -->
			<div class="row">
				<!-- TOTAL VIDEO NUMBER -->
				<div class="col-md-4 col-sm-12 ">
					<div class="card widget-flat">
						<div class="card-body">
							<div class="float-right">
								<i class="mdi mdi-movie widget-icon"></i>
							</div>
							<h5 class="text-muted font-weight-normal mt-0" title="Number of Customers">Total Movies</h5>
							<h3 class="mt-3 mb-3"><?php if(isset($tot_movie))echo $tot_movie;?></h3>
						</div>
					</div>
				</div>
				<!-- TOTAL TV SERIES NUMBER -->
				<div class="col-md-4 col-sm-12 ">
					<div class="card widget-flat">
						<div class="card-body">
							<div class="float-right">
								<i class="mdi mdi-movie-roll widget-icon"></i>
							</div>
							<h5 class="text-muted font-weight-normal mt-0" title="Number of Customers">Total Tv Series</h5>
							<h3 class="mt-3 mb-3"><?php if(isset($tot_tvseries))echo $tot_tvseries;?></h3>
						</div>
					</div>
				</div>
				<!-- TOTAL EPISODE NUMBER -->
				<div class="col-md-4 col-sm-12 ">
					<div class="card widget-flat">
						<div class="card-body">
							<div class="float-right">
								<i class="mdi mdi-video-stabilization widget-icon"></i>
							</div>
							<h5 class="text-muted font-weight-normal mt-0" title="Number of Customers">Total Episodes</h5>
							<h3 class="mt-3 mb-3"><?php if(isset($tot_episodes))echo $tot_episodes;?></h3>
						</div>
					</div>
				</div>
			</div>
			<div style="margin: 20px;"></div>
			<div class="row">
				<!-- TOTAL USER NUMBER -->
				<div class="col-md-4 col-sm-12 ">
					<div class="card widget-flat">
						<div class="card-body">
							<div class="float-right">
								<i class="mdi mdi-account-multiple-check widget-icon"></i>
							</div>
							<h5 class="text-muted font-weight-normal mt-0" title="Number of Customers">Total Registered User</h5>
							<h3 class="mt-3 mb-3"><?php if(isset($tot_users))echo $tot_users;?></h3>
						</div>
					</div>
				</div>
				<!-- TOTAL ACTIVE SUBSCRIPTION -->
				<div class="col-md-4 col-sm-12 ">
					<div class="card widget-flat">
						<div class="card-body">
							<div class="float-right">
								<i class="mdi mdi-wallet-membership widget-icon"></i>
							</div>
							<h5 class="text-muted font-weight-normal mt-0" title="Number of Customers">Total Active Subscription</h5>
							<h3 class="mt-3 mb-3">
								<?php if(isset($tot_subscription))echo $tot_subscription;?></h3>
							</div>
						</div>
					</div>
					<!-- REVENUE THIS MONTH -->
					<div class="col-md-4 col-sm-12 ">
						<div class="card widget-flat">
							<div class="card-body">
								<div class="float-right">
									<i class="mdi mdi-square-inc-cash widget-icon"></i>
								</div>
								<h5 class="text-muted font-weight-normal mt-0" title="Number of Customers">Sales This Month</h5>
								<h3 class="mt-3 mb-3">
									$<?php if(isset($tot_sale))echo $tot_sale;?>				</h3>
								</div>
							</div>
						</div>
					</div>






				</div> <!-- container -->

			</div> <!-- content -->
		</div>


