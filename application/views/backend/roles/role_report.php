<div class="wrapper">
    <div class="container-fluid">


        <!-- All Users -->
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
					<div class="btn-group pull-right">
                        <ol class="breadcrumb hide-phone p-0 m-0">
                            <li class="breadcrumb-item"><a href="dashboard">JanTrac</a></li>
							 <li class="breadcrumb-item"><a href="add_role">Role</a></li>
                            
                            <li class="breadcrumb-item active">Users List</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Users List</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <!-- end row -->

        <div class="row">

            <div class="col-sm-12">

                <div class="card-box">
                    <table class="table table-striped add-edit-table vendor_table" id="datatable-editable">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>User Name</th>
                                <th>User email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $rows;?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: page -->

        </div> <!-- end Panel -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->
