   <!-- Footer --><br> <br>
   <footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h4>This JanTrac release is still in user beta testing</h4>
                &copy; 2017 - <script>document.write(new Date().getFullYear())</script> JanTrac LLC. All rights reserved. Powered by <a target="_blank" href="https://www.obsvirtual.com">OBS.</a>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->
<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Delete Role</h4>
    <div class="custom-modal-text">
        Really Want To Delete Role ?
    </div>
    <div  class="custom-modal-text">
        <button onclick="del()" id="delete" type="button" class="btn btn-danger btn-rounded waves-effect waves-light">YES</button>
        <button type="button" class="btn btn-success btn-rounded waves-effect waves-light" onclick="Custombox.close();">NO</button>
    </div>
</div>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>

<script src="assets/plugins/peity/jquery.peity.min.js"></script>

<!-- jQuery  -->
<!-- <script src="assets/plugins/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

<script src="assets/plugins/morris/morris.min.js"></script>
<script src="assets/plugins/raphael/raphael-min.js"></script> -->

<!-- <script src="assets/plugins/jquery-knob/jquery.knob.js"></script> -->

<!-- <script src="assets/pages/jquery.dashboard.js"></script> -->

<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>
<!-- Modal-Effect -->
<script src="assets/plugins/custombox/js/custombox.min.js"></script>
<script src="assets/plugins/custombox/js/legacy.min.js"></script>
<!-- <script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();

    });
</script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<!-- <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/pages/datatables.editable.init.js"></script>
<script>
    $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
    $("#product_table").DataTable();
</script> -->
<script type="text/javascript">
    $(document).ready(function(){
     $(".flash_msg").fadeOut(5000);
 });
</script>
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $('#datatable-editable').DataTable();
    var id=0;
    $(".row-remove").on('click', function(){
        id =($(this).closest('tr').attr('id'));
    })
    function del(){
        $.ajax({
            url:'delete_role/'+id,
            method: 'POST',
            success: function( data ){
                if(data == 'false'){
                    alert('This Role Can\'t be deleted');
                    Custombox.close();
                }else{
                    $("tr#"+id).fadeOut(5000);
                    Custombox.close();
                }
            }
        })        
    }
</script>
</body>

</html>