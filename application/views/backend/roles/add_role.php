<div class="content-page">
    <div class="content">
        <div class="container-fluid">


            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <?php  if($this->session->flashdata('add_role')):?>

                          <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <span><?php echo $this->session->flashdata('add_role'); ?></span>
                        </div>           
                    <?php endif; ?>
                    <?php  if($this->session->flashdata('scc_msg')):?>

                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <span><?php echo $this->session->flashdata('scc_msg'); ?></span>
                        </div>           
                    <?php endif; ?>

                    <?php  if($this->session->flashdata('add_err')):?>

                        <div class="alert alert-danger">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <span><?php echo $this->session->flashdata('add_err'); ?></span>
                     </div>            
                 <?php endif; ?>

                 <?php  if($this->session->flashdata('error_message')):?>

                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <span><?php echo $this->session->flashdata('error_message'); ?></span>
                    </div>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b><?php echo get_phrase('Role');?></b></h4>
                <div class="row">
                    <div class="col-12">
                        <div class="p-20">

                            <form class="form-horizontal" role="form" method="POST" action="add_role" id="addRole">
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Role Name</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" value="" name="name" required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Role Description</label>
                                    <div class="col-10">
                                        <textarea class="form-control" rows="5" name="description"></textarea>
                                    </div>
                                </div>
                                <?php echo $checkbox_item; ?>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label"></label>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- end row -->

            </div> <!-- end card-box -->
            <?php if(strlen($rows)>1): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">


                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="page-title">All Roles</h4><br>
                            <table class="table table-striped add-edit-table product_table" id="datatable-buttons">
                                <thead>
                                    <tr>
                                        <th>Role ID</th>
                                        <th>Role Name</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php echo $rows; ?>                          
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->
                </div> <!-- end Panel -->
            <?php endif; ?>

        </div> <!-- end container -->
    </div>
</div>
<!-- end wrapper -->
<div id="custom-modal" class="modal-demo">
    <button type="button" class="close" onclick="Custombox.close();">
        <span>&times;</span><span class="sr-only">Close</span>
    </button>
    <h4 class="custom-modal-title">Delete Role</h4>
    <div class="custom-modal-text">
        Really Want To Delete Role ?
    </div>
    <div  class="custom-modal-text">
        <button onclick="del()" id="delete" type="button" class="btn btn-danger btn-rounded waves-effect waves-light">YES</button>
        <button type="button" class="btn btn-success btn-rounded waves-effect waves-light" onclick="Custombox.close();">NO</button>
    </div>
</div>

