<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <?php  if($this->session->flashdata('edit_role')):?>

                          <div class="alert alert-success">
                             <a href="#" class="close" data-dismiss="alert">&times;</a>
                             <span><?php echo $this->session->flashdata('edit_role'); ?></span>
                         </div>


                     <?php endif; ?>

                     


                 </div>
             </div>
         </div>
         <!-- end page title end breadcrumb -->

         <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <h4 class="page-title">Edit Role</h4><br>
                    <div class="row">
                        <div class="col-12">
                            <div class="p-20">

                                <form class="form-horizontal" role="form" method="POST" action="edit_role/<?php echo $editId;?>" id="addRole">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Role Name</label>
                                        <div class="col-10">
                                            <input type="text" class="form-control" value="<?php echo $name;?>" name="name" required>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                       <label class="col-2 col-form-label">Role Description</label>
                                       <div class="col-10">
                                           <textarea class="form-control" rows="5" name="description" value=""><?php echo $description;?></textarea>
                                       </div>
                                   </div>
                                   <?php echo $checkbox_item; ?>

                                   <div class="form-group row">
                                    <label class="col-2 col-form-label"></label>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- end row -->

            </div> <!-- end card-box -->




        </div> <!-- end container -->
    </div>
</div>
<!-- end wrapper -->
