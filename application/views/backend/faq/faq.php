<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title"><b><?php echo get_phrase('manage_faq');?> </b></h4>
                        <div class="p-20">
                            <?php $this->load->view('session_msg'); ?>
                            <div class="row">

                                <div class="col-sm-12">

                                    <div class="card-box">
                                        <div class="row">

                                            <div class="col-sm-2">
                                                <div class="m-b-30">
                                                    <button type="submit" class="btn btn-default btn-rounded waves-effect waves-light" data-toggle="modal" data-target="#exampleModal"><b><a style="margin-right:10px;" href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/faq_add');"><?php echo get_phrase('add_faq');?>


                                                </a></b>
                                            </button>
                                        </div>
                                    </div>


                                </div>

                                <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><?php echo get_phrase('serial_no');?></th>
                                            <th><?php echo get_phrase('faq_question');?></th>
                                            <th><?php echo get_phrase('operation');?></th>

                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php $sl=0; foreach($all_faq as $faq): $sl++; ?>

                                        <tr>
                                            <td><?php echo $sl; ?></td>

                                            <td><?php echo $faq['faq_question']; ?></td>
                                            <td>


                                                <a style="margin-right:20px;" href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/faq_update/<?php echo $faq['faq_id']?>');"  >
                                                    <i class="fa fa-pencil"  data-toggle="tooltip" title="<?php echo get_phrase('edit');?>"></i>

                                                </a>

                                                <a href="javascript:;" onclick="confirm_modal('<?php echo base_url();?>delete_faq/<?php echo $faq['faq_id'];?>');" 
                                                    > 
                                                    <i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="<?php echo get_phrase('delete');?>" class="fa fa-trash btn_delete_skill"></i>

                                                </a>

                                            </td>

                                        </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: page -->

                </div> <!-- end Panel -->
            </div>









        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>
</div>
</div>
</div>


<script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(".select2").select2();
    $(document).ready(function() {
        $('form').parsley();
    });
</script>








