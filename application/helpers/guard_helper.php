<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*$loggedUserType = $ci->session->userdata('role');

function adminOnly()
{
    if ($loggedUserType != ADMIN) {
    }
}
*/

/**
 * This function will responsible for sending email
 *
 * @param string $subject email subject
 * @param string $body    html email template
 * @param string $from    from email address
 * @param string $to      to email address
 *
 * @return void
 */
function sendEmail($subject = '', $body = '', $from, $to)
{
    $ci =& get_instance();

    $ci->load->library('email');
    
    /*$config = [
        'wordwrap' => true,
        'mailtype' => 'html',

        'protocol' => 'smtp',
        'smtp_host' => 'smtp.mailtrap.io',
        'smtp_port' => 2525,
        'smtp_user' => '95f93696aec9ae',
        'smtp_pass' => '10d0ec39dd855c',
        'crlf' => "\r\n",
        'newline' => "\r\n"
    ];
    $ci->email->initialize($config);*/
    $config['mailtype'] ='html';
    $ci->email->initialize($config);
    $ci->email->from($from, 'look4supplier');
    $ci->email->to($to);
    $ci->email->cc('ariful.fb@gmail.com');
    //$ci->email->bcc('ariful.fb@gmail.com');
    $ci->email->subject($subject);
    $ci->email->message($body);

    $ci->email->send();
    //echo $ci->email->print_debugger();
}
