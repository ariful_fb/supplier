<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function prdie($data)
{
    echo"<pre>";
    print_r($data);
    die;
}
function pr($data)
{
    echo"<pre>";
    print_r($data);
}




function getFacebookUrl()
{
    $ci =& get_instance();
    $ci->load->library('facebook');
    $facebookURL =  $ci->facebook->login_url();
    return $facebookURL;
}
function getGoogleUrl()
{
    $ci =& get_instance();
    $ci->load->library('google');
    $googleURL =  $ci->google->loginURL();
    return $googleURL;
}

function metaValue($id)
{
    $CI = get_instance();
    $CI->load->model('FrontEndModel');
    $tags = $CI->FrontEndModel->getColumValue('tags','id',$id,'tag');
    return $tags;
}


/**
 * get all static page
 * needed to be available on all other pages
 * thats why we need to make it globally accessible
 *
 * @return array all skills
 */
function getPages()
{
    $CI = get_instance();
    $CI->load->model('FrontEndModel');
    $pages = $CI->FrontEndModel->get('pages');
    return $pages;
}

/**
 * All skill needed on search params and search option
 * needed to be available on all other pages
 * thats why we need to make it globally accessible
 *
 * @return array all skills
 */
function allSkill()
{
    $CI = get_instance();
    $CI->load->model('FrontEndModel');
    $skills = $CI->FrontEndModel->get('skills');
    return $skills;
}

function creatorName($id)
{
    $CI = get_instance();
    $CI->load->model('FrontEndModel');
    $name = $CI->FrontEndModel->getData('users', 'user_id', $id);
    return $name['nick_name'];
}

function getCountries($id)
{
    $CI = get_instance();
    $CI->load->model('FrontEndModel');
    $countries = $CI->FrontEndModel->get('countries');
    return $countries;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString.time();
}


/**
 * Search for supplier based on main_city, state, skill or/and keyword
 * I've just written too too much costly function
 *
 * @return string page items
 */
function searchSupplier($searchVal = '', $param = '')
{
    $CI = get_instance();
    $post = $CI->input->post();
    if (isset($param) && $param==true) {
        $clean =$searchVal;
    } else {
        $clean = $CI->security->xss_clean($post);
    }

    $data['paramCount'] = count(array_filter($clean));
    $arr=array_filter($clean);
    $CI->session->set_userdata('city', $clean['city']);
    $CI->session->set_userdata('state', $clean['state']);
    $CI->session->set_userdata('skill', $clean['skill']);
    $CI->session->set_userdata('keyword', $clean['keyword']);

    //return all data if no param specified
    if (count(array_filter($clean))==0) {
        $data['suppliers'] = $CI->FrontEndModel->supSearchByParams(['1'=>'1']);
        if (isset($param) && $param==true) {
            return $data;
        }
        $data['content'] = $CI->load->view('frontend/supplier/search_result', $data, true);
        echo $data['content'];
        die;
    }

    //search by city
    if ($clean['city']) {
        $supIdsByCity = $CI->FrontEndModel->supSearchByParams(['main_city'=>$clean['city']]);
        $supIdsByCity = array_column($supIdsByCity, 'user_id');
    } else {
        $supIdsByCity = [];
    }

    //search by state
    if ($clean['state']) {
        $supIdsByState = $CI->FrontEndModel->supSearchByParams(['state'=>$clean['state']]);
        $supIdsByState = array_column($supIdsByState, 'user_id');
    } else {
        $supIdsByState = [];
    }

    //search by skill
    if ($clean['skill']) {
        $supIdsBySkill =  $CI->FrontEndModel->search('supplier_skills', ['skill_id'=>$clean['skill']]);
        $supIdsBySkill = array_unique(array_column($supIdsBySkill, 'supplier_id'));
    } else {
        $supIdsBySkill = [];
    }


    //search by keyword
    if ($clean['keyword']=='') {
        $supIdsByKeyword = [];
    } else {
        $clean['keyword'] = trim($clean['keyword']);
        $supIdsByKeyword = $CI->FrontEndModel->supplierByKeywords($clean['keyword']);
    }

    //count occurance
    $userIds = array_merge($supIdsByCity, $supIdsByState, $supIdsBySkill, $supIdsByKeyword);
    $occurance=(array_count_values($userIds));
    
    //get supplier infos
    $suppliers=[];
    foreach ($occurance as $userId => $times) {
        $supplierInfo = $CI->FrontEndModel->supSearchByParams(['user_id'=>$userId]);
        if (!count($supplierInfo)) {
            continue;
        }
        $supplierInfo = $supplierInfo[0];
        $supplierInfo['occurance'] = $times;
        $suppliers[] = $supplierInfo;
    }

    //sort data
    $data['suppliers'] = $suppliers;
    usort($data['suppliers'], 'comp');
    if (isset($param) && $param==true) {
        return $data;
    }

    //view
    $data['content'] = $CI->load->view('frontend/supplier/search_result', $data, true);
    echo $data['content'];
}

/**
 * Search result comparison(based on occurance)
 *
 * @param array $arr1 array
 * @param array $arr2 array
 *
 * @return bool
 */
function comp($arr1, $arr2)
{
    return $arr1['occurance'] > $arr2['occurance'] ? 0 : 1;
}
