<?php

class AdminModel extends CI_Model
{
    public function insert($tableName, $data)
    {
        $result=$this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    public function updateData($tableName, $selector, $condition, $data)
    {

        return $this->db
        ->where($selector, $condition)
        ->update($tableName, $data);
        
    }

    public function getWhereTwo($tableName, $selector, $condition, $selectorTwo, $conditionTwo)
    {
        $this->db->select('*');
        $this->db->where($selector, $condition);
        $this->db->where($selectorTwo, $conditionTwo);
        $result=$this->db->get($tableName)->result_array();
        return $result;
    }

    public function deleteData($tableName, $selector, $condition)
    {
        $this->db->where($selector, $condition);
        $result=$this->db->delete($tableName);
        return $result;
    }
    public function getData($tableName, $selector, $condition)
    {
        $this->db->where($selector, $condition);
        $result=$this->db->get($tableName)->result_array();
        if ($result) {
            return $result[0];
        }
    }

    public function getColomData($tableName, $selector, $condition,$colomName)
    {
        $this->db->select($colomName);
        $this->db->where($selector, $condition);
        $result=$this->db->get($tableName)->result_array();
        if ($result) 
            return $result[0][$colomName];
        else
            return false;
    }

    public function get($tableName)
    {
        $result=$this->db->get($tableName)->result_array();
        return $result;
    }

    public function getAllData($tableName, $selector, $condition)
    {
        $this->db->where($selector, $condition);
        $result=$this->db->get($tableName)->result_array();
        return $result;
    }

    public function getSettingValue($settings_key)
    {
        $this->db->select('*');
        $this->db->from('rs_styld_tbl_settings');
        $this->db->where('settings_key', $settings_key);
        $result = $this->db->get()->row();
        if (!empty($result->settings_value)) {
            return $result->settings_value;
        } else {
            return false;
        }
    }

    public function getColumValue($tableName, $selector, $condition, $colName)
    {
        $this->db->select($colName);
        $this->db->from($tableName);
        $this->db->where($selector, $condition);
        $result = $this->db->get()->row();
        if (!empty($result)) {
            return $result->$colName;
        } else {
            return false;
        }
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getApprovedSupplier()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left');
        $this->db->where('users.approved', 1);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getNewSupplier()
    {
        $this->db->select('supplier_info.*, users.user_id, users.nick_name, users.user_email, users.role, users.is_deleted, users.new_suppliers, users.created_at, users.updated_at');
        $this->db->from('users');
        $this->db->join('roles', 'users.role=roles.id', 'left');
        $this->db->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left');
        $this->db->where('roles.name', 'supplier');
        $this->db->where('approved', '0');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getUsers()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('roles', 'users.role=roles.id', 'left');
        $this->db->where('roles.name', 'user');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSupplierInfo( $supplierId)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left');
        $this->db->join('industry', 'industry.id=supplier_info.industry', 'left');
        $this->db->where('users.user_id', $supplierId);
        $query = $this->db->get()->result_array();
        if($query)
            return $query[0];
    }
    public function getNotification()
    {
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->join('users', 'users.user_id=notifications.creator_id', 'left');
        $this->db->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left');
        return $query = $this->db->get()->result_array();

    }
    public function getCountries($arr)
    {
        $this->db->select('countries.*,country_name as name');
        $this->db->from('countries');
        $this->db->where_in('id',$arr);
        return $query = $this->db->get()->result_array();

    }
}
