<?php

class AuthModel extends CI_Model
{
	function __construct() {
		$this->tableName = 'users';
		$this->primaryKey = 'user_id';
	}
	public function checkLogin($post)
	{    
		$this->db->select('*');
		$this->db->where('email', $post['email']);
		$this->db->where('password', $post['password']);
		$query = $this->db->get('admin');
		$userInfo = $query->row();		
		return $userInfo; 
	}

	public function checkSignin($post)
	{    
		$this->db->select('*');
		$this->db->where('user_email', $post['email']);
		$this->db->where('password', $post['password']);
		$this->db->where('role', $post['role']);
		$this->db->where('approved', 1);
		$query = $this->db->get('users');
		$userInfo = $query->row();		
		return $userInfo; 
	}
	
	public function emailCheck( $email )
	{
		$this->db->select("*");
		$this->db->where('email', $email);
		$qry = $this->db->get('admin');
		return ( count( $qry->result() )>0 ) ? 1:0;
	}

	public function userEmailCheck( $email )
	{
		$this->db->select("*");
		$this->db->where('user_email', $email);
		$qry = $this->db->get('users');
		return ( count( $qry->result() )>0 ) ? 1:0;
	}
	
	
	public function infoByEmail($email)
	{
		$user = $this->db->where('user_email', $email)->get('users')->result_array();
		return $user[0];
	}
	
	public function saveAuthCode($authCode, $email)
	{	       
		$this->db->where('user_email', $email);
		$this->db->update('users', array('authcode'=>$authCode) );
	}
	
	public function getEmailTempltateById($id)
	{
		$this->db->select('*');
		$this->db->from('email_template');
		$this->db->where('email_template_id',$id);
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}

	public function insert($tableName,$data)
	{
		$result=$this->db->insert($tableName,$data);
		return $this->db->insert_id();
	}

	public function checkUser($userData = array()){
		if(!empty($userData)){
            //check whether user data already exists in database with same oauth info
			$this->db->select($this->primaryKey);
			$this->db->from($this->tableName);
			$this->db->where(array('oauth_provider'=>$userData['oauth_provider'],'oauth_uid'=>$userData['oauth_uid']));
			$prevQuery = $this->db->get();
			$prevCheck = $prevQuery->num_rows();

			if($prevCheck > 0){
				$prevResult = $prevQuery->row_array();

                //update user data
				$userData['updated_at'] = date("Y-m-d H:i:s");
				$update = $this->db->update($this->tableName,$userData,array('user_id'=>$prevResult['user_id']));

                //get user ID
				$userID = $prevResult['user_id'];
			}else{
                //insert user data
				$userData['created_at']  = date("Y-m-d H:i:s");
				$userData['updated_at'] = date("Y-m-d H:i:s");
				$userData['role'] = 2;
				if($this->session->insertIdUser){
					$userID = $this->session->insertIdUser;
					$this->session->unset_userdata('insertIdUser');
					$this->db->where('user_id', $userID);
					$this->db->update('users', $userData);
					
				}else{
					$insert = $this->db->insert($this->tableName,$userData);
                //get user ID
					$userID = $this->db->insert_id();
				}
				
			}
		}

        //return user ID
		return $userID?$userID:FALSE;
	}

	public function checkUser2($userData = array()){
		if(!empty($userData)){
            //check whether user data already exists in database with same oauth info
			$this->db->select($this->primaryKey);
			$this->db->from($this->tableName);
			$this->db->where(array('oauth_provider'=>$userData['oauth_provider'],'oauth_uid'=>$userData['oauth_uid']));
			$prevQuery = $this->db->get();
			$prevCheck = $prevQuery->num_rows();

			if($prevCheck > 0){
				$prevResult = $prevQuery->row_array();

                //update user data
				$userData['updated_at'] = date("Y-m-d H:i:s");
				$update = $this->db->update($this->tableName,$userData,array('user_id'=>$prevResult['user_id']));

                //get user ID
				$userID = $prevResult['user_id'];
			}
		}

        //return user ID
		return $userID?$userID:FALSE;
	}

	public function socialLogin($id)
	{    
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$query = $this->db->get('users');
		$userInfo = $query->row();		
		return $userInfo; 
	}
}