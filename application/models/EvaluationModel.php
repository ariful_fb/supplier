<?php

class EvaluationModel extends CI_Model
{
    public $table;
    public function __construct()
    {
        $this->table = 'evaluation';
    }
    public function all($tableName)
    {
        $res = $this->db
        ->get($tableName)
        ->result_array();

        return $res;
    }

    public function allEvaluation()
    {
        $res = $this->db
        ->select('evaluation.*, users.nick_name as creator,supplier_info.company_name as company_name, sup_info.nick_name as given_to')
        
        ->join('users', 'users.user_id=evaluation.creator_id', 'left')
        ->join('users as sup_info', 'sup_info.user_id=evaluation.supplier_id', 'left')
        ->join('supplier_info', 'evaluation.supplier_id=supplier_info.supplier_id', 'left')
        ->where('evaluation.is_approved', 1)
        ->order_by('evaluation.id','desc')
        ->get('evaluation')
        ->result_array();
        return $res;
    }

    public function newEvaluation()
    {
        $res = $this->db
        ->select('evaluation.*, users.nick_name as creator,supplier_info.company_name as company_name, sup_info.nick_name as given_to,sup_info.approved as sup_approved')
        ->join('users', 'users.user_id=evaluation.creator_id', 'left')
        ->join('users as sup_info', 'sup_info.user_id=evaluation.supplier_id', 'left')
        ->join('supplier_info', 'evaluation.supplier_id=supplier_info.supplier_id', 'left')
        ->where('evaluation.is_approved', 0)
        ->order_by('evaluation.id','desc')
        ->get('evaluation')
        ->result_array();

        
        return $res;
    }
}
