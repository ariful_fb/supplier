<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class PageModel extends CI_Model{
	
	public function getPost()
	{
		$this->db->select('picturepost.id as postId,board.title as title,picturepost.comments as comments,picturepost.message as postMessage,picture.*,storagefile.*');
		$this->db->from('picturepost');
		$this->db->join('board','board.id=picturepost.boardId','left');
		$this->db->join('picture','picture.id=picturepost.pictureId','left');
		$this->db->join('storagefile','storagefile.binId=picture.bin','left');
		$this->db->where('storagefile.name','small');
		$result=$this->db->get()->result_array();
		return $result;
		
	}

	public function getPages($tableName,$selector='',$condition='')
	{
		$this->db->select('*');
		$result=$this->db->get($tableName)->result_array();
		return $result;
	}
	public function insert($tableName,$data)
	{
		$result=$this->db->insert($tableName,$data);
		return $this->db->insert_id();
	}

	public function updateData($tableName,$selector,$condition,$data)
	{
		$this->db->where($selector,$condition);
		$result=$this->db->update($tableName,$data);
		return $result;
	}

	public function deleteData($tableName,$selector,$condition)
	{
		$this->db->where($selector,$condition);
		$result=$this->db->delete($tableName);
		return $result;
	}
	public function getData($tableName,$selector,$condition)
	{
		$this->db->where($selector,$condition);
		$result=$this->db->get($tableName)->result_array();
		if($result)
			return $result[0];
	}

	public function getAllData($tableName,$selector,$condition)
	{
		$this->db->where($selector,$condition);
		$result=$this->db->get($tableName)->result_array();
		return $result;
	}

	public function getSettingValue($settings_key){
		$this->db->select('*');
		$this->db->from('rs_styld_tbl_settings');
		$this->db->where('settings_key',$settings_key);
		$result = $this->db->get()->row();
		if(!empty($result->settings_value))
			return $result->settings_value;
		else
			return false;
	}

	public function getColumValue($tableName,$selector,$condition,$colName){
		$this->db->select($colName);
		$this->db->from($tableName);
		$this->db->where($selector,$condition);
		$result = $this->db->get()->row();
		if(!empty($result))
			return $result->$colName;
		else
			return false;
	}

	public function getInfo($table, $colName, $colValue) {
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($colName, $colValue);

		$query = $this->db->get();
		return $query->result_array();
	}
}