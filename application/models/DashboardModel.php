<?php
class DashboardModel extends CI_Model {



	public function getData($tableName)
	{
		$this->db->select('*');
		$this->db->from($tableName);
		$result=$this->db->get()->result_array();
		
		if($result)
			return count($result);
		else return 0;
	}

	public function getTotalAmount($tableName)
	{
		$this->db->select_sum('payment_gross');

		$this->db->from($tableName);
		$result=$this->db->get()->result_array();
		//print_r($result);die;
		if($result)
			return $result[0]['payment_gross'];
		else return 0;
	}





}