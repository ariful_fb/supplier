<?php

class FrontEndModel extends CI_Model
{
    public function insert($tableName, $data)
    {
        $result=$this->db
        ->insert($tableName, $data);
        
        return $this->db->insert_id();
    }
    public function insert_batch($tableName, $data)
    {
        $this->db->insert_batch($tableName, $data);
        return true;
    }

    public function insertSupplierInfo($tableName, $data)
    {
        $this->db->where_in('id', $data);
        $this->db->delete($tableName);
    }

    public function insertSupplier($tableName, $data)
    {
        $result=$this->db
        ->insert($tableName, $data);

        return $result;
    }
    public function deleteSupplier($supplierId, $skillId)
    {
        $this->db->where('supplier_id', $supplierId);
        $this->db->where('skill_id', $skillId);
        $this->db->delete('supplier_skills');
    }

    public function updateData($tableName, $selector, $condition, $data)
    {

        //print_r($data);die;
        $this->db->where($selector, $condition);
        $result=$this->db->update($tableName, $data);
        return $result;
    }

    public function getWhereTwo($tableName, $selector, $condition, $selectorTwo, $conditionTwo)
    {
        $this->db->select('*');
        $this->db->where($selector, $condition);
        $this->db->where($selectorTwo, $conditionTwo);
        $result=$this->db->get($tableName)->result_array();
        return $result;
    }

    public function deleteData($tableName, $selector, $condition)
    {
        $this->db->where($selector, $condition);
        $result=$this->db->delete($tableName);
        return $result;
    }

    /**
     * Delete single or multiple data
     *
     * @param string $table      table name
     * @param string $keyCol     column name where to perform condition eg: 'id'
     * @param array  $conditions eg: [1,2,3]
     *
     * @return boolean delete or not
     */
    public function delete($table, $keyCol, $values)
    {
        return $this->db
        ->where_in($keyCol, $values)
        ->delete($table);
    }
    
    public function getData($tableName, $selector, $condition)
    {
        $this->db->where($selector, $condition);
        $result=$this->db->get($tableName)->result_array();
        if ($result) {
            return $result[0];
        }
        return false;
    }

    public function get($tableName)
    {
        $result=$this->db->get($tableName)->result_array();
        return $result;
    }

    public function getAllData($tableName, $selector, $condition)
    {
        $this->db->where($selector, $condition);
        $result=$this->db->get($tableName)->result_array();
        return $result;
    }

    public function getSettingValue($settings_key)
    {
        $this->db->select('*');
        $this->db->from('rs_styld_tbl_settings');
        $this->db->where('settings_key', $settings_key);
        $result = $this->db->get()->row();
        if (!empty($result->settings_value)) {
            return $result->settings_value;
        } else {
            return false;
        }
    }

    public function getColumValue($tableName, $selector, $condition, $colName)
    {
        $this->db->select($colName);
        $this->db->from($tableName);
        $this->db->where($selector, $condition);
        $result = $this->db->get()->row();
        if (!empty($result)) {
            return $result->$colName;
        } else {
            return false;
        }
    }

    public function getInfo($table, $colName, $colValue)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($colName, $colValue);

        $query = $this->db->get();
        return $query->result_array();
    }


    /**
     * search from any table using conditions
     *
     * @param string $tableName table to perform search
     * @param array  $params    conditions array
     *
     * @return [type]            [description]
     */
    public function search($tableName, $params = [], $whereInKey = '', $whereInArr = [])
    {
        $res = $this->db;
        
        
        if (count($whereInArr)) {
            $res = $res->where_in($whereInKey, $whereInArr);
        } else {
            $res= $res->where($params);
        }

        $res= $res->get($tableName)
        ->result_array();

        return $res;
    }

    /**
     * Search a supplier by conditions specified
     *
     * @param array  $params  eg: ['user_id'=>1, 'user_email'=>abc@def.com]
     * @param string $whereIn eg:['user_id', [1,2,3,4]]
     *
     * @return array $res search result
     */
    public function searchSupplier($params, $whereIn = ['key'=>'', 'val'=>[]], $orWhere = [])
    {
        $res = $this->db
        ->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left')
        ->join('industry', 'industry.id=supplier_info.industry', 'left')
        ->where($params);
        if (count($whereIn['val'])) {
            $res->where_in($whereIn['key'], $whereIn['val']);
        }
        if (count($orWhere)) {
            $res->or_where($orWhere);
        }
        $res = $res
            ->where('role', SUPPLIER) //supplier constant value
            ->get('users')
            ->result_array();

            $res[0]['skills'] = 'No skill found';
            if (isset($params['user_id'])) {
                $res[0]['skills'] = $this->getSupplierSkills($params['user_id']);
                $res[0]['skills'] = strlen($res[0]['skills']) ? $res[0]['skills'] : 'No skill found';
            }
            return $res;
        }


        public function supplierByKeyword($keyword)
        {
            $res = $this->db
            ->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left')
            ->or_where('users.nick_name', $keyword)
            ->or_where('supplier_info.company_description like "%'.$keyword.'%"')
            ->or_where('supplier_info.company_core_business like "%'.$keyword.'%"')
            ->limit(1000)
            ->get('users')
            ->result_array();
            $cnt=0;

        //remove other than supplier
            foreach ($res as $supplier) {
                if ($supplier['role']!=SUPPLIER) {
                    unset($res[$cnt]);
                }
                $cnt++;
            }

            return $res;
        }
        public function supplierByKeywords($keyword)
        {
            $result=array();
            $keyword=explode(" ", $keyword);
            for ($i=0; $i < count($keyword); $i++) {
                $res = $this->db
                ->select('user_id')
                ->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left')
                ->or_where('users.nick_name like "%'.$keyword[$i].'%"')
                ->or_where('supplier_info.company_name like "%'.$keyword[$i].'%"')
                ->or_where('supplier_info.company_description like "%'.$keyword[$i].'%"')
                ->or_where('supplier_info.company_core_business like "%'.$keyword[$i].'%"')
                ->or_where('supplier_info.main_activities like "%'.$keyword[$i].'%"')
                ->limit(1000)
                ->get('users')
                ->result_array();
                
                for ($j=0; $j < count($res); $j++) {
                    $result[]=$res[$j];
                }
            }
            $result=array_unique(array_column($result, 'user_id'));
            return  $result;
        }

        public function getUserEvaluation($userId)
        {
            $res = $this->db
            ->select('*')
            ->from('evaluation')
        // ->join('users', 'users.user_id = evaluation.creator_id', 'left')
            ->join('users', 'users.user_id = evaluation.supplier_id', 'left')
            ->where('evaluation.creator_id', $userId)
            ->where('evaluation.is_approved', 1)
            ->get()
            ->result_array();
            return $res;
        }

        public function getPaginationEvaluation($userId, $limit, $offset)
        {
            $res = $this->db
            ->select('evaluation.*,users.*,supplier_info.company_name')
            ->from('evaluation')
        // ->join('users', 'users.user_id = evaluation.creator_id', 'left')
            ->join('users', 'users.user_id = evaluation.supplier_id', 'left')
            ->join('supplier_info', 'supplier_info.supplier_id = evaluation.supplier_id', 'left')
            ->where('evaluation.creator_id', $userId)
            ->where('evaluation.is_approved', 1)
            ->limit($limit, $offset)
            ->get()
            ->result_array();
            return $res;
        }

        public function getEvaluation($id, $supplierId = '')
        {
            $res = $this->db
            ->select('evaluation.id as evaluationId,evaluation.*,evaluation_rating.*')
            ->from('evaluation')
            ->join('evaluation_rating', 'evaluation_rating.evaluation_id=evaluation.id', 'left')
            ->where('evaluation.id', $id);
            if ($supplierId) {
                $res->where('evaluation.supplier_id', $supplierId);
            }
            $res= $res
            ->get()
            ->result_array();
            if ($res) {
                return $res[0];
            }
        }

            /**
             * get all evaluation data
             */

            public function getAllEvaluation()
            {
                $res = $this->db
                ->select('*')
                ->from('evaluation')
                ->join('users', 'users.user_id=evaluation.creator_id', 'left')
                ->get()
                ->result_array();
                return $res;
            }



            public function getAllSupplier()
            {
                $res = $this->db
                ->select('*')
                ->from('users')
                ->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left')
                ->join('industry', 'industry.id=supplier_info.industry', 'left')
                ->where('approved', 1)
                ->where('role', 3)
                ->limit(100)
                ->get()
                ->result_array();
                return $res;
            }

            public function getSupplierInfo($supplierId)
            {
                $res = $this->db
                ->select('*')
                ->from('users')
                ->join('supplier_info', 'users.user_id=supplier_info.supplier_id', 'left')
                ->join('industry', 'industry.id=supplier_info.industry', 'left')
                ->where('user_id', $supplierId)
                ->get()
                ->result_array();
                return $res;
            }

    /**
     * Get only admin approved evaluation
     *
     * @param integer $supplierId supplier id
     *
     * @return array             evaluations
     */
    public function getSupplierEvaluation($supplierId)
    {
        $res = $this->db
        ->select('*')
        ->from('evaluation')
        ->join('users', 'users.user_id=evaluation.creator_id', 'left')
        ->where('evaluation.supplier_id', $supplierId)
        ->where('evaluation.is_approved', 1)
        ->get()
        ->result_array();
        return $res;
    }

    public function getRating($supplierId)
    {
        $res = $this->db
        ->select('sum(avg_rating) as totRating,count(avg_rating) as totEvaluation')
        ->from('evaluation')
        ->where('supplier_id', $supplierId)
        ->get()
        ->result_array();
        return $res;
    }

    public function getConnect()
    {
        $evaluation = $this->db
        ->select('count(id) as totEvaluation')
        ->from('evaluation')
        ->get()->row();

        $supplier = $this->db
        ->select('count(supplier_id) as totEvaluation')
        ->from('supplier_info')
        ->get()->row();

        $total=$evaluation->totEvaluation + $supplier->totEvaluation;
        return $total;
    }




    /**
     * Search supplier by parameters
     *
     * @param array $params search parameters
     *
     * @return array users
     */
    public function supSearchByParams($params)
    {
        return $this->db
        ->join('supplier_info', 'supplier_info.supplier_id=users.user_id', 'left')
        ->where($params)
        ->where('users.role', SUPPLIER)
        ->where('users.approved', 1)
        ->limit(1000)
        ->get('users')
        ->result_array();
    }

    public function getEvaluationSkill($evaluation_id)
    {
        //echo $params;
        $res= $this->db
        ->join('skills', 'supplier_skills.skill_id=skills.id', 'left')
        ->where('supplier_skills.evaluation_id', $evaluation_id)
        ->get('supplier_skills')
        ->result_array();
        $res=array_column($res, 'name');
        $res=implode(",", $res);
        return $res;
    }

    public function getSupplierSkills($supplierId, $param = '')
    {
        //echo $params;
        $res= $this->db
        ->join('supplier_skills', 'supplier_skills.skill_id=skills.id', 'left')
        ->where('supplier_skills.supplier_id', $supplierId)
        ->get('skills')
        ->result_array();
        if ($param=='rt_array') {
            return $res;
        }
        $res=array_column($res, 'name');
        $res=implode(",", $res);
        return $res;
    }
    
    public function getSkills()
    {
        $this->db->select('id,name as text');
        $this->db->from('skills');
        $res=$this->db->get()->result_array();
        return $res;
    } 

    public function getCountries()
    {
        $this->db->select('id,country_name as text');
        $this->db->from('countries');
        $res=$this->db->get()->result_array();
        return $res;
    }

    /**
     * Get average rating of a supplier(approved only)
     *
     * @param integer $supplierId supplier id
     *
     * @return float               average value
     */
    public function averageRating($supplierId)
    {

        //$all eval = $this->search('')
        $res = $this->db
        ->select("SUM(reliability)+SUM(flexibility)+SUM(cost)+SUM(global_quality)+SUM(people_skills) as total")
        ->select("COUNT(evaluation.id) as cnt")
        ->join('evaluation_rating', 'evaluation_rating.evaluation_id=evaluation.id', 'left')
        ->where('supplier_id', $supplierId)
        ->where('evaluation.is_approved', 1)
        ->get('evaluation')
        ->result_array();
        
        return $res[0]['total'] ? $res[0]['total']/($res[0]['cnt']*5) : 0;
    }

    public function recapData($supplierId)
    {
        return  $this->db
        ->select('COUNT(one.avg_rating), COUNT(two.avg_rating)')
        ->join('evaluation as two', 'one.supplier_id=two.supplier_id', 'left')
        ->where('one.avg_rating = 2')
        ->where('two.avg_rating < 3 AND two.avg_rating > 2')
        ->where('one.supplier_id', $supplierId)
        ->group_by('one.id')
        ->get('evaluation as one')
        ->result_array();
    }

    /**
     * Get count of rating within a range
     *
     * @param integer $supplierId supplier id
     * @param string  $conditions condition string
     *
     * @return integer             count
     */
    public function getRecapAvg($supplierId, $conditions = "")
    {
        $res = $this->db
        ->select("COUNT('id') as cnt")
        ->where('supplier_id', $supplierId)
        ->where($conditions)
        ->get('evaluation')
        ->result_array();

        return $res[0]['cnt'];
    }

    public function supplierRating($supplierId)
    {

        //$all eval = $this->search('')
        $res = $this->db
        ->select("SUM(avg_rating) as total")
        ->select("COUNT(evaluation.id) as cnt")
        ->where('supplier_id', $supplierId)
        ->where('evaluation.is_approved', 1)
        ->get('evaluation')
        ->result_array();
        
        return $res[0]['total'] ? $res[0]['total']/($res[0]['cnt']) : 0;
    }
}
