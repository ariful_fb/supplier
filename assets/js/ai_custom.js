$( document ).ready(function() {
	$(document).on('click','.submitForm',function(){
		var link =$(this).attr('url');

		$.ajax({
			url:'FrontendController/userData',
			method:'POST',
			data:{link:link,data:$("#target").serializeArray()},
			success:function(res){
				//alert(res);
				window.location.replace(res);
			},
			error:function(){

			}
		});
	});
	$( "#target" ).validate({
		rules:{
			
			nick_name:{
				remote:{
					url:"AdminController/nameCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},
			
		},
		messages: {
			
			nick_name:{
				remote:"This nick name already used!!"
			},
			
		},
		submitHandler: function(form) {
			alert(form);
		}    
		

	});

	$( "#singIn" ).submit(function() {
		event.preventDefault();
		//alert(4);
		$.ajax({
			url: "AuthController/check_sin",
			type:"POST",
			data: $( this ).serializeArray(),
			success: function(data) {
				if(data==2){
					$("#user_error").empty();
					$("#user_error").append("Invalid user request.");

				}else{
					
					//return true;
					//location.reload();
					window.location.replace("user/home");
				}
			},
			error:function(err){
				alert("err");
			},            
		});
	});


	$( "#supplier" ).submit(function() {
		//event.preventDefault();
		//alert(4);
	}).validate({
		rules:{
			user_email:{
				email:true,
				remote:{
					url:"AdminController/emailCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},
			nick_name:{
				remote:{
					url:"AdminController/nameCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},
			password:{
				remote:{
					url:"AdminController/passwordCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},

		},
		messages: {
			user_email:{
				remote:"This email already used!!"
			},
			nick_name:{
				remote:"This nick name already used!!"
			},
			password:{
				remote:"password must be length 6."
			}
		},
		submitHandler: function(form) {
			$.ajax({
				url: form.action,
				type: form.method,
				data: $(form).serialize(),
				success: function(response) {
					window.location.replace("supplier_registration_info/"+response);
					// if(response==1){
					// 	$('#supplier_login').modal('hide');
					// 	$('#supplier_register').modal('hide');
					// 	swal({
					// 		title: "Good job!",
					// 		text: "Sign up successfull.Now administrator send a link via email to complete registration.",
					// 		icon: "success",
					// 	});
					// 	$('#supplier_register').modal('hide');
					// }
					// else
					// 	$("#res_error").append("Invalid user request.");
				},
				error:function(err){
					alert("err");
				},            
			});
		}    

	});

	$( "#supplier_singin" ).submit(function() {
		event.preventDefault();
		//alert(4);
		$.ajax({
			url: "AuthController/check_sin",
			type:"POST",
			data: $( this ).serializeArray(),
			success: function(data) {
				//alert(data);
				if(data==2){
					$("#supplier_error").empty();
					$("#supplier_error").append("Invalid user request.");
				}else{
					//return true;
					// location.reload();
					window.location.replace("supplier/home");
				}
				
			},
			error:function(err){
				alert("err");
			},            
		});
	});












	$( "#add_supplier" ).validate({
		rules:{
			user_email:{
				email:true,
				remote:{
					url:"AdminController/emailCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},
			nick_name:{
				remote:{
					url:"AdminController/nameCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},
			password:{
				remote:{
					url:"AdminController/passwordCheck",
					type:"POST",
					data: {
						'request':'add',
					}

				}
			},

		},
		messages: {
			user_email:{
				remote:"This email already used!!"
			},
			nick_name:{
				remote:"This nick name already used!!"
			},
			password:{
				remote:"password must be length 6."
			}
		},

	});



	//$(".select2").select2();


	$('input.typeahead').typeahead({
		source:  function (query, process) {
			return $.get('auto_search', { query: query }, function (data) {
				data = $.parseJSON(data);
				console.log(data);
				return process(data);
			});
		}
	});


	$(function(){
		$('#activity_type').on('change',function(){      
			var val=$(this).val();
			//alert(val);
			var html='';
			if(val=='1'){
				html +='<option value="1">more than 1 year service</option>';
				html +='<option value="2">less than 1 year service</option>';
				html +='<option value="3">project</option>';
				html +='<option value="4">on demand activity</option>';
				html +='<option value="5">other Value</option>';
				$(".remove_style").removeAttr('style');
			}
			else{
				html +='<option value="1">On demand</option>';
				$('.remove_style').attr('style', 'display:none');
			}
			$('#activity_duration').html(html);
		});
	});




	$(function () {

		$(".rateYo").rateYo({
			onChange : function(rating, rateYoInstance){
				//$(this).next('.counter').text(rating);
				var inputId = $(this).attr('inputId');
				$('#'+inputId).val(rating);
				//console.log(rating);
			},
			fullStar: true, 
			minValue: 1,
			//rating: 0,
			spacing: "10px",
			starWidth: "20px",	
		});

	});


	$('.add-one').click(function(){
		$('.dynamic-element').first().clone().appendTo('.dynamic-anchor').show();
		attach_delete();
	});
//Attach functionality to delete buttons
function attach_delete(){
	$('.delete').off();
	$('.delete').click(function(){
		console.log("click");
		$(this).closest('.form-delete').remove();
	});
}

//add evaluation field check

$("#add_evaluation").validate({
	rules:{
		username:{
			required:true,
		},
		induction:{
			required:true,
		},
		activity_type:{
			required:true,
		},
		check:{
			required:true,
		},
		reliability:{
			required:true,
			remote:{
				url:"FrontendController/hasReliability",
				type:"POST",
			}
		},
		flexibility:{
			required:true,
			remote:{
				url:"FrontendController/hasFlexibility",
				type:"POST",
			}
		},
		cost:{
			required:true,
			remote:{
				url:"FrontendController/hasCost",
				type:"POST",
			}
		},
		global_quality:{
			required:true,
			remote:{
				url:"FrontendController/hasGlobal_quality",
				type:"POST",
			}
		},
		people_skills:{
			required:true,
			remote:{
				url:"FrontendController/hasPeople_skills",
				type:"POST",
			}
		}
	},
	messages: {
		reliability:{
			remote:"This field is required."
		},flexibility:{
			remote:"This field is required."
		},cost:{
			remote:"This field is required."
		},global_quality:{
			remote:"This field is required."
		},people_skills:{
			remote:"This field is required."
		},
	},
	submitHandler: function(form) {
		var role=$("#role").val();

		if(role)return true;
		$.ajax({
			url: "AuthController/demoEvaluation",
			type:"POST",
			data:$("#add_evaluation").serializeArray(),
			success: function(data) {
				$("#new_evaluation").modal('hide');
				$("#user_login").modal('show');
				console.log(data);

			},
			error:function(err){
				alert("err");
			},            
		});


			//e.preventDefault();
			
		}      
	});

$('#check').change(function(){

	if ($(this).prop('checked')) {
		$( "#check" ).prop( "checked", false );
		$("#myModal").modal('show');
	}

});
$('#check_yes').click(function(){
	$( "#check" ).prop( "checked", true );
	$("#myModal").modal('hide');
});
$('#check_no').click(function(){
	$("#myModal").modal('hide');
});

// function activityType(value) {
// 	alert(value);
// }
$(function () {

	$("#rateY").rateYo({
		rating: 3.2,
		spacing: "20px"
	});

});
$(function () {

	$("#rateYo").rateYo({
		rating: 2.6,
		spacing: "20px"
	});

});


$('#confEmailForm').on('submit', function(e){
	e.preventDefault();
	var email = $('#confEmail').val();
	$.ajax({
		url:'supplier/mail/confirmation/?email='+email,
		method: 'GET',
		success: function(){
			$('#conf_email_modal').modal('toggle');
			swal('Email sent successfully');
		}
	})
})


$('input.skills').typeahead({

	source:  function (query, process) {
				//alert(55);
				return $.get('search_skill', { query: query }, function (data) {
					data = $.parseJSON(data);
					console.log(data);
					return process(data);
				});
			}
		});



});

